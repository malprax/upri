#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# , graduate_profile_attributes:{ipk:"3.5", yudisium_date: "21 februari 2017", predicet_of_graduate: "memuaskan", alumni_number: 001 }, essay_attributes:{title: "robotika"}, faculty_field_attributes:{faculty: "Fakultas Teknik (FT)", major: "Teknik Mesin"}
# , name: "graduate1", tribe:"Bugis", nation:"Indonesia", birth_date: "26 april 1985", birth_place: "makassar", addres_attributes: {street: "karaeng bontotangnga no 31"}

# faculty
faculties = Faculty.first_or_create([{code:"fkip", name: "Fakultas Ilmu Kependidikan"}, {code: "fisipol", name:"Fakultas Ilmu Sosial Dan Politik"},{code: "fkm", name: "Fakultas Kesehatan Masyarakat"},{code:"ft", name: "Fakultas Teknik"},{code: "fe", name:"Fakultas Ekonomi"}])

# major
 majors = Major.first_or_create([
   {name: "Pendidikan Biologi", code: "biologi", faculty_id: 1}, #1
   {name: "Pendidikan Matematika", code: "matematika", faculty_id: 1}, #2
   {name:"Pendidikan PPKN", code: "ppkn", faculty_id: 1}, #3
   { name: "Pendidikan Sejarah", code: "sejarah", faculty_id: 1}, #4
   {name: "Teknologi Pendidikan", code: "teknologi_pendidikan", faculty_id: 1}, #5
   {name: "Ilmu Administrasi Negara", code: "administrasi_negara", faculty_id: 2}, #6
   {name: "Ilmu Komunikasi", code: "komunikasi", faculty_id: 2}, #7
   {name: "Kesehatan Masyarakat", code: "kesehatan_masyarakat", faculty_id: 3}, #8
   {name: "Teknik Pertambangan", code: "pertambangan", faculty_id: 4}, #9
   {name: "Teknik Mesin", code: "mesin", faculty_id: 4}, #10
   {name: "Teknik Informatika", code: "informatika", faculty_id: 4}, #11
   {name: "Manajemen", code: "manajemen", faculty_id: 5}, #12
   {name: "Akuntansi", code: "akuntansi", faculty_id: 5}]) #13

# 9.times do |x|
#   Lecture.create!([
#     {email: "lecture#{x}@gmail.com", password: "lecture#{x}", password_confirmation:"lecture#{x}", name: "lecture #{x}", nidn: "1100#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:1, faculty_id:1, type_check: "lecture"}
#   ])
# end
# #
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan1#{x}@gmail.com", password: "wisudawan1#{x}", password_confirmation:"wisudawan1#{x}", name: "wisudawan 1#{x}", stambuk: "1101#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:2, faculty_id:1, type_check: "graduate" }
#   ])
# end
#


# Dosen UPRI
DummyLecture.first_or_create!([
  {nama:"A. DWI SAPTA NINGSI", major_id:3},
  {nama:"A. SAPUTRA MACCIRINA", major_id:3},
  {nama:"ABD. MUIN", major_id:3},
  {nama:"ABDUL AZIS DP", major_id:3},
  {nama:"ABDUL BASITH RAHMAN", major_id:7},
  {nama:"ABDUL GAFFAR GANI", major_id:13},
  {nama:"ABDUL GAFUR", major_id:8},
  {nama:"ABDUL MUIN", major_id:5},
  {nama:"ABDUL RAZAK", major_id:3},
  {nama:"ABDUL WAHAB", major_id:5},

  {nama:"ADAM BADWI", major_id:8},
  {nama:"ADB HAFID HAMID", major_id:12},
  {nama:"AGUS SALIM", major_id:8},
  {nama:"AKHMAD", major_id:6},
  {nama:"ALI IMRAN", major_id:8},
  {nama:"AMALUDDIN", major_id:6},
  {nama:"AMRIANI", major_id:3},
  {nama:"ANDI ALIM", major_id:8},
  {nama:"ANDI AMINAH SUYUTI", major_id:8},
  {nama:"ANDI AMRULLAH", major_id:9},

  {nama:"ANDI ARDIANSAH", major_id:3},
  {nama:"ANDI ASRI", major_id:8},
  {nama:"ANDI ASRUL SUKMA", major_id:9},
  {nama:"ANDI FAUZIAH ASTRID ABIDIN", major_id:7},
  {nama:"ANDI HERAWATI M. NUR", major_id:13},
  {nama:"ANDI IRWAN", major_id:12},
  {nama:"ANDI JUSMIANA", major_id:2},
  {nama:"ANDI MAHYUL", major_id:5},
  {nama:"ANDI RUSMIN MULYADI", major_id:6},
  {nama:"ANDI TENRI PADA AGUSTANG", major_id:8},

  {nama:"ANDI ZULKIFLI", major_id:8},
  {nama:"ANDRY Z", major_id:4},
  {nama:"ANISA SRINDRAWANTI", major_id:11},
  {nama:"ANSAR", major_id:2},
  {nama:"ANZAR A", major_id:4},
  {nama:"ARIYANTI RUMENGAN", major_id:2},
  {nama:"ARLIN ADAM", major_id:8},
  {nama:"ARMAN YASIR", major_id:3},
  {nama:"ARTATIWATI AS", major_id:3},
  {nama:"ARWAN NURDIN", major_id:6},

  {nama:"ARWATY", major_id:1},
  {nama:"ASDINAR WAHID AWALUDDIN", major_id:12},
  {nama:"ASHARIANA", major_id:6},
  {nama:"ASLIM MUDA AZIS", major_id:10},
  {nama:"ASMAWATI ABBAS", major_id:5},
  {nama:"ASNITA RIKO", major_id:8},
  {nama:"ASRIANI ALIMUDDIN", major_id:7},
  {nama:"ASRUL", major_id:11},
  {nama:"ASRUL NUR IMAN", major_id:7},
  {nama:"ASTI WANDASARI", major_id:1},

  {nama:"ASTRI MAYALANDARI", major_id:2},
  {nama:"ASTUTI MUH. AMIN", major_id:1},
  {nama:"AULIA SABRIL", major_id:10},
  {nama:"BACHTIAR", major_id:6},
  {nama:"BASO AMRAN AMIR", major_id:6},
  {nama:"BASO JUNAIN", major_id:9},
  {nama:"BENGET HUTAGAOL", major_id:12},
  {nama:"BRAMPIE M SARIMANELLA", major_id:6},
  {nama:"D MUSTAFA", major_id:6},
  {nama:"DARMAWATI A", major_id:4},

  {nama:"DIAN EKAWATY", major_id:8},
  {nama:"DJAIS DUJO", major_id:5},
  {nama:"DJUMIATI NOOR", major_id:3},
  {nama:"ELIFAS BUNGA", major_id:9},
  {nama:"ENNI TRI MAHYUNI", major_id:9},
  {nama:"ERNI KADIR", major_id:8},
  {nama:"ETTY ROSMIATI", major_id:1},
  {nama:"FAHARUDDIN", major_id:3},
  {nama:"FAISAL", major_id:11},
  {nama:"FAISAL SUYUTHI", major_id:9},

  {nama:"FARIDAH", major_id:12},
  {nama:"FATMAWATY RACHIM", major_id:9},
  {nama:"FIFI ALFIANA", major_id:3},
  {nama:"FIRDAUS ANAS", major_id:6},
  {nama:"HAIRUDDIN M", major_id:8},
  {nama:"HAJRAH", major_id:8},
  {nama:"HANIAH HAMID", major_id:5},
  {nama:"HARDI YUSRI", major_id:3},
  {nama:"HARIFUDDIN", major_id:3},
  {nama:"HARMINIWATI", major_id:3},

  {nama:"HASANUDDIN", major_id:9},
  {nama:"HASLINDA S", major_id:10},
  {nama:"HASNI KASIM", major_id:9},
  {nama:"HASRAN", major_id:6},
  {nama:"HERMAN S", major_id:7},
  {nama:"HERNA YUSTIASARI", major_id:8},
  {nama:"HERNIAH S", major_id:12},
  {nama:"HIDAYAH", major_id:3},
  {nama:"I MADE DARMA", major_id:9},
  {nama:"IDHAR MAHMUDDIN", major_id:8},

  {nama:"ILHAM SAMANLANGI", major_id:9},
  {nama:"INDARWATI", major_id:2},
  {nama:"INDRA SULISTYANTO R", major_id:9},
  {nama:"ISKANDAR", major_id:6},
  {nama:"ISMAIL HASANUDDIN", major_id:11},
  {nama:"ISNADA", major_id:1},
  {nama:"JAMAL ANDI", major_id:6},
  {nama:"JAMALUDDIN", major_id:6},
  {nama:"JELAU MATIAS", major_id:4},
  {nama:"JUFRI NUR", major_id:9},

  {nama:"JUMRIATI", major_id:3},
  {nama:"KAMELIA MALIK", major_id:3},
  {nama:"KASMIRA", major_id:9},
  {nama:"LA ODE HARDIMAN", major_id:1},
  {nama:"M ASRI TAPA", major_id:13},
  {nama:"M. JUFRI ABURAERA", major_id:3},
  {nama:"M. DARWIS", major_id:6},
  {nama:"MAESARAH", major_id:8},
  {nama:"MAKBUL", major_id:7},
  {nama:"MARDIHANG", major_id:6},

  {nama:"MARIA", major_id:8},
  {nama:"MARWAN AIDIT", major_id:12},
  {nama:"MASDARWATI", major_id:8},
  {nama:"MASDING", major_id:5},
  {nama:"MASMARULAN R", major_id:6},
  {nama:"MASRI", major_id:10},
  {nama:"MASRIADI", major_id:6},
  {nama:"MASYKUR", major_id:3},
  {nama:"MERDUATI", major_id:3},
  {nama:"MISLIA", major_id:3},

  {nama:"MOH KHAIDIR NOOR", major_id:9},
  {nama:"MOHAMMAD ARIS PANGERANG", major_id:3},
  {nama:"MUH. AL ASH HARI", major_id:3},
  {nama:"MUH. ALFIAN PRIMANANDA", major_id:3},
  {nama:"MUH. DARWIS", major_id:10},
  {nama:"MUH. MIZWAR BONE", major_id:8},
  {nama:"MUHAMMAD AL MUHAJIR", major_id:1},
  {nama:"MUHAMMAD AMIRULLAH", major_id:12},
  {nama:"MUHAMMAD AZWAR", major_id:8},
  {nama:"MUHAMMAD BADAR", major_id:13},

  {nama:"MUHAMMAD BAKRI", major_id:12},
  {nama:"MUHAMMAD BASIR", major_id:6},
  {nama:"MUHAMMAD HASIM", major_id:6},
  {nama:"MUHAMMAD ILHAM", major_id:3},
  {nama:"MUHAMMAD IRFAN", major_id:3},
  {nama:"MUHAMMAD ISA ANSARI", major_id:6},
  {nama:"MUHAMMAD KAPRAWI", major_id:2},
  {nama:"MUHAMMAD NASIR", major_id:12},
  {nama:"MUHAMMAD RIZAL", major_id:2},
  {nama:"MUHAMMAD RUSTAN", major_id:3},

  {nama:"MUHAMMAD SAID M.", major_id:11},
  {nama:"MUHAMMAD SYUKUR", major_id:7},
  {nama:"MUHAMMAD TAHIR G", major_id:5},
  {nama:"MUHAMMAD WIJAYA", major_id:3},
  {nama:"MUHAMMAD YUSUF", major_id:11},
  {nama:"MUHLIS", major_id:2},
  {nama:"MULIANI RATNANINGSIH", major_id:8},
  {nama:"MUNADHIR", major_id:3},
  {nama:"MUNADHIR", major_id:8},
  {nama:"MUNAWAR", major_id:10},

  {nama:"MUNIARTI M", major_id:1},
  {nama:"MURNIATI JAFAR", major_id:3},
  {nama:"MURSALIM", major_id:6},
  {nama:"MUSLIMIN B", major_id:8},
  {nama:"MUSTAIN THAHIR", major_id:5},
  {nama:"MUSTAJIB", major_id:7},
  {nama:"NASNI", major_id:3},
  {nama:"NORMIYATI N", major_id:12},
  {nama:"NUDDIN", major_id:6},
  {nama:"NUR DWIYANA SARI SAUDI", major_id:12},

  {nama:"NUR ICHSAN", major_id:11},
  {nama:"NURAHMAH MANRIHU", major_id:6},
  {nama:"NURALAM", major_id:8},
  {nama:"NURHAENAH", major_id:3},
  {nama:"NURHASNAH", major_id:5},
  {nama:"NURHAYATI NOOR", major_id:3},
  {nama:"NURHIKMAH S", major_id:3},
  {nama:"NURISMI", major_id:3},
  {nama:"NURKADARWATI", major_id:6},
  {nama:"NURSINAH", major_id:8},

  {nama:"NURYADIN", major_id:6},
  {nama:"ONNO SAHLANIA", major_id:6},
  {nama:"RAFIUDDIN", major_id:9},
  {nama:"RAHMA TOMPO", major_id:9},
  {nama:"RAHMAN", major_id:6},
  {nama:"RAMLI", major_id:10},
  {nama:"RATNA KURNIATI M.A", major_id:2},
  {nama:"REGINA SAMANA", major_id:3},
  {nama:"RERY APRIANY ZAINUL", major_id:8},
  {nama:"RESTU ISTININGDIAH", major_id:13},

  {nama:"RIDWAN", major_id:3},
  {nama:"RISA BERNADIP OEMAR", major_id:10},
  {nama:"RIVAI MANA", major_id:3},
  {nama:"RUBEN GIRIKAN", major_id:4},
  {nama:"RUCHYAT DJAYADI PUTRA", major_id:9},
  {nama:"RUKMINI AS", major_id:6},
  {nama:"RUSNITA", major_id:8},
  {nama:"RUTH BUNGA RANGGU", major_id:9},
  {nama:"SAIFUDDIN AL-MUGHNY", major_id:13},
  {nama:"SAMSIR", major_id:8},

  {nama:"SARAH SAMBEN", major_id:1},
  {nama:"SASMITA NABILA SYAHRIR", major_id:13},
  {nama:"SINGARA", major_id:3},
  {nama:"ST HALIJAH", major_id:7},
  {nama:"ST. HALIJAH", major_id:6},
  {nama:"SUAIB", major_id:3},
  {nama:"SUDIRMAN", major_id:8},
  {nama:"SUDIRMAN", major_id:9},
  {nama:"SUHERMAN", major_id:3},
  {nama:"SUKADI", major_id:2},

  {nama:"SULHAN", major_id:7},
  {nama:"SUMARNI SUSILAWATI", major_id:2},
  {nama:"SUPARDI SALAM", major_id:4},
  {nama:"SURATNI", major_id:3},
  {nama:"SUWARNO", major_id:10},
  {nama:"SYAFRI", major_id:3},
  {nama:"SYAMSU K", major_id:3},
  {nama:"SYAMSUDDIN", major_id:9},
  {nama:"SYARIFUDDIN HS", major_id:6},
  {nama:"TAUFIK M.", major_id:3},

  {nama:"TENRI ABENG", major_id:3},
  {nama:"THAMRIN PAWALLURI", major_id:4},
  {nama:"TRY JAYADI", major_id:3},
  {nama:"TS SYALAHUDDIN", major_id:11},
  {nama:"VERAWATI", major_id:7},
  {nama:"WAHYUNI", major_id:9},
  {nama:"YOENIRMA MASSALINRING", major_id:2},
  {nama:"YUSRI", major_id:9},
  {nama:"YUSTIN PAISAL", major_id:9},
  {nama:"ZAINUDDIN", major_id:8},

  {nama:"BAHARUDDIN", major_id:12},
  {nama:"ANWAR RAMLI", major_id:12},
  {nama:"LILIS WIDIASTUTY", major_id:8},
  {nama:"AHMAD SYAEKHU", major_id:8},
  {nama:"MUH. ARSYAD RAHMAN", major_id:8}
])

# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan2#{x}@gmail.com", password: "wisudawan2#{x}", password_confirmation:"wisudawan2#{x}", name: "wisudawan 2#{x}", stambuk: "1102#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:3, faculty_id:1 }
#   ])
# end
#
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan3#{x}@gmail.com", password: "wisudawan3#{x}", password_confirmation:"wisudawan3#{x}", name: "wisudawan 3#{x}", stambuk: "1103#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:4, faculty_id:1 }
#   ])
# end
#
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan4#{x}@gmail.com", password: "wisudawan4#{x}", password_confirmation:"wisudawan4#{x}", name: "wisudawan 4#{x}", stambuk: "1104#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:5, faculty_id:1 }
#   ])
# end
#
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan5#{x}@gmail.com", password: "wisudawan5#{x}", password_confirmation:"wisudawan5#{x}", name: "wisudawan 5#{x}", stambuk: "1105#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:6, faculty_id:2 }
#   ])
# end
#
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan6#{x}@gmail.com", password: "wisudawan6#{x}", password_confirmation:"wisudawan6#{x}", name: "wisudawan 6#{x}", stambuk: "1106#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:7, faculty_id:2 }
#   ])
# end
#
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan7#{x}@gmail.com", password: "wisudawan7#{x}", password_confirmation:"wisudawan7#{x}", name: "wisudawan 7#{x}", stambuk: "1107#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:8, faculty_id:3 }
#   ])
# end
#
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan8#{x}@gmail.com", password: "wisudawan8#{x}", password_confirmation:"wisudawan8#{x}", name: "wisudawan 8#{x}", stambuk: "1108#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:9, faculty_id:4 }
#   ])
# end
#
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan9#{x}@gmail.com", password: "wisudawan9#{x}", password_confirmation:"wisudawan9#{x}", name: "wisudawan 9#{x}", stambuk: "1109#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:10, faculty_id:4 }
#   ])
# end
#
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan10#{x}@gmail.com", password: "wisudawan10#{x}", password_confirmation:"wisudawan10#{x}", name: "wisudawan 10#{x}", stambuk: "11010#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:11, faculty_id:4 }
#   ])
# end
#
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan11#{x}@gmail.com", password: "wisudawan11#{x}", password_confirmation:"wisudawan11#{x}", name: "wisudawan 11#{x}", stambuk: "11011#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:12, faculty_id:5 }
#   ])
# end
#
# 9.times do |x|
#   Graduate.create!([
#     {email: "wisudawan12#{x}@gmail.com", password: "wisudawan12#{x}", password_confirmation:"wisudawan12#{x}", name: "wisudawan 12#{x}", stambuk: "11012#{x}",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam", major_id:13, faculty_id:5 }
#   ])
# end

# (2..118).each do |x|
#   GraduateProfile.create!([
#     {ipk:3.5, yudisium_date: "2017-02-11", predicet_of_graduate: "sangat_memuaskan", user_id: "#{x}"}
#   ])
#   # Essay.create!!([
#   #   {user_id: "#{x}.to_i", title: "judul tesis wisudawan #{x}" }
#   # ])
# end



#{email: "lecture#{x}@gmail.com", password: "lecture#{x}", password_confirmation:"lecture#{x}", type:"Lecture", name: "lecture#{x}", tribe:"Bugis", nation:"Indonesia",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam"},
#     {email: "student#{x}@gmail.com", password: "student#{x}", password_confirmation:"student#{x}", type:"Student", name: "student#{x}", tribe:"Bugis", nation:"Indonesia",birth_date: "1985-04-26", birth_place: "makassar", religion: "Islam"}


#major

# def self.faculty_list
#   [
#    "Fakultas Keguruan Dan Ilmu Pendidikan (FKIP)",
#    "Fakultas Ilmu Sosial Dan Ilmu Politik (FISIPOL)",
#    "Fakultas Kesehatan Masyarakat (FKM)",
#    "Fakultas Teknik (FT)",
#    "Fakultas Ekonomi (FE)"
#   ]
# end
#
# def self.jurusan_list
#   [
#    "Pendidikan Biologi",
#    "Pendidikan Matematika",
#    "Pendidikan PPKN",
#    "Pendidikan Sejarah",
#    "Teknologi Pendidikan",
#    "Ilmu Administrasi Negara",
#    "Ilmu Komunikasi",
#    "Kesehatan Masyarakat",
#    "Teknik Pertambangan",
#    "Teknik Mesin",
#    "Teknik Informatika",
#    "Manajemen",
#    "Akuntansi"
#   ]
# end





# while !array.empty? do
#   begin
#     10.times do |x|
#       array << User.create(email: "graduate#{x}@gmail.com", password: "graduate#{x}", password_confirmation:"graduate#{x}", type:"Graduate", name: "graduate#{x}", tribe:"Bugis", nation:"Indonesia",birth_date: "1985-04-26", birth_place: "makassar")
#     end
#   rescue
#     array.drop(1)
#   end
# end

puts "Berhasil Upload Seed"
