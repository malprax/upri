# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_12_144140) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "academic_years", force: :cascade do |t|
    t.string "name"
    t.boolean "active_status", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_active", default: false
    t.string "semester"
    t.string "semester_status"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "addresses", force: :cascade do |t|
    t.string "street"
    t.string "rt"
    t.string "rw"
    t.string "kelurahan"
    t.string "kecamatan"
    t.string "city"
    t.string "province"
    t.integer "post_code"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_addresses_on_user_id"
  end

  create_table "admin_profiles", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_admin_profiles_on_user_id"
  end

  create_table "appreciation_backgrounds", force: :cascade do |t|
    t.string "year"
    t.string "institution"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_appreciation_backgrounds_on_user_id"
  end

  create_table "articles", force: :cascade do |t|
    t.string "title"
    t.string "author"
    t.bigint "journal_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["journal_id"], name: "index_articles_on_journal_id"
  end

  create_table "backup_graduates", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.bigint "dummy_lecture_1_id"
    t.bigint "dummy_lecture_2_id"
    t.bigint "major_id"
    t.bigint "faculty_id"
    t.integer "academic_year_id"
    t.string "phone_number"
    t.text "image_data"
    t.date "birth_date"
    t.string "birth_place"
    t.string "gender"
    t.string "religion"
    t.string "marital_status"
    t.string "hobby"
    t.string "stambuk"
    t.string "nidn"
    t.string "nip"
    t.text "blangko_data"
    t.integer "sortlist"
    t.float "ipk"
    t.date "yudisium_date"
    t.string "yudisium_letter"
    t.integer "alumni_number"
    t.string "predicet_of_yudisium"
    t.string "essay_title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "banks", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "company"
    t.string "account_name"
    t.string "account_number"
    t.string "code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "book_collections", force: :cascade do |t|
    t.string "judul"
    t.string "penulis"
    t.string "penerbit"
    t.text "cover_buku_data"
    t.string "kategori"
    t.string "posisi"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "campus_newspapers", force: :cascade do |t|
    t.string "judul"
    t.text "isi"
    t.string "kategori"
    t.integer "user_id"
    t.string "posisi"
    t.boolean "terbit"
    t.text "image_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "candidate_students", force: :cascade do |t|
    t.string "registration_number"
    t.string "class_plan"
    t.string "major_1"
    t.string "major_2"
    t.string "name"
    t.date "birth_date"
    t.string "birth_place"
    t.string "gender"
    t.string "phone_number"
    t.string "street_address"
    t.string "hamlet"
    t.string "neighbourhood"
    t.string "urban_village"
    t.string "sub_district"
    t.string "district"
    t.string "province"
    t.integer "postal_code"
    t.string "school"
    t.string "school_address"
    t.string "school_plan"
    t.string "nisn"
    t.string "sertificate_number"
    t.string "sertificate_date"
    t.string "national_exam_score"
    t.string "father_name"
    t.string "father_job"
    t.string "father_salary"
    t.string "father_education"
    t.string "mother_name"
    t.string "mother_job"
    t.string "mother_salary"
    t.string "mother_education"
    t.string "know_from"
    t.integer "academic_year_id"
    t.string "referral"
    t.string "email"
    t.boolean "approve", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "course_backgrounds", force: :cascade do |t|
    t.string "course_name"
    t.string "duration"
    t.string "pass_year"
    t.string "place"
    t.string "information"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_course_backgrounds_on_user_id"
  end

  create_table "districts", force: :cascade do |t|
    t.string "nama"
    t.integer "state_id"
    t.string "id_wilayah"
    t.string "id_negara"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string "name"
    t.integer "document_file_id"
    t.string "document_file_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dummy_lectures", force: :cascade do |t|
    t.string "nama"
    t.integer "nidn"
    t.integer "nip"
    t.string "gelar_depan_nama"
    t.string "jabatan_akademik"
    t.string "pendidikan"
    t.bigint "major_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "gelar_belakang_nama"
    t.string "status"
    t.index ["major_id"], name: "index_dummy_lectures_on_major_id"
  end

  create_table "educational_backgrounds", force: :cascade do |t|
    t.string "institution_name"
    t.string "level"
    t.string "major_study"
    t.string "pass_year"
    t.string "place"
    t.string "head_institution_name"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_educational_backgrounds_on_user_id"
  end

  create_table "educations", force: :cascade do |t|
    t.string "level"
    t.string "major"
    t.string "institute"
    t.string "graduation_year"
    t.integer "education_history_id"
    t.string "education_history_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "essay_documents", force: :cascade do |t|
    t.string "kategori"
    t.text "document_upload_data"
    t.bigint "essay_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["essay_id"], name: "index_essay_documents_on_essay_id"
  end

  create_table "essays", force: :cascade do |t|
    t.string "title"
    t.bigint "academic_year_id"
    t.bigint "graduate_id"
    t.bigint "dummy_lecture_1_id"
    t.bigint "dummy_lecture_2_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "cover_skripsi_data"
    t.string "posisi"
    t.string "kode"
    t.index ["academic_year_id"], name: "index_essays_on_academic_year_id"
    t.index ["dummy_lecture_1_id"], name: "index_essays_on_dummy_lecture_1_id"
    t.index ["dummy_lecture_2_id"], name: "index_essays_on_dummy_lecture_2_id"
    t.index ["graduate_id"], name: "index_essays_on_graduate_id"
  end

  create_table "faculties", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "university_id"
  end

  create_table "family_trees", force: :cascade do |t|
    t.string "family_name"
    t.string "family_status"
    t.string "gender"
    t.string "birth_date"
    t.string "birth_place"
    t.string "job"
    t.string "info"
    t.string "address"
    t.string "phone"
    t.string "office_name"
    t.string "office_registration_number"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_family_trees_on_user_id"
  end

  create_table "foundations", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "graduate_profiles", force: :cascade do |t|
    t.float "ipk"
    t.date "yudisium_date"
    t.string "yudisium_letter"
    t.integer "alumni_number"
    t.string "predicet_of_yudisium"
    t.bigint "graduate_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["graduate_id"], name: "index_graduate_profiles_on_graduate_id"
  end

  create_table "identities", force: :cascade do |t|
    t.string "kategori"
    t.text "file_identity_data"
    t.bigint "user_id"
    t.string "user_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_identities_on_user_id"
  end

  create_table "incomes", force: :cascade do |t|
    t.string "nama"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "item_accreditations", force: :cascade do |t|
    t.string "item"
    t.integer "academic_year_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "jenjang_didiks", force: :cascade do |t|
    t.string "nama_jenjang_didik"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "jobs", force: :cascade do |t|
    t.string "nama"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "journals", force: :cascade do |t|
    t.string "volume"
    t.integer "number"
    t.boolean "published", default: false
    t.string "issn"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "description"
  end

  create_table "lecture_appreciations", force: :cascade do |t|
    t.string "year"
    t.string "appreciation"
    t.string "presented_by"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_appreciations_on_lecture_id"
  end

  create_table "lecture_conferences", force: :cascade do |t|
    t.string "year"
    t.string "conference"
    t.string "presented_by"
    t.string "role"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_conferences_on_lecture_id"
  end

  create_table "lecture_education_histories", force: :cascade do |t|
    t.string "level"
    t.string "major"
    t.string "institute"
    t.string "graduation_year"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_education_histories_on_lecture_id"
  end

  create_table "lecture_functional_types", force: :cascade do |t|
    t.string "name"
    t.string "role"
    t.integer "credit"
    t.bigint "lecture_level_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_level_type_id"], name: "index_lecture_functional_types_on_lecture_level_type_id"
  end

  create_table "lecture_level_types", force: :cascade do |t|
    t.string "group"
    t.string "rank"
    t.string "letter"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "lecture_lists", force: :cascade do |t|
    t.string "name"
    t.string "nidn"
    t.string "nip"
    t.string "status"
    t.string "jabatan_struktural"
    t.integer "major_id"
    t.integer "faculty_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lecture_papers", force: :cascade do |t|
    t.string "year"
    t.string "paper"
    t.string "presented_by"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_papers_on_lecture_id"
  end

  create_table "lecture_position_histories", force: :cascade do |t|
    t.string "position"
    t.string "institute"
    t.datetime "start_at"
    t.datetime "end_at"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_position_histories_on_lecture_id"
  end

  create_table "lecture_professional_activities", force: :cascade do |t|
    t.string "year"
    t.string "activity"
    t.string "place"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_professional_activities_on_lecture_id"
  end

  create_table "lecture_profiles", force: :cascade do |t|
    t.bigint "lecture_id"
    t.string "nama"
    t.string "gelar_depan_nama"
    t.string "gelar_belakang_nama"
    t.string "nomor_induk_pegawai"
    t.string "nomor_induk_dosen_negara"
    t.string "nomor_induk_kependudukan"
    t.string "jenis_kelamin"
    t.string "tempat_lahir"
    t.date "tanggal_lahir"
    t.boolean "is_active", default: false
    t.string "lecture_type"
    t.string "lecture_status"
    t.string "marital_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "handphone"
    t.index ["lecture_id"], name: "index_lecture_profiles_on_lecture_id"
  end

  create_table "lecture_researches", force: :cascade do |t|
    t.string "year"
    t.string "research"
    t.string "role"
    t.string "source_funds"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_researches_on_lecture_id"
  end

  create_table "lecture_reviewers", force: :cascade do |t|
    t.string "year"
    t.string "review"
    t.string "publisher"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_reviewers_on_lecture_id"
  end

  create_table "lecture_scientific_works", force: :cascade do |t|
    t.string "year"
    t.string "science_product"
    t.string "publisher"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_scientific_works_on_lecture_id"
  end

  create_table "lecture_scientist_organizations", force: :cascade do |t|
    t.string "year"
    t.string "organization"
    t.string "position"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_scientist_organizations_on_lecture_id"
  end

  create_table "lecture_student_organization_histories", force: :cascade do |t|
    t.string "name"
    t.string "year"
    t.string "role"
    t.string "place"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_student_organization_histories_on_lecture_id"
  end

  create_table "lecture_teaching_histories", force: :cascade do |t|
    t.string "subject"
    t.string "major"
    t.string "institute"
    t.string "semester_status"
    t.string "academic_year"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_teaching_histories_on_lecture_id"
  end

  create_table "lecture_teaching_materials", force: :cascade do |t|
    t.string "subject"
    t.string "major"
    t.string "kind"
    t.string "semester_status"
    t.string "academic_year"
    t.bigint "lecture_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_teaching_materials_on_lecture_id"
  end

  create_table "level_positional_backgrounds", force: :cascade do |t|
    t.string "position"
    t.string "start_from_date"
    t.float "level_incentive"
    t.string "position_level"
    t.string "decree_officer"
    t.string "decree_number"
    t.date "decree_date"
    t.string "referal_rule"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_level_positional_backgrounds_on_user_id"
  end

  create_table "level_positionals", force: :cascade do |t|
    t.string "title"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_level_positionals_on_user_id"
  end

  create_table "lives", force: :cascade do |t|
    t.string "nama"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mahasiswa_profiles", force: :cascade do |t|
    t.integer "mahasiswa_id"
    t.string "email"
    t.string "nama"
    t.string "nim"
    t.string "jenis_kelamin"
    t.string "tempat_lahir"
    t.date "tanggal_lahir"
    t.string "kategori"
    t.bigint "religion_id"
    t.bigint "major_id"
    t.bigint "faculty_id"
    t.string "nik"
    t.string "nisn"
    t.string "npwp"
    t.bigint "state_id"
    t.string "jalan"
    t.string "dusun"
    t.string "rt"
    t.string "rw"
    t.string "kelurahan"
    t.string "kode_pos"
    t.bigint "district_id"
    t.bigint "live_id"
    t.bigint "transportation_id"
    t.string "telepon"
    t.string "handphone"
    t.string "penerima_kps"
    t.string "nomor_kps"
    t.string "nik_ayah"
    t.string "nama_ayah"
    t.string "tanggal_lahir_ayah"
    t.bigint "pendidikan_ayah_id"
    t.bigint "pekerjaan_ayah_id"
    t.bigint "penghasilan_ayah_id"
    t.string "nik_ibu"
    t.string "nama_ibu"
    t.string "tanggal_lahir_ibu"
    t.bigint "pendidikan_ibu_id"
    t.bigint "pekerjaan_ibu_id"
    t.bigint "penghasilan_ibu_id"
    t.string "nama_wali"
    t.string "tanggal_lahir_wali"
    t.bigint "pendidikan_wali_id"
    t.bigint "pekerjaan_wali_id"
    t.bigint "penghasilan_wali_id"
    t.bigint "kebutuhan_khusus_ayah_id"
    t.bigint "kebutuhan_khusus_ibu_id"
    t.bigint "kebutuhan_khusus_mahasiswa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["district_id"], name: "index_mahasiswa_profiles_on_district_id"
    t.index ["faculty_id"], name: "index_mahasiswa_profiles_on_faculty_id"
    t.index ["kebutuhan_khusus_ayah_id"], name: "index_mahasiswa_profiles_on_kebutuhan_khusus_ayah_id"
    t.index ["kebutuhan_khusus_ibu_id"], name: "index_mahasiswa_profiles_on_kebutuhan_khusus_ibu_id"
    t.index ["kebutuhan_khusus_mahasiswa_id"], name: "index_mahasiswa_profiles_on_kebutuhan_khusus_mahasiswa_id"
    t.index ["live_id"], name: "index_mahasiswa_profiles_on_live_id"
    t.index ["mahasiswa_id"], name: "index_mahasiswa_profiles_on_mahasiswa_id"
    t.index ["major_id"], name: "index_mahasiswa_profiles_on_major_id"
    t.index ["pekerjaan_ayah_id"], name: "index_mahasiswa_profiles_on_pekerjaan_ayah_id"
    t.index ["pekerjaan_ibu_id"], name: "index_mahasiswa_profiles_on_pekerjaan_ibu_id"
    t.index ["pekerjaan_wali_id"], name: "index_mahasiswa_profiles_on_pekerjaan_wali_id"
    t.index ["pendidikan_ayah_id"], name: "index_mahasiswa_profiles_on_pendidikan_ayah_id"
    t.index ["pendidikan_ibu_id"], name: "index_mahasiswa_profiles_on_pendidikan_ibu_id"
    t.index ["pendidikan_wali_id"], name: "index_mahasiswa_profiles_on_pendidikan_wali_id"
    t.index ["penghasilan_ayah_id"], name: "index_mahasiswa_profiles_on_penghasilan_ayah_id"
    t.index ["penghasilan_ibu_id"], name: "index_mahasiswa_profiles_on_penghasilan_ibu_id"
    t.index ["penghasilan_wali_id"], name: "index_mahasiswa_profiles_on_penghasilan_wali_id"
    t.index ["religion_id"], name: "index_mahasiswa_profiles_on_religion_id"
    t.index ["state_id"], name: "index_mahasiswa_profiles_on_state_id"
    t.index ["transportation_id"], name: "index_mahasiswa_profiles_on_transportation_id"
  end

  create_table "majors", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.bigint "faculty_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["faculty_id"], name: "index_majors_on_faculty_id"
  end

  create_table "office_positional_backgrounds", force: :cascade do |t|
    t.string "position"
    t.string "duration"
    t.string "office_positional_incentive"
    t.string "office_level"
    t.string "decree_officer"
    t.string "decree_number"
    t.date "decree_date"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_office_positional_backgrounds_on_user_id"
  end

  create_table "office_positionals", force: :cascade do |t|
    t.string "title"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_office_positionals_on_user_id"
  end

  create_table "organization_backrgrounds", force: :cascade do |t|
    t.string "organization_name"
    t.string "position"
    t.string "duration"
    t.string "place"
    t.string "leader"
    t.string "education_period"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_organization_backrgrounds_on_user_id"
  end

  create_table "other_certificate_backgrounds", force: :cascade do |t|
    t.string "other_certificate_name"
    t.string "decree_officer"
    t.string "decree_number"
    t.string "decree_date"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_other_certificate_backgrounds_on_user_id"
  end

  create_table "overseas_visit_backgrounds", force: :cascade do |t|
    t.string "country"
    t.string "visit_for"
    t.string "duration"
    t.string "financial_from"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_overseas_visit_backgrounds_on_user_id"
  end

  create_table "parents", force: :cascade do |t|
    t.string "nama_ayah"
    t.string "pekerjaan_ayah"
    t.string "parents_guardians_handphone"
    t.string "nama_ibu"
    t.string "pekerjaan_ibu"
    t.string "nama_wali"
    t.string "pekerjaan_wali"
    t.bigint "student_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["student_id"], name: "index_parents_on_student_id"
  end

  create_table "payment_forms", force: :cascade do |t|
    t.string "kategori"
    t.text "file_payment_data"
    t.string "user_type"
    t.bigint "users_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["users_id"], name: "index_payment_forms_on_users_id"
  end

  create_table "payments", force: :cascade do |t|
    t.string "kind"
    t.bigint "academic_year_id", null: false
    t.float "in_number"
    t.string "in_word"
    t.integer "student_id", null: false
    t.bigint "bank_id", null: false
    t.integer "approval_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "paid"
    t.string "type_student"
    t.boolean "is_approved", default: false
    t.index ["academic_year_id"], name: "index_payments_on_academic_year_id"
    t.index ["bank_id"], name: "index_payments_on_bank_id"
  end

  create_table "physical_characteristics", force: :cascade do |t|
    t.string "body_height"
    t.string "body_weight"
    t.string "hair_kind"
    t.string "face_shape"
    t.string "skin_color"
    t.string "typical_caracter"
    t.string "disability"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_physical_characteristics_on_user_id"
  end

  create_table "position_histories", force: :cascade do |t|
    t.bigint "position_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["position_id"], name: "index_position_histories_on_position_id"
  end

  create_table "positions", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "structural_position_id"
    t.string "structural_position_type"
    t.string "role"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_positions_on_user_id"
  end

  create_table "prodis", force: :cascade do |t|
    t.uuid "id_prodi"
    t.string "kode_program_studi"
    t.string "nama_program_studi"
    t.string "status"
    t.integer "jenjang_didik_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "professional_trainings", force: :cascade do |t|
    t.string "year"
    t.string "training"
    t.string "presented_by"
    t.string "terms"
    t.integer "training_professional_id"
    t.string "training_professional_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "religions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schedule_subjects", force: :cascade do |t|
    t.string "hari"
    t.string "jam"
    t.bigint "subject_id"
    t.bigint "dummy_lecture_1_id"
    t.bigint "dummy_lecture_2_id"
    t.string "ruang"
    t.bigint "major_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dummy_lecture_1_id"], name: "index_schedule_subjects_on_dummy_lecture_1_id"
    t.index ["dummy_lecture_2_id"], name: "index_schedule_subjects_on_dummy_lecture_2_id"
    t.index ["major_id"], name: "index_schedule_subjects_on_major_id"
    t.index ["subject_id"], name: "index_schedule_subjects_on_subject_id"
  end

  create_table "schools", force: :cascade do |t|
    t.string "nama"
    t.string "jurusan"
    t.bigint "student_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["student_id"], name: "index_schools_on_student_id"
  end

  create_table "special_needs", force: :cascade do |t|
    t.string "nama"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "staf_tata_usaha_profiles", force: :cascade do |t|
    t.string "nama"
    t.string "staf_tata_usaha_id"
    t.string "tugas"
    t.integer "no_telepon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "staff_profiles", force: :cascade do |t|
    t.string "nama"
    t.string "gelar_depan_nama"
    t.string "gelar_belakang_nama"
    t.string "nomor_induk_pegawai"
    t.string "nomor_induk_dosen_negara"
    t.string "nomor_induk_kependudukan"
    t.string "jenis_kelamin"
    t.string "tempat_lahir"
    t.datetime "tanggal_lahir"
    t.string "status"
    t.string "status_perkawinan"
    t.string "handphone"
    t.string "kewarganegaraan"
    t.bigint "staff_id"
    t.index ["staff_id"], name: "index_staff_profiles_on_staff_id"
  end

  create_table "states", force: :cascade do |t|
    t.string "nama"
    t.string "kode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "student_profiles", force: :cascade do |t|
    t.integer "student_id"
    t.string "nama"
    t.string "nomor_induk_mahasiswa"
    t.string "jenis_kelamin"
    t.string "tempat_lahir"
    t.datetime "tanggal_lahir"
    t.string "handphone"
    t.string "agama"
    t.string "kewarganegaraan"
    t.bigint "major_id"
    t.string "status"
    t.string "keterangan_tinggal"
    t.boolean "is_active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "angkatan"
    t.string "konsentrasi"
    t.string "tahun_berlaku_matakuliah"
    t.index ["major_id"], name: "index_student_profiles_on_major_id"
  end

  create_table "studentletters", force: :cascade do |t|
    t.string "nama"
    t.string "email"
    t.string "nomor_induk_mahasiswa"
    t.string "program_studi"
    t.string "tempat_lahir"
    t.date "tanggal_lahir"
    t.string "alamat"
    t.string "telepon"
    t.string "handphone"
    t.string "tanggal_yudisium"
    t.string "judul_skripsi"
    t.string "nama_orang_tua"
    t.string "tempat_kerja"
    t.string "pangkat_golongan"
    t.string "kategori_nomor_induk_instansi"
    t.string "nomor_induk_instansi"
    t.string "telepon_orangtua"
    t.string "jenis_surat"
    t.string "keterangan_pindah"
    t.string "diketahui_kepala_tata_usaha"
    t.date "tanggal_diketahui_kepala_tata_usaha"
    t.string "kepala_tata_usaha"
    t.string "nomor_surat"
    t.string "nomor_ijazah_fakultas"
    t.string "nomor_ijazah_universitas"
    t.string "kepada"
    t.string "casu_quo_kepada"
    t.string "jenis_surat_lainnya"
    t.text "foto_ijazah_data"
    t.text "foto_bukti_pembayaran_semester_berjalan_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "study_plans", id: false, force: :cascade do |t|
    t.string "academic_year_id"
    t.bigint "student_id"
    t.bigint "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["student_id"], name: "index_study_plans_on_student_id"
    t.index ["subject_id"], name: "index_study_plans_on_subject_id"
  end

  create_table "subject_plans", force: :cascade do |t|
    t.integer "academic_year_id"
    t.bigint "student_id"
    t.boolean "is_approved", default: false
    t.boolean "is_passed", default: false
    t.boolean "is_printed", default: false
    t.integer "stuctural_id"
    t.float "last_grade_poin_average"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "penasehat_akademik"
    t.string "nip_nidn_penasehat_akademik"
    t.string "dekan"
    t.string "nip_nidn_dekan"
    t.index ["academic_year_id"], name: "index_subject_plans_on_academic_year_id"
    t.index ["student_id"], name: "index_subject_plans_on_student_id"
  end

  create_table "subjectplans_subjects", id: false, force: :cascade do |t|
    t.bigint "subject_plan_id"
    t.bigint "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subject_id"], name: "index_subjectplans_subjects_on_subject_id"
    t.index ["subject_plan_id"], name: "index_subjectplans_subjects_on_subject_plan_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "matakuliah"
    t.string "kode"
    t.integer "kredit"
    t.string "semester"
    t.string "kategori"
    t.string "tahun_berlaku"
    t.string "khusus"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "faculty"
    t.string "major"
    t.string "tags", array: true
    t.index ["tags"], name: "index_subjects_on_tags", using: :gin
  end

  create_table "transportations", force: :cascade do |t|
    t.string "nama"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "universities", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.bigint "foundation_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "alamat"
    t.string "telepon"
    t.index ["foundation_id"], name: "index_universities_on_foundation_id"
  end

  create_table "university_structurals", force: :cascade do |t|
    t.date "start_at"
    t.date "end_at"
    t.boolean "is_active", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.text "image_data"
    t.string "type"
    t.string "provider"
    t.string "uid"
    t.string "name"
    t.date "birth_date"
    t.string "birth_place"
    t.string "gender"
    t.string "religion"
    t.string "marital_status"
    t.string "hobby"
    t.string "stambuk"
    t.boolean "is_student", default: false
    t.boolean "is_graduate"
    t.boolean "is_lecture"
    t.boolean "is_staff"
    t.string "access_token"
    t.string "nidn"
    t.string "nip"
    t.bigint "dummy_lecture_1_id"
    t.bigint "dummy_lecture_2_id"
    t.bigint "major_id"
    t.bigint "faculty_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "blangko_data"
    t.integer "academic_year_id"
    t.integer "sortlist"
    t.boolean "is_active", default: true
    t.string "phone_number"
    t.string "authy_id"
    t.datetime "last_sign_in_with_authy"
    t.boolean "authy_enabled", default: false
    t.boolean "is_phone_verified", default: false
    t.boolean "is_alumni", default: false
    t.bigint "religion_id"
    t.bigint "lecture_functional_type_id"
    t.string "referral"
    t.index ["access_token"], name: "index_users_on_access_token", unique: true
    t.index ["authy_id"], name: "index_users_on_authy_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["dummy_lecture_1_id"], name: "index_users_on_dummy_lecture_1_id"
    t.index ["dummy_lecture_2_id"], name: "index_users_on_dummy_lecture_2_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["faculty_id"], name: "index_users_on_faculty_id"
    t.index ["lecture_functional_type_id"], name: "index_users_on_lecture_functional_type_id"
    t.index ["major_id"], name: "index_users_on_major_id"
    t.index ["nidn"], name: "index_users_on_nidn", unique: true
    t.index ["religion_id"], name: "index_users_on_religion_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["stambuk"], name: "index_users_on_stambuk", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "admin_profiles", "users"
  add_foreign_key "appreciation_backgrounds", "users"
  add_foreign_key "course_backgrounds", "users"
  add_foreign_key "educational_backgrounds", "users"
  add_foreign_key "essay_documents", "essays"
  add_foreign_key "family_trees", "users"
  add_foreign_key "level_positional_backgrounds", "users"
  add_foreign_key "level_positionals", "users"
  add_foreign_key "office_positional_backgrounds", "users"
  add_foreign_key "office_positionals", "users"
  add_foreign_key "organization_backrgrounds", "users"
  add_foreign_key "other_certificate_backgrounds", "users"
  add_foreign_key "overseas_visit_backgrounds", "users"
  add_foreign_key "payments", "academic_years"
  add_foreign_key "payments", "banks"
  add_foreign_key "physical_characteristics", "users"
  add_foreign_key "users", "lecture_functional_types"
  add_foreign_key "users", "religions"
end
