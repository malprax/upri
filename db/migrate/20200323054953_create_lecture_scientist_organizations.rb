class CreateLectureScientistOrganizations < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_scientist_organizations do |t|
      t.string :year
      t.string :organization
      t.string :position
      t.belongs_to :lecture

      t.timestamps
    end
  end
end
