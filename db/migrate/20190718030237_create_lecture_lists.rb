class CreateLectureLists < ActiveRecord::Migration[5.2]
  def change
    create_table :lecture_lists do |t|
      t.string :name
      t.string :nidn
      t.string :nip
      t.string :status
      t.string :jabatan_struktural
      t.integer :major_id
      t.integer :faculty_id

      t.timestamps
    end
  end
end
