class CreateEssays < ActiveRecord::Migration[5.1]
  def change
    create_table :essays do |t|
      t.string :title
      t.references :academic_year, index: true
      t.references :graduate, index: true
      # t.references :lecture, index: true
      t.references :dummy_lecture_1, index: true
      t.references :dummy_lecture_2, index: true
      t.timestamps
    end
  end
end
