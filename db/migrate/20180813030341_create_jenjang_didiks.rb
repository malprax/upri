class CreateJenjangDidiks < ActiveRecord::Migration[5.1]
  def down
    create_table :jenjang_didiks do |t|
      t.string :nama
      t.timestamps
    end
  end
end
