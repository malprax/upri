class CreateDocuments < ActiveRecord::Migration[5.1]
  def change
    create_table :documents do |t|
      t.string :name
      t.integer :document_file_id
      t.string :document_file_type
      t.timestamps
    end
  end
end
