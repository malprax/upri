class CreateLectureLevelTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_level_types do |t|
      t.string :group
      t.string :rank
      t.string :letter
      t.timestamps
    end
  end
end
