class CreateStafTataUsahaProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :staf_tata_usaha_profiles do |t|
      t.string :nama
      t.string :staf_tata_usaha_id
      t.string :tugas
      t.integer :no_telepon

      t.timestamps
    end
  end
end
