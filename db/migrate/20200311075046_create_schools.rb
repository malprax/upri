class CreateSchools < ActiveRecord::Migration[5.2]
  def change
    create_table :schools do |t|
      t.string :nama
      t.string :jurusan
      t.belongs_to :student
      t.timestamps
    end
  end
end
