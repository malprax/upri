class CreateLectureProfessionalActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_professional_activities do |t|
      t.string :year
      t.string :activity
      t.string :place
      t.belongs_to :lecture

      t.timestamps
    end
  end
end
