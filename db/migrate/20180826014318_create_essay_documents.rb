class CreateEssayDocuments < ActiveRecord::Migration[5.1]
  def change
    create_table :essay_documents do |t|
      t.string :kategori
      t.text :document_upload_data
      t.belongs_to :essay, foreign_key: true

      t.timestamps
    end
  end
end
