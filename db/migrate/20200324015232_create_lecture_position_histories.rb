class CreateLecturePositionHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_position_histories do |t|
      t.string :position
      t.string :institute
      t.datetime :start_at
      t.datetime :end_at
      t.belongs_to :lecture
      t.timestamps
    end
  end
end
