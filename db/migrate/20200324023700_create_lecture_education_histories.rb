class CreateLectureEducationHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_education_histories do |t|
      t.string :level
      t.string :major
      t.string :institute
      t.string :graduation_year
      t.belongs_to :lecture
      t.timestamps
    end
  end
end
