class CreateCampusNewspapers < ActiveRecord::Migration[5.1]
  def change
    create_table :campus_newspapers do |t|
      t.string :judul
      t.text :isi
      t.string :kategori
      t.integer :user_id
      t.string :posisi
      t.boolean :terbit
      t.text :image_data
      t.timestamps
    end
  end
end
