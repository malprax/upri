class CreateBookCollections < ActiveRecord::Migration[5.1]
  def change
    create_table :book_collections do |t|
      t.string :kode
      t.string :judul
      t.string :penulis
      t.string :penerbit
      t.text :cover_buku_data
      t.string :kategori
      t.string :posisi

      t.timestamps
    end
  end
end
