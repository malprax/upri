class CreateCandidateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :candidate_students do |t|
      t.string :registration_number
      t.string :class_plan
      t.string :major_1
      t.string :major_2
      t.string :name
      t.date :birth_date
      t.string :birth_place
      t.string :gender
      t.string :phone_number
      t.string :street_address
      t.string :hamlet
      t.string :neighbourhood
      t.string :urban_village
      t.string :sub_district
      t.string :district
      t.string :province
      t.integer :postal_code
      t.string :school
      t.string :school_address
      t.string :school_plan
      t.string :nisn
      t.string :sertificate_number
      t.string :sertificate_date
      t.string :national_exam_score
      t.string :father_name
      t.string :father_job
      t.string :father_salary
      t.string :father_education
      t.string :mother_name
      t.string :mother_job
      t.string :mother_salary
      t.string :mother_education
      t.string :know_from
      t.integer :academic_year_id
      t.string :referral
      t.string :email
      t.boolean :approve, default: false

      t.timestamps
    end
  end
end
