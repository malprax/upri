class CreateLectureFunctionalTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_functional_types do |t|
      t.string :name
      t.string :role
      t.integer :credit
      t.belongs_to :lecture_level_type
      t.timestamps
    end
  end
end
