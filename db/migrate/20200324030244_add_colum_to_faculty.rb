class AddColumToFaculty < ActiveRecord::Migration[6.0]
  def change
    add_column :faculties, :university_id, :integer, foreign_key: true
  end
end
