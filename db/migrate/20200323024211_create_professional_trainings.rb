class CreateProfessionalTrainings < ActiveRecord::Migration[6.0]
  def change
    create_table :professional_trainings do |t|
      t.string :year
      t.string :training
      t.string :presented_by
      t.string :terms
      t.integer :training_professional_id
      t.string :training_professional_type

      t.timestamps
    end
  end
end
