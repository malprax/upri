class CreateItemAccreditations < ActiveRecord::Migration[5.1]
  def change
    create_table :item_accreditations do |t|
      t.string :item
      t.integer :academic_year_id

      t.timestamps
    end
  end
end
