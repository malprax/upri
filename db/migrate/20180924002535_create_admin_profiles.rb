class CreateAdminProfiles < ActiveRecord::Migration[5.1]
  def down
    create_table :admin_profiles do |t|
      t.references :user, index: true
      t.timestamps
    end
  end
end
