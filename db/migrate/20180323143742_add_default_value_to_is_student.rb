class AddDefaultValueToIsStudent < ActiveRecord::Migration[5.1]
  def up
  change_column :users, :is_student, :boolean, default: false
  end

  def down
    change_column :users, :is_student, :boolean, default: nil
  end
end
