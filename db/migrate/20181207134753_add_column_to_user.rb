class AddColumnToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :is_phone_verified, :boolean, default: FALSE
    add_column :users, :is_alumni, :boolean, default: FALSE
  end
end
