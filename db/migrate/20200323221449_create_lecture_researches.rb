class CreateLectureResearches < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_researches do |t|
      t.string :year
      t.string :research
      t.string :role
      t.string :source_funds
      t.belongs_to :lecture

      t.timestamps
    end
  end
end
