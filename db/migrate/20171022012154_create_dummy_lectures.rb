class CreateDummyLectures < ActiveRecord::Migration[5.1]
  def change
    create_table :dummy_lectures do |t|
      t.string :nama
      t.integer :nidn
      t.integer :nip
      t.string :gelar
      t.string :jabatan_akademik
      t.string :pendidikan
      t.references :major, index: true

      t.timestamps
    end
  end
end
