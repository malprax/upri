class CreateDistricts < ActiveRecord::Migration[5.1]
  def change
    create_table :districts do |t|
      t.string :nama
      t.integer :state_id
      t.string :id_wilayah
      t.string :id_negara

      t.timestamps
    end
  end
end
