class CreatePositionHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :position_histories do |t|
      t.belongs_to :position

      t.timestamps
    end
  end
end
