class CreateLectureTeachingMaterials < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_teaching_materials do |t|
      t.string :subject
      t.string :major
      t.string :kind
      t.string :semester_status
      t.string :academic_year
      t.belongs_to :lecture

      t.timestamps
    end
  end
end
