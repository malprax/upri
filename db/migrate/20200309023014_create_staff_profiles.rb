class CreateStaffProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :staff_profiles do |t|
      t.string  :nama
      t.string  :gelar_depan_nama
      t.string  :gelar_belakang_nama
      t.string  :nomor_induk_pegawai
      t.string  :nomor_induk_dosen_negara
      t.string  :nomor_induk_kependudukan
      t.string  :jenis_kelamin
      t.string  :tempat_lahir
      t.datetime :tanggal_lahir
      t.string :status
      t.string :status_perkawinan
      t.string :handphone
      t.string :kewarganegaraan
      t.references :staff
    end
  end
end
