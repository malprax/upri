class CreatePositions < ActiveRecord::Migration[6.0]
  def change
    create_table :positions do |t|
      t.belongs_to :user
      t.integer :structural_position_id
      t.string :structural_position_type
      t.string :role
      t.timestamps
    end
  end
end
