class AddTagsToSubjects < ActiveRecord::Migration[6.0]
  def change
    add_column :subjects, :faculty, :string
    add_column :subjects, :major, :string
    add_column :subjects, :tags, :string, array: true
    add_index :subjects, :tags, using: 'gin'
  end
end
