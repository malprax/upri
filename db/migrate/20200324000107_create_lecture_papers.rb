class CreateLecturePapers < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_papers do |t|
      t.string :year
      t.string :paper
      t.string :presented_by
      t.belongs_to :lecture

      t.timestamps
    end
  end
end
