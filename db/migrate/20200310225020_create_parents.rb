class CreateParents < ActiveRecord::Migration[5.2]
  def change
    create_table :parents do |t|
      t.string :nama_ayah
      t.string :pekerjaan_ayah
      t.string :parents_guardians_handphone
      t.string :nama_ibu
      t.string :pekerjaan_ibu
      t.string :nama_wali
      t.string :pekerjaan_wali
      t.belongs_to :student
      t.timestamps
    end
  end
end
