class AddDescriptionToJournals < ActiveRecord::Migration[6.0]
  def change
    add_column :journals, :description, :text
  end
end
