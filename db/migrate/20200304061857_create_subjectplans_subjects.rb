class CreateSubjectplansSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :subjectplans_subjects, id: false do |t|
      t.belongs_to :subject_plan
      t.belongs_to :subject
      t.timestamps
    end
  end
end
