class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :rt
      t.string :rw
      t.string :kelurahan
      t.string :kecamatan
      t.string :city
      t.string :province
      t.integer :post_code
      t.belongs_to :user
      t.timestamps
    end
  end
end
