class CreateMahasiswaProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :mahasiswa_profiles do |t|
      t.integer :mahasiswa_id, index: true, foreign_key: true
      t.string :email
      t.string :nama
      t.string :nim
      t.string :jenis_kelamin
      t.string :tempat_lahir
      t.date :tanggal_lahir
      t.string :kategori
      t.belongs_to :religion, index: true
      t.belongs_to :major, index: true
      t.belongs_to :faculty, index: true

      t.string :nik
      t.string :nisn
      t.string :npwp
      t.belongs_to :state, index: true

      t.string :jalan
      t.string :dusun
      t.string :rt
      t.string :rw
      t.string :kelurahan
      t.string :kode_pos
      t.belongs_to :district, index: true
      t.belongs_to :live, index: true
      t.belongs_to :transportation, index: true

      t.string :telepon
      t.string :handphone
      t.string :penerima_kps
      t.string :nomor_kps
      t.string :nik_ayah
      t.string :nama_ayah
      t.string :tanggal_lahir_ayah

      t.belongs_to :pendidikan_ayah, index: true
      t.belongs_to :pekerjaan_ayah, index: true
      t.belongs_to :penghasilan_ayah, index: true


      t.string :nik_ibu
      t.string :nama_ibu
      t.string :tanggal_lahir_ibu

      t.belongs_to :pendidikan_ibu, index: true
      t.belongs_to :pekerjaan_ibu, index: true
      t.belongs_to :penghasilan_ibu, index: true

      t.string :nama_wali
      t.string :tanggal_lahir_wali

      t.belongs_to :pendidikan_wali, index: true
      t.belongs_to :pekerjaan_wali, index: true
      t.belongs_to :penghasilan_wali, index: true

      t.belongs_to :kebutuhan_khusus_ayah, index: true
      t.belongs_to :kebutuhan_khusus_ibu, index: true
      t.belongs_to :kebutuhan_khusus_mahasiswa, index: true

      t.timestamps
    end
  end
end
