class AddFieldToAcademicYears < ActiveRecord::Migration[5.2]
  def change
    add_column :academic_years, :is_active, :boolean, default: false
  end
end
