class AddStatusToDummyLectures < ActiveRecord::Migration[5.1]
  def change
    add_column :dummy_lectures, :status, :string
  end
end
