class CreateLectureAppreciations < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_appreciations do |t|
      t.string :year
      t.string :appreciation
      t.string :presented_by
      t.belongs_to :lecture
      t.timestamps
    end
  end
end
