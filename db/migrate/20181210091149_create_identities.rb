class CreateIdentities < ActiveRecord::Migration[5.1]
  def change
    create_table :identities do |t|
      t.string :kategori
      t.text :file_identity_data
      t.references :user
      t.string :user_type
      t.timestamps
    end
  end
end
