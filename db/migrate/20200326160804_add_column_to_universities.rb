class AddColumnToUniversities < ActiveRecord::Migration[6.0]
  def change
    add_column :universities, :alamat, :string
    add_column :universities, :telepon, :string
  end
end
