class CreateLectureScientificWorks < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_scientific_works do |t|
      t.string :year
      t.string :science_product
      t.string :publisher
      t.belongs_to :lecture

      t.timestamps
    end
  end
end
