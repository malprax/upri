class CreateBackupGraduates < ActiveRecord::Migration[5.2]
  def change
    create_table :backup_graduates do |t|
      t.string :email
      t.string :name
      t.bigint :dummy_lecture_1_id
      t.bigint :dummy_lecture_2_id
      t.bigint :major_id
      t.bigint :faculty_id
      t.integer :academic_year_id
      t.string :phone_number
      t.text :image_data
      t.date :birth_date
      t.string :birth_place
      t.string :gender
      t.string :religion
      t.string :marital_status
      t.string :hobby
      t.string :stambuk
      t.string :nidn
      t.string :nip
      t.text :blangko_data
      t.integer :sortlist
      t.float :ipk
      t.date :yudisium_date
      t.string :yudisium_letter
      t.integer :alumni_number
      t.string :predicet_of_yudisium
      t.string :essay_title
      t.timestamps
    end
  end
end
