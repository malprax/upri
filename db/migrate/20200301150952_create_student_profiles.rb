class CreateStudentProfiles < ActiveRecord::Migration[5.2]
  def change
     create_table :student_profiles do |t|
      t.integer :student_id
      t.string :nama
      t.string :nomor_induk_mahasiswa
      t.string :jenis_kelamin
      t.string :tempat_lahir
      t.datetime :tanggal_lahir
      t.string :handphone
      t.string :agama
      t.string :kewarganegaraan
      t.references :major
      t.string :status
      t.string :keterangan_tinggal
      t.boolean :is_active, default: false
      t.timestamps
     end
  end
end
