class CreateSubjectPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :subject_plans do |t|
      t.integer :academic_year_id
      t.belongs_to :student
      t.boolean :is_approved, default: false
      t.boolean :is_passed, default: false
      t.boolean :is_printed, default: false
      t.integer :stuctural_id
      t.float :last_grade_poin_average
      t.timestamps
    end
  end
end
