class CreateGraduateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :graduate_profiles do |t|
      t.float :ipk
      t.date :yudisium_date
      t.string :yudisium_letter
      t.integer :alumni_number
      t.string :predicet_of_yudisium
      t.references :graduate, index: true
      t.timestamps
    end
  end
end
