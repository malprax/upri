class CreateLectureProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :lecture_profiles do |t|
      t.references :lecture
      t.string :nama
      t.string :gelar_depan_nama
      t.string :gelar_belakang_nama
      t.string :nomor_induk_pegawai
      t.string :nomor_induk_dosen_negara
      t.string :nomor_induk_kependudukan
      t.string :jenis_kelamin
      t.string :tempat_lahir
      t.date :tanggal_lahir
      t.boolean :is_active, default: false
      t.string :lecture_type
      t.string :lecture_status
      t.string :marital_status

      t.timestamps
    end
  end
end
