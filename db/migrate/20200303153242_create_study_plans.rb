class CreateStudyPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :study_plans, id: false do |t|
      t.string :academic_year_id
      t.belongs_to :student, index: true
      t.belongs_to :subject, index: true
      t.timestamps
    end
  end
end
