class CreateFamilyTrees < ActiveRecord::Migration[5.1]
  def change
    create_table :family_trees do |t|
      t.string :family_name
      t.string :family_status
      t.string :gender
      t.string :birth_date
      t.string :birth_place
      t.string :job
      t.string :info
      t.string :address
      t.string :phone
      t.string :office_name
      t.string :office_registration_number
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
