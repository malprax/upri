class CreateLectureReviewers < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_reviewers do |t|
      t.string :year
      t.string :review
      t.string :publisher
      t.belongs_to :lecture
      t.timestamps
    end
  end
end
