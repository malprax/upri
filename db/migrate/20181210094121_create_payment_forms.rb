class CreatePaymentForms < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_forms do |t|
      t.string :kategori
      t.text :file_payment_data
      t.string :user_type
      t.references :users

      t.timestamps
    end
  end
end
