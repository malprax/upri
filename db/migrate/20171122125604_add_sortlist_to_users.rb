class AddSortlistToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :sortlist, :integer
  end
end
