class CreateLectureTeachingHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_teaching_histories do |t|
      t.string :subject
      t.string :major
      t.string :institute
      t.string :semester_status
      t.string :academic_year
      t.belongs_to :lecture

      t.timestamps
    end
  end
end
