class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.string :kind
      t.references :academic_year, null: false, foreign_key: true
      t.float :in_number
      t.string :in_word
      t.integer :student_id, null: false, foreign_key: true
      t.references :bank, null: false, foreign_key: true
      t.boolean :is_approve
      t.integer :approval_id
      t.timestamps
    end
  end
end
