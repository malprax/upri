class CreateLectureStudentOrganizationHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_student_organization_histories do |t|
      t.string :name
      t.string :year
      t.string :role
      t.string :place
      t.belongs_to :lecture
      t.timestamps
    end
  end
end
