class ChangeColumnNameDummyLecture < ActiveRecord::Migration[5.1]
  def change
    # t.string :nama
    # t.integer :nidn
    # t.integer :nip
    # t.string :gelar
    # t.string :jabatan_akademik
    # t.string :pendidikan
    # t.references :major, index: true
    rename_column :dummy_lectures, :gelar, :gelar_depan_nama
    add_column :dummy_lectures, :gelar_belakang_nama, :string
  end
end
