class CreateSubjects < ActiveRecord::Migration[5.1]
  def change
    create_table :subjects do |t|
      t.string :matakuliah
      t.string :kode
      t.integer :kredit
      t.string :semester
      t.string :kategori
      t.string :tahun_berlaku
      t.string :khusus
      t.timestamps
    end
  end
end
