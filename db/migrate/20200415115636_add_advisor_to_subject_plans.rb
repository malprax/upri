class AddAdvisorToSubjectPlans < ActiveRecord::Migration[6.0]
  def change
    add_column :subject_plans, :penasehat_akademik, :string
    add_column :subject_plans, :nip_nidn_penasehat_akademik, :string
    add_column :subject_plans, :dekan, :string
    add_column :subject_plans, :nip_nidn_dekan, :string
    add_index :subject_plans, :academic_year_id
  end
end
