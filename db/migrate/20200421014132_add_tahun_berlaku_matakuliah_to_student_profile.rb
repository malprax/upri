class AddTahunBerlakuMatakuliahToStudentProfile < ActiveRecord::Migration[6.0]
  def change
    add_column :student_profiles, :tahun_berlaku_matakuliah, :string
  end
end
