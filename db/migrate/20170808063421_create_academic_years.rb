class CreateAcademicYears < ActiveRecord::Migration[5.1]
  def change
    create_table :academic_years do |t|
      t.string  :nama
      t.string :kategori
      t.date :tanggal_mulai
      t.date :tanggal_akhir
      t.boolean :status, default: true
      t.timestamps
    end
  end
end
