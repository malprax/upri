class CreateJournals < ActiveRecord::Migration[6.0]
  def change
    create_table :journals do |t|
      t.string :volume
      t.integer :number
      t.boolean :published, default: false
      t.string :issn

      t.timestamps
    end
  end
end
