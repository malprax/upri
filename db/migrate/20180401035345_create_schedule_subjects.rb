class CreateScheduleSubjects < ActiveRecord::Migration[5.1]
  def change
    create_table :schedule_subjects do |t|
      t.string :hari
      t.string :jam
      t.references :subject, index: true
      t.references :dummy_lecture_1, index: true
      t.references :dummy_lecture_2, index: true
      t.string :ruang
      t.references :major, index: true
      t.timestamps
    end
  end
end
