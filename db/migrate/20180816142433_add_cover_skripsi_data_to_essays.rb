class AddCoverSkripsiDataToEssays < ActiveRecord::Migration[5.1]
  def change
    add_column :essays, :cover_skripsi_data, :text
    add_column :essays, :posisi, :string
    add_column :essays, :kode, :string
  end
end
