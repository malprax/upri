class AddColumnToUsers < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :religion, foreign_key: true
    add_reference :users, :lecture_functional_type, foreign_key: true
  end
end
