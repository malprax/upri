class CreateUniversityStructurals < ActiveRecord::Migration[6.0]
  def change
    create_table :university_structurals do |t|
      t.date :start_at
      t.date :end_at
      t.boolean :is_active, default: false

      t.timestamps
    end
  end
end
