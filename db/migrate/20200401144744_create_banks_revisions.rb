class CreateBanksRevisions < ActiveRecord::Migration[6.0]
  def change
    create_table :banks do |t|
      t.string :name
      t.string :address
      t.string :company
      t.string :account_name
      t.string :account_number
      t.string :code
      t.timestamps
    end
  end
end
