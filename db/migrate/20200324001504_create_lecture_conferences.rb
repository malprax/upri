class CreateLectureConferences < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_conferences do |t|
      t.string :year
      t.string :conference
      t.string :presented_by
      t.string :role
      t.belongs_to :lecture

      t.timestamps
    end
  end
end
