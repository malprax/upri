class CreateSpecialNeeds < ActiveRecord::Migration[5.1]
  def change
    create_table :special_needs do |t|
      t.string :nama

      t.timestamps
    end
  end
end
