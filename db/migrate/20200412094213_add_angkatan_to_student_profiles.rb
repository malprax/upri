class AddAngkatanToStudentProfiles < ActiveRecord::Migration[6.0]
  def change
    add_column :student_profiles, :angkatan, :string
    add_column :student_profiles, :konsentrasi, :string
  end
end
