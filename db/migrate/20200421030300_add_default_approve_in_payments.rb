class AddDefaultApproveInPayments < ActiveRecord::Migration[6.0]
  def change
    remove_column :payments, :is_approve, :boolean
    add_column :payments, :is_approved, :boolean, default: false
  end
end
