class CreateStudentletters < ActiveRecord::Migration[5.1]
  def change
    create_table :studentletters do |t|
      t.string :nama
      t.string :email
      t.string :nomor_induk_mahasiswa
      t.string :program_studi
      t.string :tempat_lahir
      t.date :tanggal_lahir
      t.string :alamat
      t.string :telepon
      t.string :handphone
      t.string :tanggal_yudisium
      t.string :judul_skripsi
      t.string :nama_orang_tua
      t.string :tempat_kerja
      t.string :pangkat_golongan
      t.string :kategori_nomor_induk_instansi
      t.string :nomor_induk_instansi
      t.string :telepon_orangtua
      t.string :jenis_surat
      t.string :keterangan_pindah
      t.string :diketahui_kepala_tata_usaha
      t.date :tanggal_diketahui_kepala_tata_usaha
      t.string :kepala_tata_usaha
      t.string :nomor_surat
      t.string :nomor_ijazah_fakultas
      t.string :nomor_ijazah_universitas
      t.string :kepada
      t.string :casu_quo_kepada
      t.string :jenis_surat_lainnya
      t.text :foto_ijazah_data
      t.text :foto_bukti_pembayaran_semester_berjalan_data
      t.timestamps
    end
  end
end
