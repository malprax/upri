class AddReferralToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :referral, :string
  end
end
