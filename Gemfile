source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

ruby "2.7.0"

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.2.1'
# Use sqlite3 as the database for Active Record
gem 'pg', '~> 1.2.3'
# Use Puma as the app server
gem 'puma', '~> 3.7'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 5.0.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
# gem 'capistrano', '~> 3.11'
# gem 'capistrano-rails', '~> 1.4'
# gem 'capistrano-passenger', '~> 0.2.0'
# gem 'capistrano-rbenv', '~> 2.1', '>= 2.1.4'


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  # gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
# gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

#custom gem write here below
#very important gem
gem 'devise'
# gem 'cancancan'
# gem 'bootstrap', '~> 4.0.0.beta'

#pagination gem
gem 'kaminari'

#handling form
gem 'simple_form'
gem 'nested_form', '~> 0.3.2' #nested_form
# gem "cocoon" #nested_form

#handling login via social media
gem "omniauth-oauth2", '~> 1.6.0'
gem "omniauth-google-oauth2", '~> 0.8.0'
gem "omniauth-facebook", '~> 6.0.0'
#convert to pdf
# gem 'prawn', '~> 2.2.2'
# gem 'prawn-table', '~> 0.2.2'
gem 'wicked_pdf', '~> 2.0.2'
gem 'wkhtmltopdf-binary', '~> 0.12.5.4'
# gem 'wkhtmltopdf-heroku', '2.12.5.0'

#annotate
gem 'annotate', group: 'development'

#ip to map
# gem 'iptoearth', git: 'https://github.com/mbuckbee/Ip-To-Earth-Gem.git'

#font awesome rails
gem 'font_awesome5_rails'
# gem "font-awesome-rails"

#new_relic monitoring
# gem 'newrelic_rpm'

#tidak perlu di tulis disini
# gem 'mailcatcher'
# gem 'heroku'

#using JavaScript
# gem 'webpacker', git: 'https://github.com/rails/webpacker.git'
gem 'jquery-rails'

#uploader image_tag
# gem 'google-api-client'

# Shrine
gem 'shrine', '~> 2.7'
gem 'aws-sdk-s3', '~> 1.2'   # for S3 storage
gem 'sucker_punch', '~> 2.0' # for backgrounding plugin

#for creating thumb image
gem 'image_processing', '~>1.10.3'
# gem 'mini_magick'

#image store_dimensions with plugin store_dimensions in image_uploader.rb
# gem 'fastimage', '~>2.1.7'

#handling error 743 from shrine cache
# gem 'json'

#gem strong_migrations
# gem 'strong_migrations'

#excel
gem 'rubyzip', '~> 1.1.0'
gem 'axlsx', '2.1.0.pre'
gem 'axlsx_rails', '~>0.5.2'

#phone verification
gem 'authy', '~> 2.7.5'

#provide support for Cross-Origin Resource Sharing
gem 'rack-cors', require: 'rack/cors'

#scraping data
# gem 'mechanize'

gem 'bullet', group: 'development'

gem "select2-rails"
# gem 'rack-profiler', group: 'development'

#RECAPTCHA
gem "recaptcha", require: "recaptcha/rails"

# error readline
gem "rb-readline", '~> 0.5.5'

#bulma
gem "bulma-rails", "~> 0.7.5"

#javascript notifications
gem 'toastr-rails', '~> 1.0.3'

#bootsnap
# gem 'bootsnap', require: false

#email opener
# gem "letter_opener", :group => :development

#rqrcode
gem 'rqrcode', '~> 1.1.2'
gem 'barby', '~> 0.6.8'
gem 'has_barcode', '~> 0.2.3'
gem 'chunky_png', '~> 1.3', '>= 1.3.5'

#activestorage not previewable
# gem 'poppler'

#searching
gem 'pg_search'
