# == Schema Information
#
# Table name: campus_newspapers
#
#  id         :bigint(8)        not null, primary key
#  judul      :string
#  isi        :text
#  kategori   :string
#  user_id    :integer
#  posisi     :string
#  terbit     :boolean
#  image_data :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CampusNewspaper < ApplicationRecord
  # validates_presence_of :judul, :kategori, :isi

  has_one_attached :attachment
  has_many_attached :uploads

  scope :judul_kategori, -> (query){where("lower(judul) like lower(?) or lower(kategori) like lower(?)", "%#{query}%", "%#{query}%")}

  def self.category_list
    {
      "AKADEMIK" => "akademik",
      "HIMPUNAN" => "himpunan",
      "UMUM" => "umum",
    }
  end

  def self.posisi_list
    {
      "Utama" => "utama",
      "Populer" => "populer",
      "Umum"  => "umum"
    }
  end

  def self.terbit_list
    {
      "Ya" => true,
      "Tidak" => false
    }
  end
end
