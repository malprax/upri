# == Schema Information
#
# Table name: lecture_appreciations
#
#  id           :bigint(8)        not null, primary key
#  year         :string
#  appreciation :string
#  presented_by :string
#  lecture_id   :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_lecture_appreciations_on_lecture_id  (lecture_id)
#
class LectureAppreciation < ApplicationRecord
  belongs_to :lecture
end
