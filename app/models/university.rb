# == Schema Information
#
# Table name: universities
#
#  id            :bigint(8)        not null, primary key
#  name          :string
#  code          :string
#  foundation_id :bigint(8)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  alamat        :string
#  telepon       :string
#
# Indexes
#
#  index_universities_on_foundation_id  (foundation_id)
#
class University < ApplicationRecord
  belongs_to :foundation
  has_many :faculties, dependent: :destroy
  has_many :majors, through: :faculties
end
