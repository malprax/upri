# == Schema Information
#
# Table name: position_histories
#
#  id          :bigint(8)        not null, primary key
#  position_id :bigint(8)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_position_histories_on_position_id  (position_id)
#
class PositionHistory < ApplicationRecord
end
