# == Schema Information
#
# Table name: lecture_profiles
#
#  id                       :bigint(8)        not null, primary key
#  lecture_id               :bigint(8)
#  nama                     :string
#  gelar_depan_nama         :string
#  gelar_belakang_nama      :string
#  nomor_induk_pegawai      :string
#  nomor_induk_dosen_negara :string
#  nomor_induk_kependudukan :string
#  jenis_kelamin            :string
#  tempat_lahir             :string
#  tanggal_lahir            :date
#  is_active                :boolean          default(FALSE)
#  lecture_type             :string
#  lecture_status           :string
#  marital_status           :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  handphone                :string
#
# Indexes
#
#  index_lecture_profiles_on_lecture_id  (lecture_id)
#

class LectureProfile < ApplicationRecord
  belongs_to :lecture
  # validates_presence_of :nama, :jenis_kelamin, :tempat_lahir, :tanggal_lahir, :lecture_type
  # validates :nomor_induk_pegawai, format: { with: /\A\d+\z/, message: "Hanya Diisi Dengan Angka Tanpa Spasi" }
  # validates :nomor_induk_dosen_negara, format: { with: /\A\d+\z/, message: "Hanya Diisi Dengan Angka Tanpa Spasi" }
  # validates :nomor_induk_kependudukan, format: { with: /\A\d+\z/, message: "Hanya Diisi Dengan Angka Tanpa Spasi" }
end
