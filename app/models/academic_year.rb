# == Schema Information
#
# Table name: academic_years
#
#  id              :bigint(8)        not null, primary key
#  name            :string
#  active_status   :boolean          default(TRUE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  is_active       :boolean          default(FALSE)
#  semester_status :string
#  semester        :string
#

class AcademicYear < ApplicationRecord
  # has_many :essays, dependent: :nullify
  # has_many :users, dependent: :nullify
  # validates_presence_of :name, :semester
  # has_one :image
  has_many :subject_plans
  has_many :payments, dependent: :destroy
  before_create :set_active
  # after_save :reset_token
  validates_uniqueness_of :name, scope: [:semester], message: "Maaf Tahun Akademik Dengan Semester Yang Anda Isi Sudah Ada"

  scope :sortir_dari_tahun_terakhir_dan_aktifnya, -> {order("name desc").order("semester desc").order("is_active asc")}
  scope :aktif, -> {where(is_active: true).first}

  def reset_token
    Student.regenerate_access_token
  end

  def set_active
    self.is_active = true
  end

  def self.semester_list
    [
      "ganjil",
      "genap",
      "pendek"
    ]
  end

  def tahun_akademik_aktif
    AcademicYear.is_active = true
  end

  # private
  # def self.make_academic_year
  #   a = (Time.now.year).to_s + "/" + (Time.now.year+1).to_s
  #   if AcademicYear.blank?
  #     b = 0
  #   else
  #     if AcademicYear.first.name == 0 || nil
  #       b = 1
  #     else
  #       b = 0
  #     end
  #   end
  #   self.new(:nama => a , :active_status => b)
  # end
end
