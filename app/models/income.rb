# == Schema Information
#
# Table name: incomes
#
#  id         :bigint(8)        not null, primary key
#  nama       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Income < ApplicationRecord
  has_many :mahasiswa_profiles, dependent: :nullify
end
