# == Schema Information
#
# Table name: study_plans
#
#  academic_year_id :string
#  student_id       :bigint(8)
#  subject_id       :bigint(8)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_study_plans_on_student_id  (student_id)
#  index_study_plans_on_subject_id  (subject_id)
#

class StudyPlan < ApplicationRecord
  belongs_to :student
  belongs_to :subject
end
