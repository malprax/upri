# == Schema Information
#
# Table name: book_collections
#
#  id              :bigint(8)        not null, primary key
#  judul           :string
#  penulis         :string
#  penerbit        :string
#  cover_buku_data :text
#  kategori        :string
#  posisi          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class BookCollection < ApplicationRecord
  include CoverUploader[:cover_buku]
  def self.book_list
    [
      "Umum",
      "Tambang",
      "Mesin",
      "Informatika"
    ]
  end

  def self.posisi_list
    [
      "Rak 1",
      "Rak 2",
      "Rak 3",
      "Rak 4",
      "Rak 5"
    ]
  end
end
