# == Schema Information
#
# Table name: schedule_subjects
#
#  id                 :bigint(8)        not null, primary key
#  hari               :string
#  jam                :string
#  subject_id         :bigint(8)
#  dummy_lecture_1_id :bigint(8)
#  dummy_lecture_2_id :bigint(8)
#  ruang              :string
#  major_id           :bigint(8)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_schedule_subjects_on_dummy_lecture_1_id  (dummy_lecture_1_id)
#  index_schedule_subjects_on_dummy_lecture_2_id  (dummy_lecture_2_id)
#  index_schedule_subjects_on_major_id            (major_id)
#  index_schedule_subjects_on_subject_id          (subject_id)
#

class ScheduleSubject < ApplicationRecord
  belongs_to :major, optional: true
  belongs_to :subject, optional: true

  belongs_to :dummy_lecture_1, class_name:"DummyLecture", optional: true
  belongs_to :dummy_lecture_2, class_name:"DummyLecture", optional: true
  validates_presence_of :hari, :jam, :ruang, :dummy_lecture_1_id, :subject_id, message: "Terdapat Kesalahan, Pastikan Anda Mengisi Hari, Jam, Ruang, Matakuliah, Dosen 1"
  validates :subject_id, uniqueness: {scope:[:hari, :jam, :dummy_lecture_1_id, :ruang]}
  validate :cek_dosen
  def self.list_ruang
    [
      "Lab Komputer", "102", "103", "104"
    ]
  end

  def self.list_jam
    [
      "08.30 - 10.10", "10.20 - 12.00", "13.00 - 14.40"
    ]
  end

  def self.list_hari
    [
      "senin", "selasa", "rabu", "kamis", "jumat", "sabtu"
    ]
  end

  private
  def cek_dosen
    if self.dummy_lecture_1_id == self.dummy_lecture_2_id
      errors.add(:dummy_lecture_2_id, "Tidak Boleh Sama Dengan Dosen 1")
    end
  end
end
