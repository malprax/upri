# == Schema Information
#
# Table name: lecture_functional_types
#
#  id                    :bigint(8)        not null, primary key
#  name                  :string
#  role                  :string
#  credit                :integer
#  lecture_level_type_id :bigint(8)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
# Indexes
#
#  index_lecture_functional_types_on_lecture_level_type_id  (lecture_level_type_id)
#
class LectureFunctionalType < ApplicationRecord
  has_many :lectures, dependent: :destroy
  belongs_to :lecture_level_type
end
