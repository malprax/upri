# == Schema Information
#
# Table name: essays
#
#  id                 :bigint(8)        not null, primary key
#  title              :string
#  academic_year_id   :bigint(8)
#  graduate_id        :bigint(8)
#  dummy_lecture_1_id :bigint(8)
#  dummy_lecture_2_id :bigint(8)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  cover_skripsi_data :text
#  posisi             :string
#  kode               :string
#
# Indexes
#
#  index_essays_on_academic_year_id    (academic_year_id)
#  index_essays_on_dummy_lecture_1_id  (dummy_lecture_1_id)
#  index_essays_on_dummy_lecture_2_id  (dummy_lecture_2_id)
#  index_essays_on_graduate_id         (graduate_id)
#

class Essay < ApplicationRecord
  belongs_to :graduate, optional: true
  belongs_to :academic_year, optional: true
  # has_many :faculties, through: :graduates
  has_many :essay_documents, dependent: :nullify
  accepts_nested_attributes_for :essay_documents , reject_if: :all_blank, allow_destroy: true
  include CoverUploader[:cover_skripsi]

  def essay_title
    Essay.title
  end

  def essay_dummy_lecture_1_id
    Essay.dummy_lecture_1_id
  end

  def essay_dummy_lecture_2_id
    Essay.dummy_lecture_2_id
  end
end
