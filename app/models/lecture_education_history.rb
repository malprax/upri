# == Schema Information
#
# Table name: lecture_education_histories
#
#  id              :bigint(8)        not null, primary key
#  level           :string
#  major           :string
#  institute       :string
#  graduation_year :string
#  lecture_id      :bigint(8)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_lecture_education_histories_on_lecture_id  (lecture_id)
#
class LectureEducationHistory < ApplicationRecord
  belongs_to :lecture
end
