# == Schema Information
#
# Table name: users
#
#  id                         :bigint(8)        not null, primary key
#  email                      :string           default(""), not null
#  encrypted_password         :string           default(""), not null
#  reset_password_token       :string
#  reset_password_sent_at     :datetime
#  remember_created_at        :datetime
#  sign_in_count              :integer          default(0), not null
#  current_sign_in_at         :datetime
#  last_sign_in_at            :datetime
#  current_sign_in_ip         :inet
#  last_sign_in_ip            :inet
#  confirmation_token         :string
#  confirmed_at               :datetime
#  confirmation_sent_at       :datetime
#  unconfirmed_email          :string
#  failed_attempts            :integer          default(0), not null
#  unlock_token               :string
#  locked_at                  :datetime
#  image_data                 :text
#  type                       :string
#  provider                   :string
#  uid                        :string
#  name                       :string
#  birth_date                 :date
#  birth_place                :string
#  gender                     :string
#  religion                   :string
#  marital_status             :string
#  hobby                      :string
#  stambuk                    :string
#  is_student                 :boolean          default(FALSE)
#  is_graduate                :boolean
#  is_lecture                 :boolean
#  is_staff                   :boolean
#  access_token               :string
#  nidn                       :string
#  nip                        :string
#  dummy_lecture_1_id         :bigint(8)
#  dummy_lecture_2_id         :bigint(8)
#  major_id                   :bigint(8)
#  faculty_id                 :bigint(8)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  blangko_data               :text
#  academic_year_id           :integer
#  sortlist                   :integer
#  is_active                  :boolean          default(TRUE)
#  phone_number               :string
#  authy_id                   :string
#  last_sign_in_with_authy    :datetime
#  authy_enabled              :boolean          default(FALSE)
#  is_phone_verified          :boolean          default(FALSE)
#  is_alumni                  :boolean          default(FALSE)
#  religion_id                :bigint(8)
#  lecture_functional_type_id :bigint(8)
#  referral                   :string
#
# Indexes
#
#  index_users_on_access_token                (access_token) UNIQUE
#  index_users_on_authy_id                    (authy_id)
#  index_users_on_confirmation_token          (confirmation_token) UNIQUE
#  index_users_on_dummy_lecture_1_id          (dummy_lecture_1_id)
#  index_users_on_dummy_lecture_2_id          (dummy_lecture_2_id)
#  index_users_on_email                       (email) UNIQUE
#  index_users_on_faculty_id                  (faculty_id)
#  index_users_on_lecture_functional_type_id  (lecture_functional_type_id)
#  index_users_on_major_id                    (major_id)
#  index_users_on_nidn                        (nidn) UNIQUE
#  index_users_on_religion_id                 (religion_id)
#  index_users_on_reset_password_token        (reset_password_token) UNIQUE
#  index_users_on_stambuk                     (stambuk) UNIQUE
#  index_users_on_unlock_token                (unlock_token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (lecture_functional_type_id => lecture_functional_types.id)
#  fk_rails_...  (religion_id => religions.id)
#

class User < ApplicationRecord
  has_secure_token :access_token
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  include ImageUploader::Attachment.new(:image)
  include BlangkoUploader::Attachment.new(:blangko)

  attr_accessor :skip_check_type, :type_check
  before_create :check_type, if: Proc.new{|user| !user.skip_check_type.present?}
  before_create :set_not_active, unless: Proc.new{|user| user.type== "Student"}
  validates_presence_of :email, :password, :password_confirmation, :type_check, on: :create
  # validates :phone_number, :numericality => {:only_integer => true, :message => 'Hanya Angka Saja'}
  # validates :number, format: { with: /\A\d+\z/, message: "Integer only. No sign allowed." }
  # set not active only for graduate

  def set_not_active
    self.is_active = false
  end

  def input_form=(login)
    @login = login
  end

  def login
    @login || params[]
  end

  def is_admin?
    type == 'Admin'
  end

  def is_student?
    type == 'Student'
  end

  def is_lecture?
    type == 'Lecture'
  end

  def is_graduate?
    type == 'Graduate'
  end

  def is_staff?
    type == 'Staff'
  end

  def is_admin?
    type == 'Admin'
  end

  def is_active?
    is_active == true
  end

  def is_not_active?
    is_active == false
  end

  def self.type_check
    [
      "Dosen", "Mahasiswa", "Staff"
    ]
  end

  def check_type
    if type_check == "Wisudawan"
      self.type = "Graduate"
    elsif type_check == "Staff"
      self.type = "Staff"
    elsif type_check == "Dosen"
      self.type = "Lecture"
    elsif type_check == "Mahasiswa"
      self.type = "Student"
    elsif type_check == "Mahasiswa Baru"
      self.type = "Student"
      self.is_active = false
    end
  end

  def set_faculty
    if choose_faculty == "fkip"
      self.faculty_id = 2
    elsif choose_faculty == "fatek"
      self.faculty_id = 3
    elsif choose_faculty == "fisip"
      self.faculty_id = 4
    elsif choose_faculty == "fekon"
      self.faculty_id = 5
    elsif choose_faculty == "fkm"
      self.faculty_id = 6
    end
  end

  def set_major
    if program_studi == "Teknik Pertambangan"
      self.major_id = 31
    elsif program_studi == "Teknik Mesin"
      self.major_id = 32
    elsif program_studi == "Teknik Informatika"
      self.major_id = 33
    end
  end

  def set_kategori_kelas_mahasiswa
    if self.kategori_kelas_mahasiswa == "Reguler"
      self.kategori = "reguler"
    elsif kategori_kelas_mahasiswa == "Non Reguler"
      self.kategori = "non_reguler"
    elsif kategori_kelas_mahasiswa == "Non Reguler"
      self.kategori = "konversi"
    elsif kategori_kelas_mahasiswa == "Lanjut"
      self.kategori == "lanjut"
    end
  end

  def self.kategori_mahasiswa_list
    [
      "Reguler",
      "Non Reguler",
      "Non Reguler",
      "Lanjut"
    ]
  end

  def self.major_list
    [
      "Teknik Pertambangan",
      "Teknik Mesin",
      "Teknik Informatika"
    ]
  end

  def self.role_list
    [
      ["rektor","Rektor"],
      ["wakil_rektor_akademik","Wakil Rektor Akademik"],
      ["wakil_rektor_keuangan","Wakil Rektor Keuangan"],
      ["wakil_rektor_kemahasiswaan","Wakil Rektor Kemahasiswaan"],
      ["staf_rektor","Staf Rektor"],
      ["staf_wakil_rektor_akademik","Staf Wakil Rektor Akademik"],
      ["staf_wakil_rektor_keuangan","Staf Wakil Rektor Keuangan"],
      ["staf_wakil_rektor_kemahasiswaan","Staf Wakil Rektor Kemahasiswaan"],
      ["dekan","Dekan"],
      ["wakil_dekan_akademik","Wakil Dekan Akademik"],
      ["wakil_dekan_keuangan","Wakil Dekan Keuangan"],
      ["wakil_dekan_kemahasiswaan","Wakil Dekan Kemahasiswaan"],
      ["staf_dekan","Staf Dekan"],
      ["staf_wakil_dekan_akademik","Staf Wakil Dekan Akademik"],
      ["staf_wakil_dekan_keuangan","Staf Wakil Dekan Keuangan"],
      ["staf_wakil_dekan_kemahasiswaan","Staf Wakil Dekan Kemahasiswaan"],
      ["kepala_tata_usaha","Kepala Tata Usaha"],
      ["staf_tata_usaha","Staf Tata Usaha"],
      ["ketua_jurusan","Ketua Jurusan"],
      ["staf_jurusan","Staf Jurusan"],
      ["sekertaris_jurusan","Sekertaris Jurusan"]
    ]
  end

  def self.authenticate_phone(phone_number)
    user = find_by_phone_number(0 + phone_number)
    if user
      user
    else
      nil
    end
  end

  #for prevent admin send email
  # @skip = false
  # def skip_notifications!()
  #   skip_confirmation!
  #   @skip = true
  # end
  #
  # def email_changed?
  #   return false if @skip
  #   super
  # end
  #
  # def encrypted_password_changed?
  #   return false if @skip
  #   super
  # end
  # end of admin setting

  # use of identity first time STI model

end
