# == Schema Information
#
# Table name: faculties
#
#  id            :bigint(8)        not null, primary key
#  name          :string
#  code          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  university_id :integer
#

class Faculty < ApplicationRecord
  belongs_to :university
  has_many :graduates, class_name: "Graduate", foreign_key: :user_id, dependent: :nullify
  has_many :majors, class_name: "Major", foreign_key: :major_id, dependent: :nullify
  has_many :essay, through: :graduates
  has_many :lecture_lists, inverse_of: :faculty
end
