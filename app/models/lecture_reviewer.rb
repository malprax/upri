# == Schema Information
#
# Table name: lecture_reviewers
#
#  id         :bigint(8)        not null, primary key
#  year       :string
#  review     :string
#  publisher  :string
#  lecture_id :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_lecture_reviewers_on_lecture_id  (lecture_id)
#
class LectureReviewer < ApplicationRecord
  belongs_to :lecture
end
