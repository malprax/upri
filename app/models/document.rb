# == Schema Information
#
# Table name: documents
#
#  id                 :bigint(8)        not null, primary key
#  name               :string
#  document_file_id   :integer
#  document_file_type :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Document < ApplicationRecord
  belongs_to :mahasiswa, optional: true
  belongs_to :graduate, optional: true
  belongs_to :document_file, polymorphic: true
  has_one_attached :image_file
  has_one_attached :kartu_keluarga_file
  has_one_attached :kartu_tanda_penduduk_file
  has_one_attached :ijazah_file

  validate :image_file_size

  # validates_presence_of :kategori
  include DocumentUploader::Attachment.new(:file_dokumen)
  private
  def image_file_size
    errors.add :image_file, 'too big' if image_file.blob.byte_size > 4096
  end
end
