# == Schema Information
#
# Table name: articles
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  author     :string
#  journal_id :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_articles_on_journal_id  (journal_id)
#
class Article < ApplicationRecord
  belongs_to :journal
  has_one_attached :article_file
end
