# == Schema Information
#
# Table name: users
#
#  id                         :bigint(8)        not null, primary key
#  email                      :string           default(""), not null
#  encrypted_password         :string           default(""), not null
#  reset_password_token       :string
#  reset_password_sent_at     :datetime
#  remember_created_at        :datetime
#  sign_in_count              :integer          default(0), not null
#  current_sign_in_at         :datetime
#  last_sign_in_at            :datetime
#  current_sign_in_ip         :inet
#  last_sign_in_ip            :inet
#  confirmation_token         :string
#  confirmed_at               :datetime
#  confirmation_sent_at       :datetime
#  unconfirmed_email          :string
#  failed_attempts            :integer          default(0), not null
#  unlock_token               :string
#  locked_at                  :datetime
#  image_data                 :text
#  type                       :string
#  provider                   :string
#  uid                        :string
#  name                       :string
#  birth_date                 :date
#  birth_place                :string
#  gender                     :string
#  religion                   :string
#  marital_status             :string
#  hobby                      :string
#  stambuk                    :string
#  is_student                 :boolean          default(FALSE)
#  is_graduate                :boolean
#  is_lecture                 :boolean
#  is_staff                   :boolean
#  access_token               :string
#  nidn                       :string
#  nip                        :string
#  dummy_lecture_1_id         :bigint(8)
#  dummy_lecture_2_id         :bigint(8)
#  major_id                   :bigint(8)
#  faculty_id                 :bigint(8)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  blangko_data               :text
#  academic_year_id           :integer
#  sortlist                   :integer
#  is_active                  :boolean          default(TRUE)
#  phone_number               :string
#  authy_id                   :string
#  last_sign_in_with_authy    :datetime
#  authy_enabled              :boolean          default(FALSE)
#  is_phone_verified          :boolean          default(FALSE)
#  is_alumni                  :boolean          default(FALSE)
#  religion_id                :bigint(8)
#  lecture_functional_type_id :bigint(8)
#  referral                   :string
#
# Indexes
#
#  index_users_on_access_token                (access_token) UNIQUE
#  index_users_on_authy_id                    (authy_id)
#  index_users_on_confirmation_token          (confirmation_token) UNIQUE
#  index_users_on_dummy_lecture_1_id          (dummy_lecture_1_id)
#  index_users_on_dummy_lecture_2_id          (dummy_lecture_2_id)
#  index_users_on_email                       (email) UNIQUE
#  index_users_on_faculty_id                  (faculty_id)
#  index_users_on_lecture_functional_type_id  (lecture_functional_type_id)
#  index_users_on_major_id                    (major_id)
#  index_users_on_nidn                        (nidn) UNIQUE
#  index_users_on_religion_id                 (religion_id)
#  index_users_on_reset_password_token        (reset_password_token) UNIQUE
#  index_users_on_stambuk                     (stambuk) UNIQUE
#  index_users_on_unlock_token                (unlock_token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (lecture_functional_type_id => lecture_functional_types.id)
#  fk_rails_...  (religion_id => religions.id)
#

class Student < User
  include PgSearch::Model

  belongs_to :major, optional:true

  has_many :subject_plans
  has_many :subjects, through: :subject_plans

  has_one :student_profile, dependent: :destroy
  has_one :parent, dependent: :destroy
  has_one :alamat_tinggal, foreign_key: :user_id, class_name:"Address"
  has_one :school, dependent: :destroy

  has_one_attached :student_photo_file
  has_one_attached :kartu_keluarga_file
  has_one_attached :kartu_tanda_penduduk_file
  has_one_attached :ijazah_file

  has_many :payments, dependent: :destroy
  # validate :student_photo_file_size
  # validate :kartu_keluarga_file_size
  # validate :kartu_tanda_penduduk_file_size
  # validate :ijazah_file_size


  # has_many :documents, as: :document_file, dependent: :destroy


  # has_one :alamat_ayah, through: :parent, foreign_key: :user_id, class_name:"Address"
  # has_one :alamat_ibu, through: :parent, foreign_key: :user_id, class_name:"Address"
  # has_one :alamat_wali, through: :parent, foreign_key: :user_id, class_name:"Address"

  accepts_nested_attributes_for :student_profile, allow_destroy: true
  accepts_nested_attributes_for :parent, allow_destroy: true
  accepts_nested_attributes_for :alamat_tinggal, allow_destroy: true
  accepts_nested_attributes_for :school, allow_destroy: true

  accepts_nested_attributes_for :payments, allow_destroy: true
  # accepts_nested_attributes_for :documents

  # accepts_nested_attributes_for :alamat_ayah
  # accepts_nested_attributes_for :alamat_ibu
  # accepts_nested_attributes_for :alamat_wali

  attr_accessor :create_payment
  pg_search_scope :search, against: [:email], associated_against: {
    major: [:name],
    student_profile: [:nama, :nomor_induk_mahasiswa]
  }

  scope :begining, lambda{where("created_at > ?", "01 Jan 2020")}


  def self.referral_list
    [
      "Iskandar (085230873781)",
      "Muhammad Yusuf (081354763890)",
      "Ruth Bunga (085342293810)",
      "Ahmad Fachrial (085242813366)"
    ]
  end

  def create_payment
      @academic_year = AcademicYear.find_by(is_active: true)
      @bank = Bank.where(bank: "BRI", account_number: "034301001373303").first
      @payment = Payment.where(kind:'registrasi', academic_year_id: @academic_year.id, student_id: self.id, in_number: 350000, in_word: "Tiga Ratus Lima Puluh Ribu Rupiah", bank_id: @bank.id ).first_or_create
  end
  private

  # def student_photo_file_size
  #   # errors.add :student_photo_file, 'file terlalu besar' if student_photo_file.blob.byte_size > 2.megabytes
  #   errors[:student_photo_file] << "should be less than 5MB" if student_photo_file.blob.byte_size > 2.megabytes
  # end
  # def kartu_keluarga_file_size
  #   errors.add :kartu_keluarga_file, 'file terlalu besar' if kartu_keluarga_file.blob.byte_size > 2.megabytes
  # end
  # def kartu_tanda_penduduk_file_size
  #   errors.add :kartu_tanda_penduduk_file, 'file terlalu besar' if kartu_tanda_penduduk_file.blob.byte_size > 2.megabytes
  # end
  # def ijazah_file_size
  #   errors.add :ijazah_file, 'file terlalu besar' if ijazah_file.blob.byte_size > 2.megabytes
  # end

  def generate_access_token
    self.regenerate_access_token
  end


  def self.authenticate_student_token(student, access_token)
    if student && student.access_token == access_token
      student
    else
      nil
    end
  end
end
