# == Schema Information
#
# Table name: payments
#
#  id               :bigint(8)        not null, primary key
#  kind             :string
#  academic_year_id :bigint(8)        not null
#  in_number        :float
#  in_word          :string
#  student_id       :integer          not null
#  bank_id          :bigint(8)        not null
#  approval_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  paid             :datetime
#  type_student     :string
#  is_approved      :boolean          default(FALSE)
#
# Indexes
#
#  index_payments_on_academic_year_id  (academic_year_id)
#  index_payments_on_bank_id           (bank_id)
#
# Foreign Keys
#
#  fk_rails_...  (academic_year_id => academic_years.id)
#  fk_rails_...  (bank_id => banks.id)
#
require 'barby/barcode/code_128'
require 'barby/barcode/qr_code'
# require 'barby/outputter/png_outputter'

class Payment < ApplicationRecord
  include HasBarcode
  include PgSearch::Model
  pg_search_scope :search, against: [:kind, :in_number], associated_against: {
    bank: [:name],
    student_profile: [:nama, :nomor_induk_mahasiswa]
  }

  belongs_to :academic_year
  belongs_to :student
  has_one :student_profile, through: :student

  belongs_to :bank, optional: true
  belongs_to :approval, optional: true
  has_one_attached :payment_file
  before_save :set_in_words
  before_save :set_bank
  # validate :payment_file_size

  validates_uniqueness_of :academic_year_id, scope: [:kind, :student_id], message: "Maaf Pembayaran Untuk Semester Ini Sudah Ada"

  default_scope {order(is_approved: :desc).order(academic_year_id: :desc).order(paid: :asc)}
  # scope :search, -> (query){where("lower(kind) like lower(?) ", "%#{query}%")}

  has_barcode :barcode,
      outputter: :svg,
      # type: Barby::Code128,
      type: Barby::QrCode,
      value: Proc.new{|c| c.codenya}

  attr_accessor :genbar
  # barcode = "#{self.created_at}"+"#{self.student.student_profile.nama}"+"#{self.student.student_profile.nomor_induk_mahasiswa}"

  def is_semester_pendek?
    self.kind == "semester pendek"
  end

  def is_not_semester_pendek?
    self.kind != "semester pendek"
  end

  def is_not_approved?
    self.is_approved == false
  end

  def codenya
    self.genbar = "UPRI Makassar #{self.bank.name}"
  end

  def self.bpp_tambang_list_price
    [
      2500000,
      3000000,
      4000000
    ]
  end

  def self.spp_tambang_list_price
    [
      1000000,
      1500000,
      2000000,
      2500000
    ]
  end

  SPJ_TAMBANG_LIST_PRICE =
    {
      "Rp 2.000.000" => 2000000
    }
  LABORATORIUM_TAMBANG_LIST_PRICE =
    {
      "Rp 1.000.000" => 1000000
    }

  def self.class_type_list
    [
      "p2k"
    ]
  end

  def set_bank
    bank_bpp = Bank.where(name: "BRI", account_number: "034301001373303").first
    bank_spp = Bank.find_by_name("SULSELBAR")
    kind = self.kind
    case kind
    when "spp"
      self.bank_id = bank_spp.id
    when "bpp"
      self.bank_id = bank_bpp.id
    end
  end

  def self.payment_kind_list
    ["bpp","spp","spj","semester pendek","laboratorium","kunjungan industri","pelatihan","kuliah kerja nyata","proposal","seminar hasil", "ujian tutup", "wisuda"]
  end

  def set_in_words
    number = self.in_number
    case number
    when 350000
      self.in_word = "Tiga Ratus Lima Puluh Ribu Rupiah"
    when 1000000
      self.in_word = "Satu Juta Rupiah"
    when 1500000
      self.in_word = "Satu Juta Lima Ratus Ribu Rupiah"
    when 2000000
      self.in_word = "Dua Juta Rupiah"
    when 2500000
      self.in_word = "Dua Juta Lima Ratus Ribu Rupiah"
    when 3000000
      self.in_word = "Tiga Juta Rupiah"
    when 4000000
      self.in_word = "Empat Juta Rupiah"
    end
  end

  # private
  # def payment_file_size
  #   errors.add :payment_file, 'file terlalu besar' if payment_file.blob.byte_size > 2.megabytes
  # end
end
