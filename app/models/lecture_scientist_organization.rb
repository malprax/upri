# == Schema Information
#
# Table name: lecture_scientist_organizations
#
#  id           :bigint(8)        not null, primary key
#  year         :string
#  organization :string
#  position     :string
#  lecture_id   :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_lecture_scientist_organizations_on_lecture_id  (lecture_id)
#
class LectureScientistOrganization < ApplicationRecord
  belongs_to :lecture
end
