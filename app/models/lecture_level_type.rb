# == Schema Information
#
# Table name: lecture_level_types
#
#  id         :bigint(8)        not null, primary key
#  group      :string
#  rank       :string
#  letter     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class LectureLevelType < ApplicationRecord
  belongs_to :lecture_functional_type, optional: true
end
