# == Schema Information
#
# Table name: studentletters
#
#  id                                           :bigint(8)        not null, primary key
#  nama                                         :string
#  email                                        :string
#  nomor_induk_mahasiswa                        :string
#  program_studi                                :string
#  tempat_lahir                                 :string
#  tanggal_lahir                                :date
#  alamat                                       :string
#  telepon                                      :string
#  handphone                                    :string
#  tanggal_yudisium                             :string
#  judul_skripsi                                :string
#  nama_orang_tua                               :string
#  tempat_kerja                                 :string
#  pangkat_golongan                             :string
#  kategori_nomor_induk_instansi                :string
#  nomor_induk_instansi                         :string
#  telepon_orangtua                             :string
#  jenis_surat                                  :string
#  keterangan_pindah                            :string
#  diketahui_kepala_tata_usaha                  :string
#  tanggal_diketahui_kepala_tata_usaha          :date
#  kepala_tata_usaha                            :string
#  nomor_surat                                  :string
#  nomor_ijazah_fakultas                        :string
#  nomor_ijazah_universitas                     :string
#  kepada                                       :string
#  casu_quo_kepada                              :string
#  jenis_surat_lainnya                          :string
#  foto_ijazah_data                             :text
#  foto_bukti_pembayaran_semester_berjalan_data :text
#  created_at                                   :datetime         not null
#  updated_at                                   :datetime         not null
#

class Studentletter < ApplicationRecord
  belongs_to :major

  def self.jenis_surat_list
    ["Keterangan Kuliah Biasa",
    "Keterangan Kuliah Tanggungan",
    "Keterangan Akreditasi",
    "Rekomendasi Beasiswa",
    "Rekomendasi Kegiatan",
    "Cuti Akademik",
    "Perbaikan Nama pada PD-Dikti",
    "Keterangan Pindah"   ]
  end
end
