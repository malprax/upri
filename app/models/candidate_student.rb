# == Schema Information
#
# Table name: candidate_students
#
#  id                  :bigint(8)        not null, primary key
#  registration_number :string
#  class_plan          :string
#  major_1             :string
#  major_2             :string
#  name                :string
#  birth_date          :date
#  birth_place         :string
#  gender              :string
#  phone_number        :string
#  street_address      :string
#  hamlet              :string
#  neighbourhood       :string
#  urban_village       :string
#  sub_district        :string
#  district            :string
#  province            :string
#  postal_code         :integer
#  school              :string
#  school_address      :string
#  school_plan         :string
#  nisn                :string
#  sertificate_number  :string
#  sertificate_date    :string
#  national_exam_score :string
#  father_name         :string
#  father_job          :string
#  father_salary       :string
#  father_education    :string
#  mother_name         :string
#  mother_job          :string
#  mother_salary       :string
#  mother_education    :string
#  know_from           :string
#  academic_year_id    :integer
#  referral            :string
#  email               :string
#  approve             :boolean          default(FALSE)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#


class CandidateStudent < ApplicationRecord
  before_save :set_academic_year, :send_email

  def set_academic_year
    self.academic_year_id = AcademicYear.aktif
  end

  def send_email
    CandidateStudentMailer.email_pendaftaran(self).perform_later
  end

  def self.class_plan_list
    [
      ["Reguler (mahasiswa baru)", "reguler"],
      ["Konversi (mahasiswa pindah)", "konversi"],
      ["Transfer (mahasiswa lanjut)", "transfer"]
    ]
  end

  def self.prodi_list
    [
      ["Teknik Pertambangan", "teknik_pertambangan"],
      ["Teknik Mesin", "teknik_mesin"],
      ["Teknik Informatika", "teknik_informatika"]
    ]
  end

  def self.gender_list
    [
      ["Laki laki", "laki_laki"],
      ["Perempuan", "perempuan"]
    ]
  end

  def self.school_plan_list
    [
      ["IPA", "ipa"],
      ["IPS", "ips"],
      ["Bahasa", "bahasa"],
      ["Lainnya", "lainnya"]
    ]
  end

  def self.job_list
    [
      ["Aparatur Sipil Negara", "aparatur_sipil_negara"],
      ["Polisi / Tentara", "polisi_atau_tentara"],
      ["Dokter atau medis", "dokter_atau_medis"],
      ["Pegawai Swasta", "swasta"],
      ["Wirausaha", "wirausaha"],
      ["Petani", "petani"],
      ["Lainnya", "lainnya"]
    ]
  end

  def self.salary_list
    [
      ["< 1 Juta", "kecil_dari_satu_juta"],
      ["1 - 2 Juta","satu_sampai_dua_juta"],
      ["2 - 5 Juta","dua_sampai_5_juta"],
      ["> 5 Juta","lebih_dari_lima_juta"],
      ["Lainnya", "lainnya"]
    ]
  end

  def self.education_list
    [
      ["Sekolah Dasar", "sekolah_dasar"],
      ["SMP Sederajat", "sekolah_menengah_pertama_atau_sederajat"],
      ["SMA Sederajat", "sekolah_menengah_atas_atau_sederajat"],
      ["Diploma", "diploma"],
      ["Sarjana", "sarjana"],
      ["Magister", "magister"],
      ["Doktor", "doktor"],
      ["Lainnya", "lainnya"]
    ]
  end

  def self.know_from_list
    [
      ["Sekolah", "sekolah"],
      ["Orang tua/Kerabat", "orang_tua_kerabat"],
      ["Spanduk/Banner", "spanduk_banner"],
      ["Internet", "internet"],
      ["Televisi", "televisi"],
      ["Teman", "teman"],
      ["Brosur/Poster", "brosur_poster"],
      ["Surat Kabar", "surat_kabar"],
      ["Radio", "radio"],
      ["Lainnya", "lainnya"]
    ]
  end
end
