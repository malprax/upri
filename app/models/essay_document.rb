# == Schema Information
#
# Table name: essay_documents
#
#  id                   :bigint(8)        not null, primary key
#  kategori             :string
#  document_upload_data :text
#  essay_id             :bigint(8)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_essay_documents_on_essay_id  (essay_id)
#
# Foreign Keys
#
#  fk_rails_...  (essay_id => essays.id)
#

class EssayDocument < ApplicationRecord
  belongs_to :essay
  include DocumentUploader[:document_upload]

  def self.kategori_dokumen_list
    {
      "File Word"=>"file_word",
      "File Pdf"=>"file_pdf"
    }
  end
end
