# == Schema Information
#
# Table name: users
#
#  id                         :bigint(8)        not null, primary key
#  email                      :string           default(""), not null
#  encrypted_password         :string           default(""), not null
#  reset_password_token       :string
#  reset_password_sent_at     :datetime
#  remember_created_at        :datetime
#  sign_in_count              :integer          default(0), not null
#  current_sign_in_at         :datetime
#  last_sign_in_at            :datetime
#  current_sign_in_ip         :inet
#  last_sign_in_ip            :inet
#  confirmation_token         :string
#  confirmed_at               :datetime
#  confirmation_sent_at       :datetime
#  unconfirmed_email          :string
#  failed_attempts            :integer          default(0), not null
#  unlock_token               :string
#  locked_at                  :datetime
#  image_data                 :text
#  type                       :string
#  provider                   :string
#  uid                        :string
#  name                       :string
#  birth_date                 :date
#  birth_place                :string
#  gender                     :string
#  religion                   :string
#  marital_status             :string
#  hobby                      :string
#  stambuk                    :string
#  is_student                 :boolean          default(FALSE)
#  is_graduate                :boolean
#  is_lecture                 :boolean
#  is_staff                   :boolean
#  access_token               :string
#  nidn                       :string
#  nip                        :string
#  dummy_lecture_1_id         :bigint(8)
#  dummy_lecture_2_id         :bigint(8)
#  major_id                   :bigint(8)
#  faculty_id                 :bigint(8)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  blangko_data               :text
#  academic_year_id           :integer
#  sortlist                   :integer
#  is_active                  :boolean          default(TRUE)
#  phone_number               :string
#  authy_id                   :string
#  last_sign_in_with_authy    :datetime
#  authy_enabled              :boolean          default(FALSE)
#  is_phone_verified          :boolean          default(FALSE)
#  is_alumni                  :boolean          default(FALSE)
#  religion_id                :bigint(8)
#  lecture_functional_type_id :bigint(8)
#  referral                   :string
#
# Indexes
#
#  index_users_on_access_token                (access_token) UNIQUE
#  index_users_on_authy_id                    (authy_id)
#  index_users_on_confirmation_token          (confirmation_token) UNIQUE
#  index_users_on_dummy_lecture_1_id          (dummy_lecture_1_id)
#  index_users_on_dummy_lecture_2_id          (dummy_lecture_2_id)
#  index_users_on_email                       (email) UNIQUE
#  index_users_on_faculty_id                  (faculty_id)
#  index_users_on_lecture_functional_type_id  (lecture_functional_type_id)
#  index_users_on_major_id                    (major_id)
#  index_users_on_nidn                        (nidn) UNIQUE
#  index_users_on_religion_id                 (religion_id)
#  index_users_on_reset_password_token        (reset_password_token) UNIQUE
#  index_users_on_stambuk                     (stambuk) UNIQUE
#  index_users_on_unlock_token                (unlock_token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (lecture_functional_type_id => lecture_functional_types.id)
#  fk_rails_...  (religion_id => religions.id)
#

require 'csv'
class Graduate < User
  belongs_to :faculty, optional: true
  belongs_to :major, optional: true
  # belongs_to :lecture_1, class_name: "Lecture", optional: true
  # belongs_to :lecture_2, class_name: "Lecture", optional: true
  belongs_to :dummy_lecture_1, class_name:"DummyLecture", optional: true
  belongs_to :dummy_lecture_2, class_name:"DummyLecture", optional: true

  has_one :essay, foreign_key: :graduate_id, dependent: :destroy
  has_one :graduate_profile, foreign_key: :graduate_id, dependent: :destroy
  has_one :address, foreign_key: :user_id, dependent: :destroy
  has_many :identities, foreign_key: :user_id, dependent: :destroy
  has_many :documents, foreign_key: :user_id, dependent: :destroy
  has_many :payment_forms, foreign_key: :user_id, dependent: :destroy

  accepts_nested_attributes_for :graduate_profile #, reject_if: proc{|a| a[:ipk].blank?}, allow_destroy: true
  accepts_nested_attributes_for :essay #, reject_if: proc{|a| a[:title].blank?}, allow_destroy: true
  accepts_nested_attributes_for :address #, reject_if: proc{|a| a[:street].blank? && a[:phone].blank?}, allow_destroy: true
  accepts_nested_attributes_for :documents #, reject_if: proc{|a| a[:name].blank?}, allow_destroy: true
  accepts_nested_attributes_for :identities #, reject_if: proc{|a| a[:name].blank?}, allow_destroy: true
  accepts_nested_attributes_for :payment_forms #, reject_if: proc{|a| a[:name].blank?}, allow_destroy: true

  validates_presence_of :email
  # validates_uniqueness_of :stambuk, message: "Stambuk Sudah Ada"
  # validate :validate_stambuk_for_one_email
  #validates :stambuk, :format => { :with => /\A^(0|[1-9][0-9]*)$\z/, :message => "Input Hanya Angka Saja" }
  #validates :email, presence: true, format: {/\A([\w+\-]\.?)+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i}

  attr_accessor :pembimbing_1, :pembimbing_2 #, :sortlist
  # before_save :set_sortlist

  scope :fakultas_wisudawan, -> (faculty_id){where faculty_id: faculty_id}
  scope :program_studi_wisudawan, -> (major_id){where major_id: major_id}
  scope :nama_email_stambuk, -> (query){where("lower(name) like lower(?) or lower(email) like lower(?) or stambuk like ? ", "%#{query}%", "%#{query}%", "%#{query}%")}

  # def set_sortlist
  #   self.sortlist = self.stambuk.to_i
  # end

  before_save :set_backup_graduate

  def set_backup_graduate
    BackupGraduate.where(
        id: self.id,
        image_data: self.image_data,
        name: self.name,
        stambuk: self.stambuk,
        email: self.email,
        birth_date: self.birth_date,
        birth_place: self.birth_place,
        gender: self.gender,
        religion: self.religion,
        marital_status: self.marital_status,
        hobby: self.hobby,
        sortlist: self.sortlist,
        dummy_lecture_1_id: self.try(:essay_dummy_lecture_1_id),
        dummy_lecture_2_id: self.try(:essay_dummy_lecture_2_id),
        major_id: self.major_id,
        faculty_id: self.faculty_id,
        academic_year_id: self.academic_year_id,
        phone_number: self.phone_number,
        ipk: self.try(:graduate_profile_ipk),
        yudisium_date: self.try(:graduate_profile_yudisium_date),
        yudisium_letter: self.try(:graduate_profile_yudisium_letter),
        alumni_number: self.try(:graduate_profile_alumni_number),
        predicet_of_yudisium: self.try(:graduate_profile_predicet_of_yudisium),
        essay_title: self.try(:essay_title)
    ).first_or_create
  end
  def self.fakultas
    [
      ["fkip" => "Fakultas Ekonomi"],
      ["fisipol" => "Fakultas Ilmu Sosial Politik"],
      ["fkm" => "Fakultas Kesehatan Masyarakat"],
      ["ft" => "Fakultas Teknik"],
      ["fe" => "Fakultas Ekonomi"]
    ]
  end

  def self.jurusan
    [
      ["pendidikan_biologi" => "Pendidikan Biologi"],
      ["pendidikan_matematika" => "Pendidikan Matematika"],
      ["pendidikan_ppkn" => "Pendidikan PPKN"],
      ["pendidikan_sejarah" => "Pendidikan Sejarah"],
      ["teknologi_pendidikan" => "Teknologi Pendidikan"],
      ["ilmu_administrasi_negara" => "Ilmu Administrasi Negara"],
      ["ilmu_komunikasi" => "Ilmu Komunikasi"],
      ["kesehatan_masyarakat" => "Kesehatan Masyarakat"],
      ["teknik_pertambangan" => "Teknik Pertambangan"],
      ["teknik_mesin" => "Teknik Mesin"],
      ["teknik_informatika" => "Teknik Informatika"],
      ["manajemen" => "Manajemen"],
      ["akuntasi" => "Akuntansi"]
    ]
  end

  def self.religion_list
    ["islam", "kristen protestan", "katolik", "hindu", "budha"]
  end

  def self.predicet_list
    ["memuaskan", "sangat memuaskan","dengan pujian"]
  end

  def generate_access_token
    self.regenerate_access_token
  end

  def self.authenticate_graduate_token(stambuk, access_token)
    graduate = find_by_stambuk(stambuk)
    if graduate && graduate.access_token == access_token
      graduate
    else
      nil
    end
  end

  # def self.to_csv(fields = column_names, options ={})
  #   CSV.generate(options) do |csv|
  #     csv << fields
  #     all.each do |graduate|
  #       graduate.image = URI.parse(image_url)
        # csv << graduate.attributes.values_at(*fields)
  #       csv << [graduate.image, graduate.stambuk, graduate.name, graduate.faculty.name, graduate.major.name]
  #     end
  #   end
  # end

  def self.to_csv
    CSV.generate do |csv|
      csv << ['NAMA','STAMBUK','TANGGAL LAHIR','FAKULTAS','PRODI', 'JUDUL SKRIPSI']
      all.each do |graduate|
        # data = URI.parse(graduate.image_url)
        # csv << [data]
        # format.csv { send_data @graduates.to_csv(['NAMA','STAMBUK','TANGGAL LAHIR','FAKULTAS','PRODI', 'PEMBIMBING I', 'PEMBIMBING II', 'JUDUL SKRIPSI']) }
        csv << [graduate.name, graduate.stambuk, graduate.birth_date, graduate.faculty.name, graduate.major.name, graduate.essay.title ||= atau]
      end
    end
  end

  def self.type_export_list
    [
      ["Halaman Ini"],
      ["Semua"],
      ["FKIP"],
      ["FATEK"],
      ["FISIP"],
      ["FEKON"],
      ["FKM"]
    ]
  end

  # def validate_stambuk_for_one_email
  #   if User.where(stambuk: stambuk).present??
  #     errors.add(:stambuk, :invalid)
  #   end
  # end

  private
  def self.search(query)
      where("lower(name) like lower(?) or lower(email) like lower(?) or stambuk like ? ", "%#{query}%", "%#{query}%", "%#{query}%")
  end
end
