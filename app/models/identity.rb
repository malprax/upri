# == Schema Information
#
# Table name: identities
#
#  id                 :bigint(8)        not null, primary key
#  kategori           :string
#  file_identity_data :text
#  user_id            :bigint(8)
#  user_type          :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_identities_on_user_id  (user_id)
#

class Identity < ApplicationRecord
  belongs_to :user, foreign_key: :user_id
  include IdentityUploader::Attachment.new(:file_identity)
end
