# == Schema Information
#
# Table name: lecture_lists
#
#  id                 :bigint(8)        not null, primary key
#  name               :string
#  nidn               :string
#  nip                :string
#  status             :string
#  jabatan_struktural :string
#  major_id           :integer
#  faculty_id         :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class LectureList < ApplicationRecord
  belongs_to :major,  inverse_of: :lecture_lists
  belongs_to :faculty, inverse_of: :lecture_lists

  def self.status_list
    [
      ["Dosen Dipekerjakan (DPK)", "dosen_dipekerjakan"],
      ["Dosen Yayasan", "dosen_yayasan"],
      ["Dosen Luar Biasa", "dosen_luar_biasa"]
    ]
  end

  def self.prodi_list
    [
      ["Teknik Pertambangan", "teknik_pertambangan"],
      ["Teknik Mesin", "teknik_mesin"],
      ["Teknik Informatika", "teknik_informatika"]
    ]
  end


end
