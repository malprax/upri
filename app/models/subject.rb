# == Schema Information
#
# Table name: subjects
#
#  id            :bigint(8)        not null, primary key
#  matakuliah    :string
#  kode          :string
#  kredit        :integer
#  semester      :string
#  kategori      :string
#  tahun_berlaku :string
#  khusus        :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  faculty       :string
#  major         :string
#  tags          :string           is an Array
#
# Indexes
#
#  index_subjects_on_tags  (tags) USING gin
#

class Subject < ApplicationRecord
  include PgSearch::Model

  attr_accessor :teknik, :semester_ganjil, :semester_genap
  validates_presence_of :kode, :kredit, :matakuliah, :kategori, :semester

  has_many :subjectplans_subjects
  has_many :subject_plans, through: :subjectplans_subjects

  # default_scope -> { order(:semester) }
  # scope :kategori_matakuliah, lambda{|kategori_matakuliah| where(:kategori => kategori_matakuliah)}
  # scope :semester, lambda{|semester| where(:semester => semester)}
  # scope :prodi, lambda{|prodi| where(:major => prodi)}
  # scope :khusus, lambda{|khusus| where(:khusus => khusus)}
  # scope :tahun_berlaku, lambda{|tahun_berlaku| where tahun_berlaku: tahun_berlaku}
  # scope :word_search, -> (query){where("lower(matakuliah) like lower(?) or lower(kode) like lower(?) or lower(faculty) like lower(?) or lower(major) like lower(?)", "%#{query}%", "%#{query}%", "%#{query}%", "%#{query}%")}
  # validates_uniqueness_of :kode

  # pg_search_scope :word_search, against: [:email], associated_against: {
  #   major: [:name],
  #   student_profile: [:nama, :nomor_induk_mahasiswa]
  # }

  pg_search_scope :word, against: [:matakuliah, :kode, :faculty, :major]
  pg_search_scope :tahun_berlaku, against: [:tahun_berlaku]
  pg_search_scope :khusus, against: [:khusus]
  pg_search_scope :major, against: [:major]
  pg_search_scope :semester, against: [:semester]
  pg_search_scope :kategori_matakuliah, against: [:kategori]
  before_save :add_tags
  LIST_STATUS =
    {
      "MKP" => "Mata Kuliah Pilihan",
      "MPK" => "Mata Kuliah Pengembangan Kepribadian",
      "MKK" => "Mata Kuliah Keilmuan dan kepribadian",
      "MKB" => "Mata Kuliah Keahlian Berkarya",
      "MPB" => "Mata Kuliah Perilaku Berkarya",
      "MBB" => "Mata Kuliah Berkehidupan Bersama"
    }

  NOMOR_INDUK_MAHASISWA_TAMBANG_LIST =
    {
      "Angkatan 2015 umum" => "015311056",
      "Angkatan 2015 eksplorasi" => "015312056",
      "Angkatan 2016 umum" => "016311064",
      "Angkatan 2016 eksplorasi" => "016312064",
      "Angkatan 2017 umum" => "1731101030",
      "Angkatan 2017 eksplorasi" => "1731201030",
      "Angkatan 2018" => "1831201002",
      "Angkatan 2019" => "1931201026"
    }

  def self.kategori_matakuliah_list
    {
      "Universitas" => "universitas",
      "Fakultas" => "fakultas",
      "Program Studi" => "program_studi",
      "Program Studi Pilihan" => "pilihan"
    }
  end

  def self.tahun_berlaku_list
    {
      "2017 dan tahun sebelumnya" => "2014",
      "2018 dan tahun kemudian" => "2018"
    }
  end

  def self.khusus_list
    {
      "Tambang Umum"=>"tambang umum",
      "Tambang Khusus"=>"tambang eksplorasi"
    }
  end

  def self.semester_list
    {
      "I" => "1",
      "II" => "2",
      "III" => "3",
      "IV" => "4",
      "V" => "5",
      "VI" => "6",
      "VII" => "7",
      "VIII" => "8"
    }
  end

  def self.faculty_list
    {
      # "Fakultas Ilmu Keguruan Dan Pendidikan" => "ilmu keguruan dan pendidikan",
      # "Fakultas Ilmu Sosial Dan Politik" => "ilmu sosial dan politik",
      # "Fakultas Kesehatan Masyarakat" => "kesehatan masyarakat",
      "Fakultas Teknik" => "teknik"
      # "Fakultas Ekonomi" => "ekonomi"
    }
  end

  FACULTY_LIST =
    {
      # "Fakultas Ilmu Keguruan Dan Pendidikan" => "ilmu keguruan dan pendidikan",
      # "Fakultas Ilmu Sosial Dan Politik" => "ilmu sosial dan politik",
      # "Fakultas Kesehatan Masyarakat" => "kesehatan masyarakat",
      "Fakultas Teknik" => "teknik"
      # "Fakultas Ekonomi" => "ekonomi"
    }

  MAJOR_LIST =
    {
      # "Pendidikan Biologi" => "pendidikan_biologi",
      # "Pendidikan Matematika" => "pendidikan_matematika",
      # "Pendidikan PPKN" => "pendidikan_ppkn",
      # "Pendidikan Sejarah" => "sejarah",
      # "Teknologi Pendidikan" => "teknologi",
      # "Ilmu Administrasi Negara" => "administrasi negara",
      # "Ilmu Komunikasi" => "komunikasi",
      # "Kesehatan Masyarakat" => "kesehatan masyarakat",
      "Teknik Pertambangan" => "pertambangan",
      "Teknik Mesin" => "mesin",
      "Teknik Informatika" => "informatika"
      # "Manajemen" => "manajeme nhj7iyhgut5t5tujgtumb                                                                   c       shbj  myyy  nmg ngm n",
      # "Akuntansi" => "akuntansi"
    }

  def self.program_studi_list
    {
      # "Pendidikan Biologi" => "biologi",
      # "Pendidikan Matematika" => "matematika",
      # "Pendidikan PPKN" => "ppkn",
      # "Pendidikan Sejarah" => "sejarah",
      # "Teknologi Pendidikan" => "teknologi",
      # "Ilmu Administrasi Negara" => "administrasi negara",
      # "Ilmu Komunikasi" => "komunikasi",
      # "Kesehatan Masyarakat" => "kesehatan masyarakat",
      "Teknik Pertambangan" => "pertambangan",
      "Teknik Mesin" => "mesin",
      "Teknik Informatika" => "informatika"
      # "Manajemen" => "manajemen",
      # "Akuntansi" => "akuntansi"
    }
  end

  def add_tags
    jenis_matakuliah = self.kategori
    case jenis_matakuliah
    when "universitas"
      self.tags = ['biologi','matematika','ppkn', 'sejarah', 'teknologi', 'administrasi negara', 'komunikasi', 'kesehatan masyarakat', 'pertambangan', 'mesin', 'informatika', 'manajemen', 'akuntansi']
    when "fakultas"
      fakultas = self.faculty
      case fakultas
      when "teknik"
        self.tags = ['pertambangan', 'mesin', 'informatika']
      when "ekonomi"
        self.tags = ['manajemen', 'akuntansi']
      when "kesehatan masyarakat"
        self.tags = ['kesehatan masyarakat']
      when "ilmu sosial dan politik"
        self.tags = ['komunikasi','administrasi negara']
      when "ilmu keguruan dan pendidikan"
        self.tags = ['teknologi','sejarah','ppkn', 'matematika', 'biologi']
      end
    when "program_studi"
      program_studi = self.major
      case program_studi
      when "pertambangan"
        pilihan = self.khusus

        case pilihan
        when "tambang umum"
          self.tags = ["pertambangan"]
        when "tambang eksplorasi"
          self.tags = ["pertambangan"]
        else
          self.tags = ["pertambangan"]
        end

      when "mesin"
        self.tags = ["mesin"]
      when "informatika"
        self.tags = ["informatika"]
      end
    when "pilihan"
      program_studi_pilihan = self.khusus
      case program_studi_pilihan
      when "tambang umum"
        self.tags = ["pertambangan"]
      when "tambang eksplorasi"
        self.tags = ["pertambangan"]
      end
    end
  end


end
