# == Schema Information
#
# Table name: foundations
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Foundation < ApplicationRecord
  has_one :university
end
