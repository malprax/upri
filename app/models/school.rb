# == Schema Information
#
# Table name: schools
#
#  id         :bigint(8)        not null, primary key
#  nama       :string
#  jurusan    :string
#  student_id :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_schools_on_student_id  (student_id)
#

class School < ApplicationRecord
  belongs_to :student

  def self.jurusan_list
    [
      "IPA","IPS","Kejuruan","Pesantren","Dan Lain-Lain"
    ]
  end
end
