# == Schema Information
#
# Table name: kelurahans
#
#  id          :bigint(8)        not null, primary key
#  nama        :string
#  district_id :bigint(8)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_kelurahans_on_district_id  (district_id)
#
# Foreign Keys
#
#  fk_rails_...  (district_id => districts.id)
#

class Kelurahan < ApplicationRecord
  belongs_to :district
end
