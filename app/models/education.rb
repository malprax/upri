# == Schema Information
#
# Table name: educations
#
#  id                     :bigint(8)        not null, primary key
#  level                  :string
#  major                  :string
#  institute              :string
#  graduation_year        :string
#  education_history_id   :integer
#  education_history_type :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class Education < ApplicationRecord
  has_many :mahasiswa_profiles, dependent: :nullify
end
