# == Schema Information
#
# Table name: states
#
#  id         :bigint(8)        not null, primary key
#  nama       :string
#  kode       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class State < ApplicationRecord
  has_many :districts, dependent: :destroy
  has_many :mahasiswa_profiles, dependent: :nullify
end
