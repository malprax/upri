# == Schema Information
#
# Table name: districts
#
#  id         :bigint(8)        not null, primary key
#  nama       :string
#  state_id   :integer
#  id_wilayah :string
#  id_negara  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class District < ApplicationRecord
  belongs_to :state, optional: true
  has_many :mahasiswa_profiles, dependent: :nullify
end
