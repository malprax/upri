# == Schema Information
#
# Table name: positions
#
#  id                       :bigint(8)        not null, primary key
#  user_id                  :bigint(8)
#  structural_position_id   :integer
#  structural_position_type :string
#  role                     :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
# Indexes
#
#  index_positions_on_user_id  (user_id)
#
class Position < ApplicationRecord
end
