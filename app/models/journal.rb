# == Schema Information
#
# Table name: journals
#
#  id           :bigint(8)        not null, primary key
#  volume       :string
#  number       :string
#  publish_time :datetime
#  published    :boolean          default(FALSE)
#  issn         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Journal < ApplicationRecord
  has_many :articles, dependent: :destroy
  has_many_attached :journal_files
end
