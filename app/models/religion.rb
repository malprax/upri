# == Schema Information
#
# Table name: religions
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Religion < ApplicationRecord
  has_many :lectures, dependent: :destroy
  has_many :staffs, dependent: :destroy
end
