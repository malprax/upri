# == Schema Information
#
# Table name: special_needs
#
#  id         :bigint(8)        not null, primary key
#  nama       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SpecialNeed < ApplicationRecord
  has_many :mahasiswa_profiles, dependent: :nullify
end
