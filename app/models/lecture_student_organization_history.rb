# == Schema Information
#
# Table name: lecture_student_organization_histories
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  year       :string
#  role       :string
#  place      :string
#  lecture_id :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_lecture_student_organization_histories_on_lecture_id  (lecture_id)
#
class LectureStudentOrganizationHistory < ApplicationRecord
  belongs_to :lecture
end
