# == Schema Information
#
# Table name: item_accreditations
#
#  id               :bigint(8)        not null, primary key
#  item             :string
#  academic_year_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class ItemAccreditation < ApplicationRecord
end
