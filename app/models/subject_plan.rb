# == Schema Information
#
# Table name: subject_plans
#
#  id                          :bigint(8)        not null, primary key
#  academic_year_id            :integer
#  student_id                  :bigint(8)
#  is_approved                 :boolean          default(FALSE)
#  is_passed                   :boolean          default(FALSE)
#  is_printed                  :boolean          default(FALSE)
#  stuctural_id                :integer
#  last_grade_poin_average     :float
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  penasehat_akademik          :string
#  nip_nidn_penasehat_akademik :string
#  dekan                       :string
#  nip_nidn_dekan              :string
#
# Indexes
#
#  index_subject_plans_on_academic_year_id  (academic_year_id)
#  index_subject_plans_on_student_id        (student_id)
#
require 'barby/barcode/code_128'
require 'barby/barcode/qr_code'
class SubjectPlan < ApplicationRecord
  include HasBarcode
  include PgSearch::Model
  pg_search_scope :search, associated_against: {
    student_profile: [:nama, :nomor_induk_mahasiswa]
  }

  belongs_to :student
  belongs_to :academic_year
  has_one_attached :subject_plan_file

  has_many :subjectplans_subjects
  has_many :subjects, through: :subjectplans_subjects
  has_one :student_profile, through: :student

  accepts_nested_attributes_for :subjects
  before_save :cari_penasehat_akademik
  after_touch :set_payment_on_sp


  # validates_uniqueness_of :student_id, scope: [:academic_year_id]
  validates_uniqueness_of :academic_year_id, scope: [:student_id], message: "Maaf KRS dengan Tahun Akademik Dan Semester Yang Anda Isi Sudah Ada"

  has_barcode :barcode,
      outputter: :svg,
      # type: Barby::Code128,
      type: Barby::QrCode,
      value: Proc.new{|c| c.codenya}

  attr_accessor :genbar
  # barcode = "#{self.created_at}"+"#{self.student.student_profile.nama}"+"#{self.student.student_profile.nomor_induk_mahasiswa}"

  def is_not_approved?
    self.is_approved == false
  end
  def codenya
    self.genbar = "KRS ONLINE FATEK UPRI Makassar"
  end

  def cari_penasehat_akademik
    unless AcademicYear.aktif.semester == "Pendek"
      angkatan_2019_rafiuddin = 1931201001..1931201035
      angkatan_2019_riza = 1931201036..1931201071
      angkatan_2019_hasni = 1931201072..1931201101
      angkatan_2018_made = 1831201001..1831201030
      angkatan_2018_faizal = 1831201001..1831201060
      angkatan_2017_rahma = 1731201001..1731201030
      angkatan_2017_syamsuddin = 1731201031..1731201060
      angkatan_2016_te_elifas = 16312001..16312052
      angkatan_2016_tu_hafsa = 16311001..16311065
      angkatan_2015_te_baso = 15312001..15312065
      angkatan_2015_tu_ruth = 15311001..15311065
      angkatan_2015_te_riza = 15312066..15312165
      angkatan_2015_tu_riza = 15311066..15311165
      angkatan_2014_te_aslim = 14312001..14312200
      angkatan_2014_tu_aslim = 14311001..14311200

      nomor_induk_mahasiswa = self.student.student_profile.nomor_induk_mahasiswa.to_i

      case nomor_induk_mahasiswa
      when angkatan_2019_rafiuddin
        self.penasehat_akademik = "Ir. H. Rafiuddin, M.T."
        self.nip_nidn_penasehat_akademik = "0010106301"
      when angkatan_2019_riza
        self.penasehat_akademik = "Dr. Ir. Risa Bernadip Umar, M.Si., M.Pd."
        self.nip_nidn_penasehat_akademik = "0016116101"
      when angkatan_2019_hasni
        self.penasehat_akademik = "Ir. Hj. Hasni Kasim, M.T."
        self.nip_nidn_penasehat_akademik = "0014116501"
      when angkatan_2018_made
        self.penasehat_akademik = "Ir. H. Made Darma, M.T."
        self.nip_nidn_penasehat_akademik = "007055602"
      when angkatan_2018_faizal
        self.penasehat_akademik = "Ir. H. Faizal Suyuti, M.M."
        self.nip_nidn_penasehat_akademik = "0001035881"
      when angkatan_2017_rahma
        self.penasehat_akademik = "Dr. Hj. Rahma Tompo, M.Kes."
        self.nip_nidn_penasehat_akademik = "0005115402"
      when angkatan_2017_syamsuddin
        self.penasehat_akademik = "Syamsuddin, S.T., M.Biotech."
        self.nip_nidn_penasehat_akademik = "0931127107"
      when angkatan_2016_te_elifas
        self.penasehat_akademik = "Dr. Ir. Elifas Bunga, M.T."
        self.nip_nidn_penasehat_akademik = "0019025601"
      when angkatan_2016_tu_hafsa
        self.penasehat_akademik = "Dra. Hafsa Djanieb, M.Si."
        self.nip_nidn_penasehat_akademik = "195008171988012001"
      when angkatan_2015_te_baso
        self.penasehat_akademik = "Ir. H. Baso Junain, M.M"
        self.nip_nidn_penasehat_akademik = "0031125605"
      when angkatan_2015_tu_ruth
        self.penasehat_akademik = "Ir. Ruth Bunga Ranggu, M.Si"
        self.nip_nidn_penasehat_akademik = "0924066201"
      when angkatan_2015_te_riza
        self.penasehat_akademik = "Dr. Ir. Risa Bernadip Umar, M.Si., M.Pd."
        self.nip_nidn_penasehat_akademik = "0016116101"
      when angkatan_2015_tu_riza
        self.penasehat_akademik = "Dr. Ir. Risa Bernadip Umar, M.Si., M.Pd."
        self.nip_nidn_penasehat_akademik = "0016116101"
      when angkatan_2014_te_aslim
        self.penasehat_akademik = "Aslim Muda Azis, S.Pd., M.T."
        self.nip_nidn_penasehat_akademik = "0907058204"
      when angkatan_2014_tu_aslim
        self.penasehat_akademik = "Aslim Muda Azis, S.Pd., M.T."
        self.nip_nidn_penasehat_akademik = "0907058204"
      end

      self.dekan = "Ir. H. Rafiuddin, M.T."
      self.nip_nidn_dekan ="196310161991031011"
    end
  end

  def self.tag_params_major(major)
    case major
    when "Pendidikan Biologi"
       "biologi"
    when "Pendidikan Matematika"
       "matematika"
    when "Pendidikan PPKN"
       "ppkn"
    when "Pendidikan Sejarah"
       "sejarah"
    when "Teknologi Pendidikan"
       "teknologi"
    when "Ilmu Administrasi Negara"
       "administrasi negara"
    when "Ilmu Komunikasi"
       "komunikasi"
    when "Kesehatan Masyarakat"
       "kesehatan masyarakat"
    when "Teknik Pertambangan"
       "pertambangan"
    when "Teknik Mesin"
       "mesin"
    when "Teknik Informatika"
       "informatika"
    when "Manajemen"
       "manajemen"
    when "Akuntansi"
       "akuntansi"
    end
  end

  def is_not_approved?
    self.is_approved == false
  end

  def set_payment_on_sp
    @bank = Bank.where(name: "BRI", account_number: "034301001373303").first
    @totalpay = (self.subjects.sum(:kredit) * 75000) + 50000
    @academic_year_aktif = AcademicYear.where(is_active: true).first
    # @payment = Payment.where(kind: "semester pendek", academic_year_id: AcademicYear.aktif.id, in_number: @totalpay, is_approved:true, paid: self.updated_at, bank_id: @bank.id, student_id: self.student_id ).first
    Rails.logger.info('----------------------------started--------------------------------')
    if @academic_year_aktif.semester == "Pendek" && self.is_not_approved?
      Rails.logger.info('----------------------------execute 1--------------------------------')
      @payment = Payment.where(kind: "semester pendek", academic_year_id: @academic_year_aktif.id, in_number: @totalpay, is_approved:true, paid: self.updated_at, bank_id: @bank.id, student_id: self.student_id ).first_or_create
    elsif self.delete?
    end
    Rails.logger.info('----------------------------passed--------------------------------')
  end
end
