# == Schema Information
#
# Table name: graduate_profiles
#
#  id                   :bigint(8)        not null, primary key
#  ipk                  :float
#  yudisium_date        :date
#  yudisium_letter      :string
#  alumni_number        :integer
#  predicet_of_yudisium :string
#  graduate_id          :bigint(8)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_graduate_profiles_on_graduate_id  (graduate_id)
#

class GraduateProfile < ApplicationRecord
  belongs_to :graduate, optional: true

  # default_scope {data_scope}
  #
  # def self.data_scope
  #   all.sort_by do |item|
  #     DateTime.parse(item[:yudisium_date]).to_i * 1
  #   end
  # end

  def graduate_profile_ipk
    GraduateProfile.ipk
  end

  def graduate_profile_yudisium_date
    GraduateProfile.yudisium_date
  end

  def graduate_profile_yudisium_letter
    GraduateProfile.yudisium_letter
  end

  def graduate_profile_alumni_number
    GraduateProfile.alumni_number
  end

  def graduate_profile_predicet_of_yudisium
    GraduateProfile.predicet_of_yudisium
  end

end
