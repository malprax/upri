# == Schema Information
#
# Table name: lecture_scientific_works
#
#  id              :bigint(8)        not null, primary key
#  year            :string
#  science_product :string
#  publisher       :string
#  lecture_id      :bigint(8)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_lecture_scientific_works_on_lecture_id  (lecture_id)
#
class LectureScientificWork < ApplicationRecord
  belongs_to :lecture
end
