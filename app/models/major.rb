# == Schema Information
#
# Table name: majors
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  code       :string
#  faculty_id :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_majors_on_faculty_id  (faculty_id)
#

class Major < ApplicationRecord
  belongs_to :faculty
  has_many :student_profiles, dependent: :nullify
  has_many :staff_profiles, dependent: :nullify
  has_many :lecture_profiles, dependent: :nullify
  has_many :graduates, class_name: "Graduate", dependent: :nullify
  has_many :dummy_lectures
  has_many :mahasiswa_profiles,  dependent: :nullify
  has_many :lecture_lists, inverse_of: :major
  has_many :lectures, class_name: "Lecture", dependent: :nullify
  has_many :students, dependent: :nullify
end
