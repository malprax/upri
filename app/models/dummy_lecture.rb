# == Schema Information
#
# Table name: dummy_lectures
#
#  id                  :bigint(8)        not null, primary key
#  nama                :string
#  nidn                :integer
#  nip                 :integer
#  gelar_depan_nama    :string
#  jabatan_akademik    :string
#  pendidikan          :string
#  major_id            :bigint(8)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  gelar_belakang_nama :string
#  status              :string
#
# Indexes
#
#  index_dummy_lectures_on_major_id  (major_id)
#

class DummyLecture < ApplicationRecord
  has_one :graduate
  has_one :essay, through: :graduate
  has_many :schedule_subjects

  attr_accessor :nama_dengan_gelar_dan_prodi

  scope :semua_kecuali, ->(jurusan){ where.not(major_id: jurusan) }

  belongs_to :major
  PROGRAM_STUDI = {
    "pendidikan_biologi" => "Pendidikan Biologi",
    "pendidikan_matematika" => "Pendidikan Matematika",
    "pendidikan_ppkn" => "Pendidikan PPKN",
    "pendidikan_sejarah" => "Pendidikan Sejarah",
    "teknologi_pendidikan" => "Teknologi Pendidikan",
    "ilmu_administrasi_negara" => "Ilmu Administrasi Negara",
    "ilmu_komunikasi" => "Ilmu Komunikasi",
    "kesehatan_masyarakat" => "Kesehatan Masyarakat",
    "teknik_pertambangan" => "Teknik Pertambangan",
    "teknik_mesin" => "Teknik Mesin",
    "teknik_informatika" => "Teknik Informatika",
    "manajemen" => "Manajemen",
    "akuntasi" => "Akuntansi"
  }

  JABATAN_AKADEMIK = {
    "tenaga_pengajar" => "TENAGA PENGAJAR",
    "asisten_ahli" => "ASISTEN AHLI",
    "lektor" => "LEKTOR",
    "lektor_kepala" => "LEKTOR KEPALA"
  }

  def pembimbing_1
    "#{self.nama.titleize} - #{self.major.name}"
  end

  def pembimbing_2
    "#{self.nama.titleize} - #{self.major.name}"
  end

  def nama_dengan_gelar
    if self.gelar_depan_nama.present? && self.gelar_belakang_nama.blank?
      "#{self.gelar_depan_nama} #{self.nama}"
    elsif self.gelar_depan_nama.blank? && self.gelar_belakang_nama.present?
      "#{self.nama}, #{self.gelar_belakang_nama}"
    elsif self.gelar_depan_nama.present? && self.gelar_belakang_nama.present?
      "#{self.gelar_depan_nama} #{self.nama}, #{self.gelar_belakang_nama}"
    else
      "#{self.nama}"
    end
  end

  def nama_dengan_gelar_dan_prodi
    if self.gelar_depan_nama.present? && self.gelar_belakang_nama.blank?
      "#{self.gelar_depan_nama} #{self.nama} - #{self.major.name}"
    elsif self.gelar_depan_nama.blank? && self.gelar_belakang_nama.present?
      "#{self.nama}, #{self.gelar_belakang_nama} - #{self.major.name}"
    elsif self.gelar_depan_nama.present? && self.gelar_belakang_nama.present?
      "#{self.gelar_depan_nama} #{self.nama}, #{self.gelar_belakang_nama} - #{self.major.name}"
    else
      "#{self.nama}"
    end
  end

end
