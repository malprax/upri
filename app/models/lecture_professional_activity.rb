# == Schema Information
#
# Table name: lecture_professional_activities
#
#  id         :bigint(8)        not null, primary key
#  year       :string
#  activity   :string
#  place      :string
#  lecture_id :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_lecture_professional_activities_on_lecture_id  (lecture_id)
#
class LectureProfessionalActivity < ApplicationRecord
  belongs_to :lecture
end
