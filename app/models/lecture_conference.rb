# == Schema Information
#
# Table name: lecture_conferences
#
#  id           :bigint(8)        not null, primary key
#  year         :string
#  conference   :string
#  presented_by :string
#  role         :string
#  lecture_id   :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_lecture_conferences_on_lecture_id  (lecture_id)
#
class LectureConference < ApplicationRecord
  belongs_to :lecture
end
