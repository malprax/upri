# == Schema Information
#
# Table name: parents
#
#  id                          :bigint(8)        not null, primary key
#  nama_ayah                   :string
#  pekerjaan_ayah              :string
#  parents_guardians_handphone :string
#  nama_ibu                    :string
#  pekerjaan_ibu               :string
#  nama_wali                   :string
#  pekerjaan_wali              :string
#  student_id                  :bigint(8)
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_parents_on_student_id  (student_id)
#

class Parent < ApplicationRecord
  belongs_to :student, optional: true
  # has_one :alamat_ayah, class_name: "Address"
  # has_one :alamat_ibu, class_name: "Address"
  # has_one :alamat_wali, class_name: "Address"

  def self.pekerjaan_list
    [
      "Karyawan Swasta",
      "Karyawan BUMN",
      "PNS/TNI/POLRI",
      "Wiraswasta",
      "Pensiunan",
      "Lain-lain"
    ]
  end
end
