# == Schema Information
#
# Table name: lecture_teaching_histories
#
#  id              :bigint(8)        not null, primary key
#  subject         :string
#  major           :string
#  institute       :string
#  semester_status :string
#  academic_year   :string
#  lecture_id      :bigint(8)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_lecture_teaching_histories_on_lecture_id  (lecture_id)
#
class LectureTeachingHistory < ApplicationRecord
  belongs_to :lecture
end
