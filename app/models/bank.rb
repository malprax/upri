# == Schema Information
#
# Table name: banks
#
#  id             :bigint(8)        not null, primary key
#  name           :string
#  address        :string
#  company        :string
#  account_name   :string
#  account_number :string
#  code           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
class Bank < ApplicationRecord
  has_many :payments, dependent: :destroy
  validates_presence_of :name, :account_name, :account_number
  has_one_attached :logo_file
  # validate :logo_file_size

  # private

  # def logo_file_size
  #   errors.add :logo_file, 'file terlalu besar' if logo_file.blob.byte_size > 2.megabytes
  # end


end
