# == Schema Information
#
# Table name: lecture_papers
#
#  id           :bigint(8)        not null, primary key
#  year         :string
#  paper        :string
#  presented_by :string
#  lecture_id   :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_lecture_papers_on_lecture_id  (lecture_id)
#
class LecturePaper < ApplicationRecord
  belongs_to :lecture
end
