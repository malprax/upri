# == Schema Information
#
# Table name: payment_forms
#
#  id                :bigint(8)        not null, primary key
#  kategori          :string
#  file_payment_data :text
#  user_type         :string
#  users_id          :bigint(8)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_payment_forms_on_users_id  (users_id)
#

class PaymentForm < ApplicationRecord
  belongs_to :user, foreign_key: :user_id
  include PaymentFormUploader::Attachment.new(:file_payment)
  # validates_presence_of :kategori
end
