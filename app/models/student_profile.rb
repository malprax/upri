# == Schema Information
#
# Table name: student_profiles
#
#  id                       :bigint(8)        not null, primary key
#  student_id               :integer
#  nama                     :string
#  nomor_induk_mahasiswa    :string
#  jenis_kelamin            :string
#  tempat_lahir             :string
#  tanggal_lahir            :datetime
#  handphone                :string
#  agama                    :string
#  kewarganegaraan          :string
#  major_id                 :bigint(8)
#  status                   :string
#  keterangan_tinggal       :string
#  is_active                :boolean          default(FALSE)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  angkatan                 :string
#  konsentrasi              :string
#  tahun_berlaku_matakuliah :string
#
# Indexes
#
#  index_student_profiles_on_major_id  (major_id)
#

class StudentProfile < ApplicationRecord
  belongs_to :student
  validates_presence_of :student_id, :nama, :jenis_kelamin, :tempat_lahir, :tanggal_lahir
  # validates_uniqueness_of :nomor_induk_mahasiswa
  before_save :set_angkatan
  def set_angkatan
    #
    angkatan_2019 = 1931201001..1931201201
    angkatan_2018 = 1831201001..1831201260
    angkatan_2017 = 1731201001..1731201260
    angkatan_2016_te = 16312001..16312252
    angkatan_2016_tu = 16311001..16311265
    angkatan_2015_te = 15312001..15312265
    angkatan_2015_tu = 15311001..15311265
    angkatan_2014_te = 14312001..14312200
    angkatan_2014_tu = 14311001..14311200
    #
    nomor_induk = self.nomor_induk_mahasiswa.to_i
    case nomor_induk
    when angkatan_2019
      self.angkatan = "2019"
      self.tahun_berlaku_matakuliah = "2018"
      self.konsentrasi = "tambang umum"
    when angkatan_2018
      self.angkatan = "2018"
      self.tahun_berlaku_matakuliah = "2018"
      self.konsentrasi = "tambang umum"
    when angkatan_2017
      self.angkatan = "2017"
      self.tahun_berlaku_matakuliah = "2014"
      self.konsentrasi = "tambang umum"
    when angkatan_2016_te
      self.angkatan = "2016"
      self.tahun_berlaku_matakuliah = "2014"
      self.konsentrasi = "tambang eksplorasi"
    when angkatan_2016_tu
      self.angkatan = "2016"
      self.tahun_berlaku_matakuliah = "2014"
      self.konsentrasi = "tambang umum"
    when angkatan_2015_te
      self.angkatan = "2015"
      self.tahun_berlaku_matakuliah = "2014"
      self.konsentrasi = "tambang eksplorasi"
    when angkatan_2015_tu
      self.angkatan = "2015"
      self.tahun_berlaku_matakuliah = "2014"
      self.konsentrasi = "tambang umum"
    when angkatan_2014_te
      self.angkatan = "2014"
      self.tahun_berlaku_matakuliah = "2014"
      self.konsentrasi = "tambang eksplorasi"
    when angkatan_2014_tu
      self.angkatan = "2014"
      self.tahun_berlaku_matakuliah = "2014"
      self.konsentrasi = "tambang umum"
    end
  end

  def self.keterangan_tinggal_list
    [
      "Kedua Orang Tua",
      "Ayah",
      "Ibu",
      "Wali",
      "Sendiri"
    ]
  end

  def self.jenis_kelamin_list
    [
      "Laki-laki",
      "Perempuan"
    ]
  end

  def self.agama_list
    [
      "Islam",
      "Katolik",
      "Kristen Protestan",
      "Hindu",
      "Budha"
    ]
  end

  def self.kewarganegaraan_list
    [
      "WNI",
      "WNI Keturunan",
      "WNA"
    ]
  end

  def self.status_mahasiswa_list
    [
      "Belum Aktif",
      "Aktif",
      "Skorsing",
      "Cuti",
      "Drop Out",
      "Alumni",
      "Lain lain"
    ]
  end
end
