document.addEventListener('turbolinks:load', () =>{
    var markup;
    var biayaRegistrasi = 50000;
    var totalAmount = 0;
    var checkSemester = $("#table1").attr('value');


    $('#kreditTotal').text(totalAmount); //default kreditTotal


    // default views after loading
    var $defaultChecked = $('#table1 input[type="checkbox"]:checked');
    $.each($defaultChecked, function(){
      var defaultKode =  $(this).closest('tr').find('#kodeMatakuliah').text();
      var defaultMatakuliah = $(this).closest('tr').find('#namaMatakuliah').text();
      var defaultKredit = $(this).closest('tr').find('#satuanKredit').attr('value');
      var defaultBiaya = defaultKredit*75000;
      var defaultMarkup = "<tr class='is-size-7'><td class='has-text-centered'>" + defaultKode + "</td ><td class='targetMatakuliah'>" + defaultMatakuliah + "</td><td class='has-text-centered'>" + defaultKredit + "</td><td class='has-text-centered'>" + defaultBiaya + "</td></tr>";
      totalAmount += parseInt(defaultKredit);
      totalBiaya = biayaRegistrasi + (totalAmount*75000);
      $('#table2 tbody').append(defaultMarkup);
    });


    $('#kreditTotal').text(totalAmount); //default kreditTotal
    $('#biayaTotal').text(totalBiaya); //default kreditTotal

    //event checkbox on click
    $('#table1 input[type="checkbox"]').click(function(){
      var kode = $(this).closest('tr').find('#kodeMatakuliah').text();
      var matakuliah = $(this).closest('tr').find('#namaMatakuliah').text();
      var kredit = $(this).closest('tr').find('#satuanKredit').attr('value');
      var biaya = $(this).closest('tr').find('#satuanKredit').attr('value')*75000;

      if($(this).prop("checked") == true){
          markup = "<tr class='is-size-7'><td class='has-text-centered'>" + kode + "</td ><td class='targetMatakuliah'>" + matakuliah + "</td><td class='has-text-centered'>" + kredit + "</td><td class='has-text-centered'>" + biaya + "</td></tr>";
          // if(totalAmount <= 21){
          if(totalAmount <= parseInt(checkSemester)){
            // var lastRow = $('#table2 tr').find('#kreditTotal');
            totalAmount += parseInt(kredit);
            totalBiaya = 50000 + (totalAmount*75000);
            $('#table2 tbody').append(markup);

          }else{
            alert('anda tidak bisa lagi menambah matakuliah');
            $(this).prop('checked', false);
          }
      }else if($(this).prop("checked") == false){
        if (totalAmount > 0){
          var targetDeleted = $('#table2 td:contains("'+kode+'")').closest('tr');
          targetDeleted.remove();
          totalAmount -= parseInt(kredit);
          // totalBiaya = totalAmount*75000;
          totalBiaya = 50000 + (totalAmount*75000);
        }
      }
      $('#kreditTotal').text(totalAmount);
      $('#biayaTotal').text(totalBiaya);

    });
});
