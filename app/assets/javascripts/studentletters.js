$(document).on("turbolinks:load",function() {
  $('#studentletter_jenis_surat').on('change', function() {
    $('.studentletter_input').addClass('hidden');
    if (this.value == "Keterangan Kuliah Biasa"){
      // $('#'+this.value).removeClass('hidden');
      $('#biasa_beasiswa_kegiatan_cuti_akreditasi_biasa').removeClass('hidden');
    }

    else if(this.value == "Keterangan Kuliah Tanggungan"){
      $('#biasa_beasiswa_kegiatan_cuti_akreditasi_biasa').removeClass('hidden');
      $('#keterangan_tanggungan').removeClass('hidden');
    }
    else if(this.value == "Keterangan Akreditasi"){
      $('#biasa_beasiswa_kegiatan_cuti_akreditasi_biasa').removeClass('hidden');
      $('#lulus_akreditasi_alumni').removeClass('hidden');
    }
    else if(this.value == "Keterangan Beasiswa"){
      $('#biasa_beasiswa_kegiatan_cuti_akreditasi_biasa').removeClass('hidden');
      $('#lulus_akreditasi_alumni').removeClass('hidden');
    }
    else if(this.value == "Keterangan Kegiatan"){
      $('#biasa_beasiswa_kegiatan_cuti_akreditasi_biasa').removeClass('hidden');
      $('#lulus_akreditasi_alumni').removeClass('hidden');
    }
    else if(this.value == "Cuti Akademik"){
      $('#biasa_beasiswa_kegiatan_cuti_akreditasi_biasa').removeClass('hidden');
      $('#lulus_akreditasi_alumni').removeClass('hidden');
    }
    else if(this.value == "Keterangan Pindah"){
      $('#biasa_beasiswa_kegiatan_cuti_akreditasi_biasa').removeClass('hidden');
      $('#pindah').removeClass('hidden');
    }
    else if(this.value == "Perbaikan Nama pada PD-Dikti"){
      $('#biasa_beasiswa_kegiatan_cuti_akreditasi_biasa').removeClass('hidden');
      $('#lulus_akreditasi_alumni').removeClass('hidden');
    }
  });
});
