class GraduateMailer < ApplicationMailer
  def token_email user
    @user = user
    mail(to:@user.email, subject: 'Kode Token',from: 'admin@upri.ac.id')
  end
end
