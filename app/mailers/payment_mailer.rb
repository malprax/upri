class PaymentMailer < ApplicationMailer
  def approve_payment student, staff, payment
    # @users = %w{user staff}
    @student = student
    @staff = staff
    @payment = payment
    # @student = student

    # @users.each do |user|
      mail(to: @student.email, subject: 'Status Pembayaran Berhasil Di Approve', from: 'robot@teknik.upri.ac.id', bcc: ["kingmalprax@gmail.com"])
    # end
  end
end
