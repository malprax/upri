class MahasiswaMailer < ApplicationMailer
  default from: 'admin@upri.ac.id'
  layout 'mailer'

  def new_mahasiswa_waiting_for_active(email)
    @email = email
    mail(to: 'aulia@gmail.com', subject: 'Mahasiswa Baru Menunggu Aktivasi')
  end
end
