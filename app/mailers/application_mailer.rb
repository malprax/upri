class ApplicationMailer < ActionMailer::Base
  default from: 'robot@teknik.upri.ac.id'
  layout 'mailer'
end
