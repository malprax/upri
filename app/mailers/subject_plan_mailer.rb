class SubjectPlanMailer < ApplicationMailer
  def send_token_payment user
    @user = user
    mail(to:@user.email, subject: 'Kode Token Kartu Rencana Studi',from: 'robot@teknik.upri.ac.id', bcc: ["bungahati.2310@gmail.com", "kingmalprax@gmail.com", "risabernadip@gmail.com"])
  end
end
