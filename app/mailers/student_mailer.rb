class StudentMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.student_mailer.activation.subject
  #
  def activation student
    @user = student

    mail(to: @user.email, subject: 'Anda Berhasil Di Aktivasi', from: 'robot@teknik.upri.ac.id')
  end
end
