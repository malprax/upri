class CandidateMailer < ApplicationMailer
  def email_pendaftaran user
    @user = user
    mail(to:@user.email, subject: 'Formulir Mahasiswa Baru UPRI',from: 'admin@upri.ac.id')
  end
end
