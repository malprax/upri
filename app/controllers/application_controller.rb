class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale
  helper_method  :current_authenticate_graduate,
                 :current_academic_year,
                 :current_authenticate_staff,
                 :current_authenticate_student,
                 :current_staff,
                 :current_user_subject_plan,
                 :check_profile_student!,
                 :info_profile_student,
                 :info_profile_staff,
                 :current_student
                # :check_staff_is_active
  layout :layout_by_resource

  private
  def layout_by_resource
    if devise_controller?
      "login"
    else
      "application"
    end
  end

  # def check_staff_is_active
  #   unless current_user.is_staff? && current_user.is_active?
  #     redirect_to root_path, alert: 'Akun anda belum aktif'
  #   end
  # end

  def check_profile_present!
    if current_user.is_staff? #&& !current_user.staff_profile.present?
      redirect_to edit_staff_path(current_user), alert: 'Anda mesti mengisi profil terlebih dahulu'
    elsif current_user.is_student? #&& !current_user.student_profile.present?
      unless current_user.major.present?
        redirect_to edit_student_path(current_user), alert: 'Anda mesti mengisi profil terlebih dahulu'
      end
    elsif current_user.is_lecture? && !current_user.lecture_profile.present?
      redirect_to edit_lecture_path(current_user, type:"IDENTITAS DIRI"), alert: 'Anda mesti mengisi profil terlebih dahulu'
    end
  end

  def check_profile_student!
    return if current_user.student_profile.present?
    redirect_to edit_student_path(current_user), alert: 'Anda wajib mengisi profil iedntitas dan upload dokumen terlebih dahulu'
  end

  def current_user_subject_plan
    @current_user_subject_plan ||= current_user.subject_plans.last
  end

  def current_academic_year
    @current_academic_year = AcademicYear.aktif
  end
  def current_authenticate_graduate
      @current_graduate ||= Graduate.find(session[:user_id]) if session[:user_id]
  end

  def current_authenticate_staff
      @current_staff ||= Staff.find(session[:user_id]) if session[:user_id]
  end

  def current_authenticate_student
    @current_student ||= Student.find(session[:user_id]) if session[:user_id]
  end

  def current_staff
    @current_staff ||= Staff.find(params[:id])
  end

  def current_lecture
    @current_lecture ||= Lecture.find(params[:id])
  end



  def authenticate_admin
    redirect_to root_path, alert: "Anda Tidak Memiliki Akses" unless current_user.email=="kingmalprax@gmail.com"
  end

  def authenticate_lecture
    redirect_to token_session_path, alert: "Anda Tidak Memiliki Akses, Masukkan NIDN Dan Token Anda" if current_authenticate_lecture.nil?
  end

  def authenticate_graduate
    redirect_to token_session_path, alert: "Anda Tidak Memiliki Akses, Masukkan Stambuk Dan Token Anda" if current_authenticate_graduate.nil?
  end

  def authenticate_staff
    redirect_to root_path, alert: "Anda Tidak Memiliki Akses " unless current_user.is_staff?
  end



  def authenticate_phone_number
    redirect_to new_authenticate_phone_path, alert: "Handphone Anda Belum Diverifikasi, Pastikan Nomor Dan Handphone Aktif Untuk Dikirimi SMS" unless current_user.is_phone_verified?
  end

  def authenticate_user_active
     if current_user && current_user.is_not_active?
       redirect_to send("#{user_base_url.pluralize}_dashboards_path"), flash: { error: 'Akun anda belum aktif, staf kami akan mereview akun anda, mohon cek email anda secara berkala mengenai status anda' }
    end
  end

  def save_login_state
    if session[:user_id]
      flash[:notice] = "Anda Sudah Mengisi Token"
      redirect_to root_path
    end
  end

  def disable_nav
    @disable_nav = true
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def after_sign_in_path_for(resource)
    if resource.type
      send("#{user_base_url}_dashboards_path")
      # send("#{resource.type.downcase.pluralize}_dashboards_path")
    else
      flash[:alert] = "Anda tidak memiliki akses ke halaman ini"
      root_path
    end
  end

  def user_base_url
    current_user.type.downcase.pluralize
  end


end
