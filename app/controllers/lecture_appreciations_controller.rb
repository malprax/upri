class LectureAppreciationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_appreciation, only:[:edit, :update, :destroy]
  def new
    @appreciation = @lecture.lecture_appreciations.build
  end

  def create
    @appreciation = @lecture.lecture_appreciations.build(appreciation_params)
    if @appreciation.save
       redirect_to lecture_path(@lecture, type:"PENGHARGAAN"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @appreciation.update(appreciation_params)
      redirect_to lecture_path(@lecture, type:"PENGHARGAAN"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @appreciation.destroy
    redirect_to lecture_path(@lecture, type:"PENGHARGAAN"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_appreciation
    @appreciation = @lecture.lecture_appreciations.find(params[:id])
  end

  def appreciation_params
    params.require(:lecture_appreciation).permit(
      :lecture_id,
      :year,
      :appreciation,
      :presented_by
    )
  end
end
