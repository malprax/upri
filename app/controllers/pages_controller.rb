class PagesController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  before_action :go_to_dashboards
  def index
      @campus_newspapers = CampusNewspaper.where(terbit: true).order("created_at desc" )
  end

  def daftar
  end

  def info
    #code
  end

  def kalendar_akademik
    #code
  end

  private
  def go_to_dashboards
    redirect_to send("#{user_base_url.pluralize}_dashboards_path") if current_user
  end
end
