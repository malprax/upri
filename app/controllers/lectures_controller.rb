class LecturesController < ApplicationController
  include LectureController
  def index
    @lectures = Lecture.includes(:lecture_profile).all
    respond_to do |format|
      format.html
    end
    # @lectures = @lectures.includes(lecture_profile: [major: [:faculty]]).where(faculty_id: 3)
  end

  def edit_formulir
    @lecture = Lecture.find(params[:id])
  end

  def edit
    if params[:type]=="IDENTITAS DIRI"
      @lecture.build_lecture_profile unless @lecture.lecture_profile.present?
      @lecture.addresses.build unless @lecture.addresses.present?
    end
    if params[:type]=="RIWAYAT PENDIDIKAN"
      @lecture.lecture_education_histories.build unless @lecture.lecture_education_histories.present?
    end
    if params[:type]=="PELATIHAN"
      @lecture.professional_trainings.build unless @lecture.professional_trainings.present?
      # @lecture.training_professional.build unless @lecture.training_professional.present?
    end
    if params[:type]=="MENGAJAR"
      @lecture.lecture_teaching_histories.build unless @lecture.lecture_teaching_histories.present?
    end
    if params[:type]=="BAHAN AJAR"
      @lecture.lecture_teaching_materials.build unless @lecture.lecture_teaching_materials.present?
    end
    if params[:type]=="PENELITIAN"
      @lecture.lecture_researches.build unless @lecture.lecture_researches.present?
    end
    if params[:type]=="KARYA ILMIAH"
      @lecture.lecture_scientific_works.build unless @lecture.lecture_scientific_works.present?
    end
    if params[:type]=="MAKALAH"
      @lecture.lecture_papers.build unless @lecture.lecture_papers.present?
    end
    if params[:type]=="PENYUNTING"
      @lecture.lecture_reviewers.build unless @lecture.lecture_reviewers.present?
    end
    if params[:type]=="KONFERENSI"
      @lecture.lecture_conferences.build unless @lecture.lecture_conferences.present?
    end
    if params[:type]=="PENGABDIAN MASYARAKAT"
      @lecture.lecture_professional_activities.build unless @lecture.lecture_professional_activities.present?
    end
    if params[:type]=="JABATAN INSTITUSI"
      @lecture.lecture_position_histories.build unless @lecture.lecture_position_histories.present?
    end
    if params[:type]=="KEGIATAN KEMAHASISWAAN"
      @lecture.lecture_student_organization_histories.build unless @lecture.lecture_student_organization_histories.present?
    end
    if params[:type]=="PENGHARGAAN"
      @lecture.lecture_appreciations.build unless @lecture.lecture_appreciations.present?
    end
    if params[:type]=="ORGANISASI PROFESI"
      @lecture.lecture_scientist_organizations.build unless @lecture.lecture_scientist_organizations.present?
    end
    respond_to do |format|
      format.html
    end
  end

  def update
    respond_to do |format|
      if @lecture.update(lecture_params)
        format.html{redirect_to lecture_path(@lecture), notice: "Data Berhasil Diperbarui"}
      else
        # flash[:alert] = "Gagal simpan"
        # render :edit
        format.html{redirect_to lecture_path(@lecture), alert: "Data Gagal Diperbarui"}
      end
    end
  end

  def show
    @profile = @lecture.lecture_profile
    @addresses = @lecture.addresses
    @educations = @lecture.lecture_education_histories
    @training_professionals = @lecture.professional_trainings
    @teachings = @lecture.lecture_teaching_histories
    @materials = @lecture.lecture_teaching_materials
    @researchs = @lecture.lecture_researches
    @scientifics = @lecture.lecture_scientific_works
    @papers = @lecture.lecture_papers
    @reviewers = @lecture.lecture_reviewers
    @conferences = @lecture.lecture_conferences
    @positions = @lecture.lecture_position_histories
    @professionals = @lecture.lecture_professional_activities
    @reviewers = @lecture.lecture_reviewers
    @student_organizations = @lecture.lecture_student_organization_histories
    @appreciations = @lecture.lecture_appreciations
    @organizations = @lecture.lecture_scientist_organizations
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Curriculum Vitae #{@lecture.lecture_profile.try(:nama).titleize}",
        template: "lectures/pdf.html.erb",
        layout: 'pdf.html',
        locals: {:lecture => @lecture},
        page_size: 'A4'
      end
    end
  end

  def destroy
    respond_to do |format|
      @lecture.destroy
      format.html{ redirect_to lectures_path, notice: "Data berhasil dihapus"}
    end
  end

  def print
    @lecture = Lecture.find(params[:id])
    @profile = @lecture.lecture_profile
    @educations = @lecture.lecture_education_histories
    @training_professionals = @lecture.professional_trainings
    @teachings = @lecture.lecture_teaching_histories
    @materials = @lecture.lecture_teaching_materials
    @researchs = @lecture.lecture_researches
    @scientifics = @lecture.lecture_scientific_works
    @papers = @lecture.lecture_papers
    @reviewers = @lecture.lecture_reviewers
    @conferences = @lecture.lecture_conferences
    @positions = @lecture.lecture_position_histories
    @professionals = @lecture.lecture_professional_activities
    @reviewers = @lecture.lecture_reviewers
    @student_organizations = @lecture.lecture_student_organization_histories
    @appreciations = @lecture.lecture_appreciations
    @organizations = @lecture.lecture_scientist_organizations
    respond_to do |format|
      format.html
      format.pdf do
        #{@lecture.lecture_profile.try(:nama).titleize}
        render pdf: "Curriculum Vitae ",
        template: "lectures/print.html.erb",
        layout: 'pdf.html',
        locals: {:lecture => @lecture},
        page_size: 'A4'
      end
    end
  end

  def profile
    @lecture = Lecture.find(params[:id])
    @profile = @lecture.lecture_profile
    @addresses = @lecture.addresses
    respond_to do |format|
      format.js
    end
  end

  def educations
    @lecture = Lecture.find(params[:id])
    @educations = @lecture.lecture_education_histories
      respond_to do |format|
        format.js
      end
  end

  def training_professionals
    @lecture = Lecture.find(params[:id])
    @training_professionals = @lecture.professional_trainings
    respond_to do |format|
      format.js
    end
  end

  def teachings
    @lecture = Lecture.find(params[:id])
    @teachings = @lecture.lecture_teaching_histories
    respond_to do |format|
      format.js
    end
  end

  def materials
    @lecture = Lecture.find(params[:id])
    @materials = @lecture.lecture_teaching_materials
    respond_to do |format|
      format.js
    end
  end

  def researchs
    @lecture = Lecture.find(params[:id])
    @researchs = @lecture.lecture_researches
    respond_to do |format|
      format.js
    end
  end

  def scientifics
    @lecture = Lecture.find(params[:id])
    @scientifics = @lecture.lecture_scientific_works
    respond_to do |format|
      format.js
    end
  end

  def papers
    @lecture = Lecture.find(params[:id])
    @papers = @lecture.lecture_papers
    respond_to do |format|
      format.js
    end
  end

  def reviewers
    @lecture = Lecture.find(params[:id])
    @reviewers = @lecture.lecture_reviewers
    respond_to do |format|
      format.js
    end
  end

  def conferences
    @lecture = Lecture.find(params[:id])
    @conferences = @lecture.lecture_conferences
    respond_to do |format|
      format.js
    end
  end

  def professionals
    @lecture = Lecture.find(params[:id])
    @professionals = @lecture.lecture_professional_activities
    respond_to do |format|
      format.js
    end
  end

  def positions
    @lecture = Lecture.find(params[:id])
    @positions = @lecture.lecture_position_histories
    respond_to do |format|
      format.js
    end
  end

  def student_organizations
    @lecture = Lecture.find(params[:id])
    @student_organizations = @lecture.lecture_student_organization_histories
    respond_to do |format|
      format.js
    end
  end

  def appreciations
    @lecture = Lecture.find(params[:id])
    @appreciations = @lecture.lecture_appreciations
    respond_to do |format|
      format.js
    end
  end

  def scientific_organizations
    @lecture = Lecture.find(params[:id])
    @scientific_organizations = @lecture.lecture_scientist_organizations
    respond_to do |format|
      format.js
    end
  end

  def make
    @lecture = Lecture.find(params[:id])
    respond_to do |format|
      @lecture.make!
      format.html{redirect_to daftar_calon_mahasiswa_path, notice: "Berhasil Jadi Mahasiswa"}
    end
  end

  def studyplans
    @lecture = current_user
    @subjects = Subject.all
    # @subject_ids = params[:subject_ids].require('subject_ids')
    @subject_ids = Subject.where(params[:subject_id])
    @lectures = Lecture.all
    @subject_ids.each do |subject_id|
      @study_plan = StudyPlan.new(study_plans_params)
      @study_plan.subject_id = subject_id
      if @study_plan.save
        flash[:success] = "Rencana studi berhasil dibuat"
      else
        @study_plan.errors
        return
      end
    end
    redirect_to root_path
  end
end
