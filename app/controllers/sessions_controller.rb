class SessionsController < ApplicationController
  def new
  end

  def create
    if current_user.is_graduate?
      authorized_graduate = Graduate.authenticate_graduate_token(params[:stambuk], params[:access_token])
      if authorized_graduate
        session[:user_id] = authorized_graduate.id
        flash[:notice] = "#{authorized_graduate.email}, Stambuk atau Token Yang Anda Masukkan Benar "
        redirect_to formulir_wisuda_graduate_path(authorized_graduate.id)
      else
        flash[:danger] = "Maaf Stambuk Atau Token Yang Anda Masukkan Salah"
        redirect_to token_session_path
      end
    elsif current_user.is_staff?
      @staff = Staff.find(current_user.id)
      logger.debug "hit----------------------------------------------------------------------------"
      logger.debug "#{@staff}"
      authorized_staff = Staff.authenticate_staff_token(@staff, params[:access_token])
      if authorized_staff
        session[:user_id] = authorized_staff.id
        flash[:notice] = "Token Yang Anda Masukkan Benar "
        redirect_to subject_plans_path
      else
        flash[:alert] = "Token Yang Anda Masukkan Salah"
        redirect_to token_session_path
      end
    elsif current_user.is_lecture?
      authorized_lecture = Lecture.authenticate_lecture_token(params[:email], params[:access_token])
      if authorized_lecture
        session[:user_id] = authorized_lecture.id
        flash[:notice] = "#{authorized_lecture.email}, Email atau Token Yang Anda Masukkan Benar "
        redirect_to new_academic_year_path
      else
        flash[:alert] = "Maaf Email Atau Token Yang Anda Masukkan Salah"
        redirect_to token_session_path
      end
    elsif current_user.is_student?
      @student = Student.find(current_user.id)
      @subject_plan = SubjectPlan.find(params[:subject_plan_id_input])
      authorized_student = Student.authenticate_student_token(@student, params[:access_token])
      if authorized_student
        session[:user_id] = authorized_student.id
        redirect_to print_subject_plan_path(@subject_plan, format: :pdf)
      else
        flash[:alert] = "Maaf Email Atau Token Yang Anda Masukkan Salah"
        redirect_to token_session_path
      end
    end
  end

  def destroy_token
    session[:user_id] = nil
  end
end
