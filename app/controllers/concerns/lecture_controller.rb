module LectureController
  extend ActiveSupport::Concern
  # code
  included  do
    before_action :authenticate_user!
    before_action :set_lecture, only: [:show, :print, :edit, :update, :destroy]
  end

  private
  def study_plans_params
    params.require(:study_plan).permit(:lecture_id, :academic_year_id, subject_id:[])
  end

  def set_lecture
    @lecture = Lecture.find(params[:id])
  end

  def lecture_params
    params.require(:lecture).permit(
      :religion_id,
      :major_id,
      :lecture_functional_type_id,
      lecture_profile_attributes:[
         :id,
         :nama,
         :gelar_depan_nama,
         :gelar_belakang_nama,
         :nomor_induk_pegawai,
         :nomor_induk_dosen_negara,
         :nomor_induk_kependudukan,
         :handphone,
         :tempat_lahir,
         :tanggal_lahir,
         :jenis_kelamin,
         :marital_status,
         :lecture_type,
         :is_active,
         :_destroy
      ],
      addresses_attributes:[
        :id,
        :street,
        :rt,
        :rw,
        :kelurahan,
        :kecamatan,
        :city,
        :province,
        :post_code,
        :_destroy
      ],
      lecture_education_histories_attributes:[
        :id,
        :level,
        :major,
        :institute,
        :graduation_year,
        :_destroy
      ],

      lecture_teaching_histories_attributes:[
        :id,
        :subject,
        :major,
        :institute,
        :semester_status,
        :academic_year,
        :_destroy
      ],
      lecture_teaching_materials_attributes:[
        :id,
        :subject,
        :major,
        :kind,
        :semester_status,
        :academic_year,
        :_destroy
      ],
      lecture_researches_attributes:[
        :id,
        :year,
        :research,
        :role,
        :source_funds,
        :_destroy
      ],

      lecture_scientific_works_attributes:[
        :id,
        :year,
        :science_product,
        :publisher,
        :_destroy
      ],
      lecture_papers_attributes:[
        :id,
        :year,
        :paper,
        :presented_by,
        :_destroy
      ],
      lecture_reviewers_attributes:[
        :id,
        :year,
        :review,
        :publisher,
        :_destroy
      ],
      lecture_conferences_attributes:[
        :id,
        :year,
        :conference,
        :presented_by,
        :role,
        :_destroy
      ],
      lecture_professional_activities_attributes:[
        :id,
        :year,
        :activity,
        :place,
        :_destroy
      ],
      lecture_position_histories_attributes:[
        :id,
        :position,
        :institute,
        :start_at,
        :end_at,
        :_destroy
      ],
      lecture_student_organization_histories_attributes:[
        :id,
        :name,
        :year,
        :role,
        :place,
        :_destroy
      ],
      lecture_appreciations_attributes:[
        :id,
        :year,
        :appreciation,
        :presented_by,
        :_destroy
      ],
      lecture_scientist_organizations_attributes:[
        :id,
        :year,
        :organization,
        :position,
        :_destroy
      ],
      professional_trainings_attributes:[
        :id,
        :year,
        :training,
        :presented_by,
        :terms,
        :training_professional_id,
        :training_professional_type,
        :_destroy
      ],
      subject_ids:[]
    )
  end
end
