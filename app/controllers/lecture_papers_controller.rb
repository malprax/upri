class LecturePapersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_paper, only:[:edit, :update, :destroy]
  def new
    @paper = @lecture.lecture_papers.build
  end

  def create
    @paper = @lecture.lecture_papers.build(paper_params)
    if @paper.save
       redirect_to lecture_path(@lecture, type:"MAKALAH"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @paper.update(paper_params)
      redirect_to lecture_path(@lecture, type:"MAKALAH"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @paper.destroy
    redirect_to lecture_path(@lecture, type:"MAKALAH"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_paper
    @paper = @lecture.lecture_papers.find(params[:id])
  end

  def paper_params
    params.require(:lecture_paper).permit(
      :lecture_id,
      :year,
      :paper,
      :presented_by
    )
  end
end
