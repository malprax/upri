class ProfessionalTrainingsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_professional_training, only:[:edit, :update, :destroy]
  def new
    @professional_training = @lecture.professional_trainings.build
  end

  def create
    @professional_training = @lecture.professional_trainings.build(professional_training_params)
    if @professional_training.save
       redirect_to lecture_path(@lecture, type:"PELATIHAN"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @professional_training.update(professional_training_params)
      redirect_to lecture_path(@lecture, type:"PELATIHAN"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @professional_training.destroy
    redirect_to lecture_path(@lecture, type:"PELATIHAN"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_professional_training
    @professional_training = @lecture.professional_trainings.find(params[:id])
  end

  def professional_training_params
    params.require(:professional_training).permit(
      :lecture_id,
      :year,
      :training,
      :presented_by,
      :terms,
      :training_professional_id,
      :training_professional_type,
    )
  end
end
