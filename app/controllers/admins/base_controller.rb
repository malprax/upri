class Admins::BaseController < ApplicationController
  before_action :authenticate_user!
  before_action :checking_admin!
  #
  private
  def checking_admin!
    redirect_to send("#{user_base_url.pluralize}_dashboards_path"), flash: { error: 'Anda tidak memiliki akses ke halaman ini' } if !current_user.is_admin?
  end
end
