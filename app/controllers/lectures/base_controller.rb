class Lectures::BaseController < ApplicationController
  before_action :authenticate_user!
  before_action :checking_lecture!

  private

  def checking_lecture!
    redirect_to send("#{user_base_url.pluralize}_dashboards_path"), flash: { error: 'Anda tidak memiliki akses ke halaman ini' } if !current_user.is_lecture?
  end
end
