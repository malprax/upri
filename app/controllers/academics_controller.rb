class AcademicsController < ApplicationController
  before_action :authenticate_user!
  layout 'academics_layout'
  def tahun_akademik
    @academic_years = AcademicYear.sortir_dari_tahun_terakhir_dan_aktifnya
    @academic_year = AcademicYear.new
  end
end
