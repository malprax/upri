class LecturePositionHistoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_position, only:[:edit, :update, :destroy]
  def new
    @posisi = @lecture.lecture_position_histories.build
  end

  def create
    @posisi = @lecture.lecture_position_histories.build(position_params)
    if @posisi.save
       redirect_to lecture_path(@lecture, type:"JABATAN INSTITUSI"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @posisi.update(position_params)
      redirect_to lecture_path(@lecture, type:"JABATAN INSTITUSI"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @posisi.destroy
    redirect_to lecture_path(@lecture, type:"JABATAN INSTITUSI"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_position
    @posisi = @lecture.lecture_position_histories.find(params[:id])
  end

  def position_params
    params.require(:lecture_position_history).permit(
      :lecture_id,
      :position,
      :institute,
      :start_at,
      :end_at
    )
  end
end
