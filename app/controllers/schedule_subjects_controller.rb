class ScheduleSubjectsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_schedule_subject, only: [:edit, :update, :destroy]

  def jadwal_kuliah
    @semua_jadwalkuliah_tambang = ScheduleSubject.where(major_id: 31)
    @semua_jadwalkuliah_mesin = ScheduleSubject.where(major_id: 32)
    @semua_jadwalkuliah_informatika = ScheduleSubject.where(major_id: 33)
  end
  
  def new
    @schedule_subject = ScheduleSubject.new
  end

  def create
    @schedule_subject = ScheduleSubject.new(schedule_params)
    respond_to do |format|
      if @schedule_subject.save
        format.html{redirect_to jadwal_kuliah_path, notice: "Data Jadwal Kuliah Berhasil Dibuat" }
        format.json { render json: @schedule_subject }
      else
        format.html{redirect_to jadwal_kuliah_path, notice: "Data Jadwal Kuliah Gagal Dibuat" }
      end
    end
  end

  def edit
    @majors = Major.where(faculty_id: 3)
  end

  def update
    respond_to do |format|
      if @schedule_subject.update(schedule_params)
        format.html{redirect_to jadwal_kuliah_path, notice: "Data Jadwal Kuliah Berhasil Diperbarui" }
        format.json { render json: @schedule_subject }
      else
        format.html{redirect_to jadwal_kuliah_path, notice: "Data Jadwal Kuliah Gagal Diperbarui" }
      end
    end
  end

  def show
    #code
  end

  def destroy
    respond_to do |format|
      @schedule_subject.delete
      format.html{redirect_to jadwal_kuliah_path, notice: "Data Jadwal Kuliah Berhasil Dihapus" }
      format.json { render json: @schedule_subject }
    end
  end

  private
  def set_schedule_subject
    @schedule_subject = ScheduleSubject.find(params[:id])
  end

  def schedule_params
    params.require(:schedule_subject).permit(:hari, :jam, :ruang, :subject_id, :major_id, :dummy_lecture_1_id, :dummy_lecture_2_id )
  end
end
