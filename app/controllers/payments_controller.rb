class PaymentsController < ApplicationController
  before_action :authenticate_user!
  # before_action :check_profile_present!
  before_action :set_student, except:[:index, :show, :edit, :approve]
  before_action :set_payment, only: [:print, :update, :destroy] #, except:[:print_registrasi]
  before_action :check_if_approved, only: [:edit, :destroy]
  helper_method :sort_column, :sort_direction

  def check_if_approved
    if current_user.is_student?
      @student = current_user
      @payment = @student.payments.find(params[:id])
      redirect_to student_payments_path(@student), alert:"Maaf anda tidak dapat mengedit atau menghapus blanko pembayaran yang sudah di periksa bagian keuangan" if @payment.is_approved?
    end
  end

  def sort_column
    params[:sort] || 'created_at'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def check_if_approve
    # code
  end

  # def send_payment_token_email
  #   respond_to do |format|
  #     GraduateMailer.token_email(@student).deliver_later
  #     @student.regenerate_access_token
  #     format.html{redirect_to token_path, notice:"Token Berhasil Di Email" }
  #   end
  # end

  def approve
    respond_to do |format|
      @staff = current_user.email
      @payment = Payment.find(params[:id])
      @student = @payment.student
      if @payment.is_approved == false
        @payment.update(is_approved: true)
        PaymentMailer.approve_payment(@student, @staff, @payment).deliver_now
        format.html{redirect_to payments_path, notice: "Pembayaran berhasil di approve dan dikirim ke email mahasiswa, anda, dan wadek bidang keuangan"}
      elsif @payment.is_approved == true
        @payment.update(is_approved: false)
        format.html{redirect_to payments_path, alert: "Pembayaran gagal di approve"}
      elsif @payment.is_approved == nil
        @payment.update(is_approved: true)
        PaymentMailer.approve_payment(@student, @staff, @payment).deliver_now
        format.html{redirect_to payments_path, notice: "Pembayaran berhasil di approve dan dikirim ke email mahasiswa, anda, dan wadek bidang keuangan"}
      end
    end
  end

  def letter
    respond_to do |format|
      @payment = Payment.find(params[:id])
      @student = @payment.student
      format.html
      format.pdf do
        render pdf: "Surat Penangguhan BPP",
        template: "payments/letter.html.erb",
        layout: 'pdf.html',
        margin:  {top:10,left: 15},
        zoom: 0.78125,
        locals: {:payment => @payment},
        page_size: 'A4'
      end
    end
  end

  # GET /payments
  # GET /payments.json
  def index
    if current_user.is_student?
      @student = Student.find(params[:student_id])
      @payments = @student.payments.includes(:academic_year, :student, :bank)
    elsif current_user.is_staff?
      @payments = Payment.includes(:academic_year, :bank, student: [:student_profile, :major]).order(sort_column + " " + sort_direction).page(params[:page]).per(8)
      # order("kind desc").order("academic_years.name asc").order("academic_years.semester asc")
      @payments = @payments.search(params[:search]) if params[:search].present?
    elsif current_user.is_lecture?
      @payments = Payment.includes(:academic_year, :bank, student: [:student_profile, :major]).order(sort_column + " " + sort_direction).page(params[:page]).per(8)
      # order("kind desc").order("academic_years.name asc").order("academic_years.semester asc")
      @payments = @payments.search(params[:search]) if params[:search].present?
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
    if current_user.is_student?
      @student = Student.find(params[:student_id])
      @payment = @student.payments.find(params[:id])
    elsif current_user.is_staff?
      @payment = Payment.find(params[:id])
      @student = @payment.student
    elsif current_user.is_lecture?
      @payment = Payment.find(params[:id])
      @student = @payment.student
    end
    respond_to do |format|
      format.html
    end
  end

  def print_registrasi
    # @payment = @student.payments.find_by_student_id(params[:student_id])
    @payment = @student.payments.find(params[:id])
    # @payment.kind.upcase
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Blanko Pembayaran #{@payment.student.student_profile.nama}",
        template: "payments/print_registrasi.html.erb",
        layout: 'pdf.html',
        margin:  {top:10,left: 15},
        zoom: 0.78125,
        locals: {:payment => @payment},
        page_size: 'A4'
      end
    end
  end

  def print
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Blanko Pembayaran",
        template: "payments/print.html.erb",
        layout: 'pdf.html',
        orientation: 'Landscape',
        margin:  {top:10,left: 15},
        zoom: 0.78125,
        locals: {:payment => @payment},
        page_size: 'A4'
      end
    end
  end

  # GET /payments/new
  def new
    @student = Student.find(params[:student_id])
    @payment = @student.payments.build
  end

  # GET /payments/1/edit
  def edit
    if current_user.is_student?
      @student = Student.find(params[:student_id])
      @payment = @student.payments.find(params[:id])

    elsif current_user.is_staff?
      @payment = Payment.find(params[:id])
      @student = @payment.student
    end
  end

  # POST /payments
  # POST /payments.json
  def create
    @payment = @student.payments.build(payment_params)
    respond_to do |format|
      if @payment.save
        format.html { redirect_to student_payments_path , notice: 'Data berhasil disimpan' }
      else
        format.html { redirect_to student_payments_path , notice: 'Data gagal disimpan' }
      end
    end
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    respond_to do |format|
      if @payment.update(payment_params)
        if current_user.is_student?
            format.html { redirect_to student_payments_path, notice: 'Data berhasil diperbarui' }
        elsif current_user.is_staff?
            format.html { redirect_to payments_path, notice: 'Data berhasil diperbarui' }
        end
      else
        if current_user.is_student?
            format.html { redirect_to student_payments_path , notice: '"Data gagal diperbarui"' }
        elsif current_user.is_staff?
            format.html { redirect_to payments_path , notice: '"Data gagal diperbarui"' }
        end
      end
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    respond_to do |format|
      if current_user.is_student?
        @payment.destroy
        format.html { redirect_to student_payments_path, notice: 'Data berhasil dihapus' }
      elsif current_user.is_staff?
        @payment.destroy
        format.html { redirect_to payments_path, notice: 'Data berhasil dihapus' }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:student_id])
    end

    def set_payment
      @payment = @student.payments.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def payment_params
      params.require(:payment).permit(:kind, :academic_year_id, :in_number, :in_word, :student_id, :bank_id, :is_approve, :payment_file, :paid, :approval_id, :type_student)
    end
end
