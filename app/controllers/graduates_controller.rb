class GraduatesController < ApplicationController
  before_action :authenticate_user!, except:[:wisudawan, :show]
  # before_action :authenticate_phone_number
  # before_action :authenticate_graduate, only: [:formulir_wisuda] #unless user_signed_in && current_user_staff
  before_action :disable_nav, only: [:galeri]
  before_action :set_graduate, only: [:edit, :update, :show, :send_token_email, :destroy, :download ]
  before_action :set_graduate_galeri, only: [:galeri]
  before_action :set_graduates_search_params_1, only: [:wisudawan]
  before_action :set_graduates_search_params_2, only: [:token]

  def edit
    @faculties = Faculty.order("id asc").where(id: 3)
    @majors = Major.order("id asc").where(faculty_id: 3)
    @graduate.build_graduate_profile unless @graduate.graduate_profile.present?
    @graduate.build_essay unless @graduate.essay.present?
    @graduate.build_address unless @graduate.address.present?
    @graduate.identities.build unless @graduate.identities.present?
    @identity = @graduate.identities
  end

  def update
    respond_to do |format|
      if @graduate.update!(graduate_params)
        format.html{redirect_to edit_graduate_path(@graduate), notice: "Data Wisudawan Berhasil Dibuat"}
      else
        format.html{redirect_to edit_graduate_path(@graduate), alert: "Data Wisudawan Gagal Dibuat"}
      end
    end
  end

  def show
    session[:return_to] ||= params[:page] #request.referer
  end

  def update_access_token
    @graduate.regenerate_access_token
    if @graduate.save
      redirect_to token_path, notice: "Token Berhasil Diperbaharui"
    else
      redirect_to token_path, alert: "Token Gagal Diperbaharui"
    end
  end

  def send_token_email
    respond_to do |format|
      GraduateMailer.token_email(@graduate).deliver_later
      format.html{redirect_to token_path, notice:"Token Berhasil Di Email" }
    end
  end

  def update_all
    @graduates = Graduate.find(params[:user_ids])
    @graduates.each do |graduate|
      graduate.regenerate_access_token
    end
    redirect_to token_path, notice: "Semua Token Berhasil Diperbaharui"
  end

  def destroy
    @graduate.destroy
    respond_to do |format|
      format.html {redirect_to token_path, notice: "Data Wisudawan Berhasil Dihapus"}
      format.json { head :no_content }
    end
  end

  def token
  end

  def wisudawan
    session[:return_to] ||= params[:page] #request.referer
    respond_to do |format|
      format.html
      format.csv { send_data @graduates.to_csv }
      format.xlsx
      format.pdf do
         render pdf: "mahasiswa FKM",
         template: "graduates/wisudawan.html.erb",
         layout: 'pdf.html.erb',
         :page_size => 'legal',
         locals: {:graduates => @graduates}
      end
    end
  end

  def buku
    if params[:limit].present?
      @graduates = Graduate.order(:major_id).includes(:graduate_profile, :essay).page(params[:page]).per(params[:limit])
    else
      @graduates = Graduate.order(:major_id).includes(:graduate_profile, :essay).page(params[:page]).per(10)
    end
    @graduates = @graduates.fakultas_wisudawan(params[:fakultas_wisudawan]) if params[:fakultas_wisudawan].present?
    @graduates = @graduates.program_studi_wisudawan(params[:program_studi_wisudawan]) if params[:program_studi_wisudawan].present?
    respond_to do |format|
      format.html
      format.csv { send_data @graduates.to_csv }
      format.xlsx
      format.pdf do
         render pdf: "Wisudawan",
         template: "graduates/table/_buku_table.html.erb",
         layout: 'pdf.html.erb',
         :cell_style => { size: 7 },
         locals: {:graduates => @graduates}
      end
    end
  end

  def semua
    @graduates = Graduate.where(faculty_id: 6).includes(:graduate_profile, :essay).page(params[:page]).per(1250)
    respond_to do |format|
      format.html
      format.csv { send_data @graduates.to_csv}
      format.pdf do
         render pdf: "mahasiswa FKM",
         template: "graduates/table/_mahasiswa_table.html.erb",
         layout: 'pdf.html.erb',
         page_size:'legal',
         orientation: 'Landscape',                  # default Portrait
         locals: {:graduates => @graduates}
      end
    end
  end

  def download
    graduate = @graduate
    data = open(graduate.image_url)
    send_data data.read, type: data.content_type, :x_sendfile => true
  end

  def galeri
  end

  private
  def set_graduate
    @graduate = Graduate.find(params[:id])
  end

  def set_graduates_search_params_1
    if params[:limit]
      @graduates = Graduate.order(:major_id).includes(:faculty, :major, :dummy_lecture_1, :dummy_lecture_2).page(params[:page]).per(params[:limit])
    else
      @graduates = Graduate.order(:major_id).includes(:faculty, :major, :dummy_lecture_1, :dummy_lecture_2).page(params[:page]).per(10)
      @graduates = @graduates.fakultas_wisudawan(params[:fakultas_wisudawan]) if params[:fakultas_wisudawan].present?
      @graduates = @graduates.program_studi_wisudawan(params[:program_studi_wisudawan]) if params[:program_studi_wisudawan].present?
      @graduates = @graduates.nama_email_stambuk(params[:nama_email_stambuk]) if params[:nama_email_stambuk].present?
    end
  end

  def set_graduates_search_params_2
    @graduates = Graduate.order(:major_id).order("sortlist asc").includes(:faculty, :major, :dummy_lecture_1, :dummy_lecture_2).page(params[:page]).per(10)
    @graduates = @graduates.nama_email_stambuk(params[:nama_email_stambuk]) if params[:nama_email_stambuk].present?
    # fresh_when last_modified: @graduates.maximum(:updated_at)
  end

  def set_graduate_galeri
    @graduates = Graduate.order(:major_id).order("sortlist asc").includes(:faculty, :major, :dummy_lecture_1, :dummy_lecture_2).page(params[:page]).per(1)
  end

  def graduate_params
    params.require(:graduate).permit(
      :sortlist,
      :name,
      :stambuk,
      :image,
      :birth_date,
      :birth_place,
      :religion,
      :faculty_id,
      :major_id,
      :dummy_lecture_1_id,
      :dummy_lecture_2_id,
      :blangko,
      address_attributes: [:id, :street, :phone, :_destroy],
      graduate_profile_attributes: [:id, :ipk, :yudisium_date, :yudisium_letter, :predicet_of_yudisium, :alumni_number, :_destroy],
      essay_attributes: [:id, :title, :_destroy],
      identities_attributes: [:id, :kategori, :file_identity, :user_type, :_destroy],
      payment_forms_attributes: [:id, :kategori, :file_payment, :user_type, :_destroy],
      document_attributes: [:id, :kategori, :file_document, :user_type, :_destroy]
    )
  end
end
