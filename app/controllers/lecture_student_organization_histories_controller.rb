class LectureStudentOrganizationHistoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_student_organization, only:[:edit, :update, :destroy]
  def new
    @student_organization = @lecture.lecture_student_organization_histories.build
  end

  def create
    @student_organization = @lecture.lecture_student_organization_histories.build(student_organization_params)
    if @student_organization.save
       redirect_to lecture_path(@lecture, type:"KEGIATAN KEMAHASISWAAN"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @student_organization.update(student_organization_params)
      redirect_to lecture_path(@lecture, type:"KEGIATAN KEMAHASISWAAN"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @student_organization.destroy
    redirect_to lecture_path(@lecture, type:"KEGIATAN KEMAHASISWAAN"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_student_organization
    @student_organization = @lecture.lecture_student_organization_histories.find(params[:id])
  end

  def student_organization_params
    params.require(:lecture_student_organization_history).permit(
      :lecture_id,
      :name,
      :year,
      :role,
      :place
    )
  end
end
