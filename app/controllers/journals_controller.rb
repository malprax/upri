class JournalsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :authenticate_user_active, except: [:index, :show]
  before_action :set_journal, only:[:show, :edit, :update, :destroy, :delete_journal_file_attachment]
  def index
    @journals = Journal.includes(:articles).where(published:true).order(:number)
  end

  def new
    # code
    @journal = Journal.new
  end

  def create
    # code
    respond_to do |format|
      @journal = Journal.create(journal_params)
      if @journal.save
        format.html{redirect_to journals_path, notice: "Data berhasil dibuat"}
      else
        flash[:alert] = "Data gagal dibuat"
        render :new
      end

    end

  end

  def show
    # code
    respond_to do |format|
      format.html
      format.js
    end
  end

  def edit
    # code
  end

  def update
    # code
    respond_to do |format|
      if @journal.update(journal_params)
        format.html{redirect_to journals_path, notice: "Data berhasil diupdate"}
      else
        flash[:alert]="Data gagal diperbarui"
        render :edit
      end
    end
  end

  def delete_journal_file_attachment
    @journal.journal_files.find_by_id(params[:journal_files_id]).purge
    redirect_back(fallback_location: request.referer)
  end

  def destroy
    @journal.delete
    flash[:notice] = "Data berhasil dihapus"
    redirect_to journals_path
  end

  private
  def set_journal
    @journal = Journal.find(params[:id])
  end
  def journal_params
    params.require(:journal).permit(:volume, :number, :published, :issn, :description, journal_files:[])
  end
end
