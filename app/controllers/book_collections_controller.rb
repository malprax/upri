class BookCollectionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_book, only: [:edit, :update, :destroy]
  def index_buku
    @book_collections = BookCollection.order(:posisi).page(params[:page]).per(10)
  end

  def index
  end

  def new
    @book_collection = BookCollection.new
  end

  def create
    @book_collection = BookCollection.new(book_params)
    respond_to do |format|
      if @book_collection.save
        format.html{redirect_to index_buku_path, notice: "Data Mata Kuliah Berhasil Dibuat" }

      else
        format.html{redirect_to index_buku_path, notice: "Data Mata Kuliah Gagal Dibuat" }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @book_collection.update(book_params)
        format.html{redirect_to index_buku_path, notice: "Data Mata Kuliah Berhasil Dibuat" }
        format.json { render json: @book_collection }
      else
        format.html{redirect_to index_buku_path, notice: "Data Mata Kuliah Gagal Dibuat" }
      end
    end
  end

  def destroy
    @book_collection.destroy
    redirect_to index_buku_path
  end

  private
  def set_book
    @book_collection = BookCollection.find(params[:id])
  end

  def book_params
    params.require(:book_collection).permit!
  end
end
