class EssaysController < ApplicationController
  before_action :authenticate_user!
  def index_skripsi
    # @essays = Essay.includes(:graduate => [:major]).page(params[:page]).per(10)
      @essays = Essay.includes(:graduate => [:major]).page(params[:page]).per(10)
  end

  def edit
    @essay = Essay.find(params[:id])
  end

  def update
    @essay = Essay.where(:faculty_id => 3)
    respond_to do |format|
      if @essay.update(skripsi_params)
        format.html{redirect_to index_skripsi_path, notice: "Data Skripsi Berhasil Dibuat" }
        format.json { render json: @essay }
      else
        format.html{redirect_to index_skripsi_path, notice: "Data Skripsi Gagal Dibuat" }
      end
    end
  end

  def show
    @essay = Essay.find(params[:id])
  end

  private
  def skripsi_params
    params.require(:essay).permit(
       :kode,
       :posisi,
       :cover_skripsi,
       essay_documents_attributes:[:id, :kategori, :document_upload, :_destroy]
    )
  end
end
