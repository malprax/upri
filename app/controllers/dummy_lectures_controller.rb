class DummyLecturesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_dummy_lecture, only:[:edit, :update, :show, :destroy]

  def index_dosen
    @semua_dosen_tambang = DummyLecture.where(major_id: 31)
    @semua_dosen_mesin = DummyLecture.where(major_id: 32).order(:status)
    @semua_dosen_informatika = DummyLecture.where(major_id: 33).order(:status)
    @semua_dosen = DummyLecture.semua_kecuali([31,32,33]).order(:status)
  end

  def new
    @faculties = Faculty.order(:id)
    @majors = Major.order(:faculty_id)
    @dummylecture = DummyLecture.new
  end

  def create
    @dummylecture = DummyLecture.new(lecture_params)
    respond_to do |format|
      if @dummylecture.save
        format.html{redirect_to index_dosen_path, notice: "Data Dosen Berhasil Dibuat" }
        format.json { render json: @dummylecture }
      else
        format.html{redirect_to index_dosen_path, notice: "Data Dosen Gagal Dibuat" }
      end
    end
  end

  def edit
    @majors = Major.order(:faculty_id)
  end

  def update
    respond_to do |format|
      if @dummylecture.update(lecture_params)
        format.html{redirect_to index_dosen_path, notice: "Data Dosen Berhasil Dibuat" }
        format.json { render json: @dummylecture }
      else
        format.html{redirect_to index_dosen_path, notice: "Data Dosen Gagal Dibuat" }
      end
    end
  end

  def show
    #code
  end

  def destroy
    respond_to do |format|
      @dummylecture.delete
      format.html{redirect_to index_dosen_path, notice: "Data Dosen Berhasil Dihapus" }
      format.json { render json: @dummylecture }
    end
  end

  private
  def set_dummy_lecture
    @dummylecture = DummyLecture.find(params[:id])
  end

  def lecture_params
    params.require(:dummy_lecture).permit(:nama, :nidn, :gelar_depan_nama, :gelar_belakang_nama, :status, :major_id)
  end
end
