class AuthenticatePhonesController < ApplicationController
  before_action :authenticate_user!
  def new
    #code
  end

  def create
    @response = Authy::PhoneVerification.start(
      via: "sms",
      country_code: "+62",
      phone_number: params[:phone_number]
    )

    if @response.ok?
      session[:phone_number] = params[:phone_number]
      session[:country_code] = "+62"
      redirect_to verifikasi_authenticate_phones_path
    else
      respond_to do |format|
        format.html{
          flash[:alert] = "Nomor Handphone Yang Anda Masukkan Kurang"
          render :new
         }
      end

    end
  end

  def verifikasi

  end

  def konfirmasi
    @response = Authy::PhoneVerification.check(
      verification_code: params[:code],
      country_code: session[:country_code],
      phone_number: session[:phone_number]
    )
    if @response.ok?
      # current_user.mahasiswa_profile.update(handphone: params[:phone_number])
      respond_to do |format|
        format.html{
          current_user.update!(phone_number: session[:phone_number], is_phone_verified: true)
          logger.debug "Oi sudah save ini"
          session[:phone_number] = nil
          session[:country_code] = nil
          flash[:notice] = "Kode Yang Anda Masukkan Benar"
          redirect_to root_path
         }
      end

    else
      respond_to do |format|
        format.html{
          flash[:alert] = "Kode Yang Anda Masukkan Salah"
          render :verifikasi
         }
      end

    end
  end

  def destroy
    # session[:user_id] = nil
  end
end
