class LectureReviewersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_review, only:[:edit, :update, :destroy]
  def new
    @reviewer = @lecture.lecture_reviewers.build
  end

  def create
    @reviewer = @lecture.lecture_reviewers.build(review_params)
    if @reviewer.save
       redirect_to lecture_path(@lecture, type:"PENYUNTING"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @reviewer.update(review_params)
      redirect_to lecture_path(@lecture, type:"PENYUNTING"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @reviewer.destroy
    redirect_to lecture_path(@lecture, type:"PENYUNTING"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_review
    @reviewer = @lecture.lecture_reviewers.find(params[:id])
  end

  def review_params
    params.require(:lecture_reviewer).permit(
      :lecture_id,
      :year,
      :review,
      :publisher
    )
  end
end
