class LectureScientificWorksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_scientific, only:[:edit, :update, :destroy]
  def new
    @scientific = @lecture.lecture_scientific_works.build
  end

  def create
    @scientific = @lecture.lecture_scientific_works.build(scientific_params)
    if @scientific.save
       redirect_to lecture_path(@lecture, type:"PENYUNTING"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @scientific.update(scientific_params)
      redirect_to lecture_path(@lecture, type:"PENYUNTING"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @scientific.destroy
    redirect_to lecture_path(@lecture, type:"PENYUNTING"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_scientific
    @scientific = @lecture.lecture_scientific_works.find(params[:id])
  end

  def scientific_params
    params.require(:lecture_scientific_work).permit(
      :lecture_id,
      :year,
      :science_product,
      :publisher
    )
  end
end
