class MahasiswasController < ApplicationController
  before_action :authenticate_phone_number
  before_action :set_mahasiswa, only: [:edit_biodata, :update]
  def index
    @mahasiswas = Mahasiswa.order(:created_at).page(params[:page]).per(10)
  end

  def biodata
    @mahasiswa = Mahasiswa.find(params[:id])
  end

  def edit_biodata
    @mahasiswa = Mahasiswa.find(params[:id])
    @mahasiswa.build_mahasiswa_profile unless @mahasiswa.mahasiswa_profile.present?
  end

  def update
    respond_to do |format|
      if @mahasiswa.update(mahasiswa_params)
        session[:phone_number] = nil
        session[:country_code] = nil
        format.html{redirect_to root_path, notice: "Data Mahasiswa Berhasil Dibuat"}
      else
        format.html{redirect_to root_path, notice: "Data Mahasiswa Gagal Dibuat"}
      end
    end

  end

  def activate
    if params[:commit] == "Status Aktif"
      @mahasiswas = Mahasiswa.where(id: params[:mahasiswa_id]).update_all(is_active: true)
      redirect_to mahasiswas_url
    elsif params[:commit] == "Hapus Semua"
      @mahasiswas = Mahasiswa.where(id: params[:mahasiswa_id]).delete_all
      redirect_to mahasiswas_url
    end
  end

  private
  def set_mahasiswa
    @mahasiswa = Mahasiswa.find(params[:id])
  end

  def mahasiswa_params
    params.require(:mahasiswa).permit(
      mahasiswa_profile_attributes: [
        :id,
        :major_id,
        :nim,
        :kategori,
        :nama,
        :jenis_kelamin,
        :tempat_lahir,
        :tanggal_lahir,
        :religion_id,
        :nik,
        :nisn,
        :npwp,
        :state_id,
        :jalan,
        :dusun,
        :rt,
        :rw,
        :kelurahan,
        :kode_pos,
        :district_id,
        # districts_attributes:[:id, :nama, :state_id, :id_wilayah],
        :live_id,
        :transportation_id,
        # :telepon,
        :handphone,
        :penerima_kps,
        :nomor_kps,
        :nik_ayah,
        :nama_ayah,
        :tanggal_lahir_ayah,
        :pendidikan_ayah_id,
        :pekerjaan_ayah_id,
        :penghasilan_ayah_id,
        :nik_ibu,
        :nama_ibu,
        :tanggal_lahir_ibu,
        :pendidikan_ibu_id,
        :pekerjaan_ibu_id,
        :penghasilan_ibu_id,
        :nama_wali,
        :tanggal_lahir_wali,
        :pendidikan_wali_id,
        :pekerjaan_wali_id,
        :penghasilan_wali_id,
        :kebutuhan_khusus_ayah_id,
        :kebutuhan_khusus_ibu_id,
        :kebutuhan_khusus_mahasiswa_id,
        :_destroy
      ],
      documents_attributes: [
        :id,
        :kategori,
        :file_dokumen,
        :academic_year_id,
        :_destroy
      ]
    )
  end


end
