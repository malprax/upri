class LectureConferencesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_conference, only:[:edit, :update, :destroy]
  def new
    @conference = @lecture.lecture_conferences.build
  end

  def create
    @conference = @lecture.lecture_conferences.build(conference_params)
    if @conference.save
       redirect_to lecture_path(@lecture, type:"KONFERENSI"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @conference.update(conference_params)
      redirect_to lecture_path(@lecture, type:"KONFERENSI"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @conference.destroy
    redirect_to lecture_path(@lecture, type:"KONFERENSI"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_conference
    @conference = @lecture.lecture_conferences.find(params[:id])
  end

  def conference_params
    params.require(:lecture_conference).permit(
      :lecture_id,
      :year,
      :conference,
      :presented_by,
      :role
    )
  end
end
