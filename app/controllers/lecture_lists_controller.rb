class LectureListsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_user_active
  def index
    @lecture_lists = LectureList.all
  end

  def new
    @lecture_list = LectureList.new
  end

  def create
    #code
  end

  def show
    #code
  end

  def edit
    #code
  end

  def update
    #code
  end

  def destroy
    #code
  end

  private
  def set_lecture
    #code
  end

  def lecture_params
    params.require(:lecture_list).permit(:name, :nidn, :nip, :status, :jabatan_struktural, :major_id, :faculty_id)
  end
end
