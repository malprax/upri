class PhoneVerificationsController < ApplicationController
  def new
  end

  def create
    @response = Authy::PhoneVerification.start(
      via: "sms",
      country_code: 62,
      phone_number: params[:phone_number]
    )

    if @response.ok?
      session[:phone_number] = params[:phone_number]
      redirect_to verifikasi_phone_verifications_path
    else
      flash[:alert] = "Kode Yang Anda Masukkan Salah"
      render :new
    end
  end

  def verifikasi

  end

  def konfirmasi
    @response = Authy::PhoneVerification.check(
      verification_code: params[:code],
      country_code: 62,
      phone_number: session[:phone_number]
    )
    if @response.ok?
      # current_user.mahasiswa_profile.update(handphone: params[:phone_number])
          session[:phone_number] = nil

          flash[:notice] = "Kode Yang Anda Masukkan Benar"
          redirect_to edit_biodata_mahasiswa_path(current_user.id)
    else
      respond_to do |format|
        format.html{
          flash[:alert] = "Kode Yang Anda Masukkan Salah"
          render :verifikasi
         }
      end
    end
  end

  def success
  end
end
