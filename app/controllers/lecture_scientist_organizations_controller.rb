class LectureScientistOrganizationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_scientist_organization, only:[:edit, :update, :destroy]
  def new
    @scientist_organization = @lecture.lecture_scientist_organizations.build
  end

  def create
    @scientist_organization = @lecture.lecture_scientist_organizations.build(scientist_organization_params)
    if @scientist_organization.save
       redirect_to lecture_path(@lecture, type:"ORGANISASI PROFESI"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @scientist_organization.update(scientist_organization_params)
      redirect_to lecture_path(@lecture, type:"ORGANISASI PROFESI"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @scientist_organization.destroy
    redirect_to lecture_path(@lecture, type:"ORGANISASI PROFESI"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_scientist_organization
    @scientist_organization = @lecture.lecture_scientist_organizations.find(params[:id])
  end

  def scientist_organization_params
    params.require(:lecture_scientist_organization).permit(
      :lecture_id,
      :year,
      :organization,
      :position
    )
  end
end
