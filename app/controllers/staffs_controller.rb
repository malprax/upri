class StaffsController < ApplicationController
  before_action :authenticate_user!
  def active_one
    @staff = Staff.find(params[:id])
    if @staff.is_active == false
      @staff.update(is_active: true)
      redirect_to staffs_path, notice: "Berhasil diaktifkan"
    elsif @staff.is_active == true
      @staff.update(is_active: false)
      redirect_to staffs_path, notice: "Berhasil digagalkan"
    elsif @staff.is_active == nil
      @staff.update(is_active: true)
      redirect_to staffs_path, notice: "Berhasil diaktifkan"
    end
  end

  def active_all
    if params[:commit] == "Semua Aktif"
      @staffs = Staff.where(id: params[:staff_id]).update_all(is_active: true)
      redirect_to staffs_path, notice: "Berhasil Aktif"
    elsif params[:commit] == "Semua Tidak Aktif"
      @staffs = Staff.where(id: params[:staff_id]).update_all(is_active: false)
      redirect_to staffs_path, notice: "Berhasil Tidak Aktif"
    end
  end

  def index
    # @staffs = StaffProfile.order(sort_column + " " + sort_direction)
    @staffs = Staff.where("created_at > ?", "01 Jan 2020").includes(:major).order(sort_column + " " + sort_direction).page(params[:page]).per(20)
    @staffs = @staffs.staff_search(params[:staff_search]) if params[:staff_search]
    respond_to do |format|
      format.html
      format.js
    end
  end


  def sort_column
    params[:sort] || 'created_at'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def edit_formulir
    @staff = Staff.find(params[:id])
  end

  def edit
    @staff = Staff.find(params[:id])
    @staff.build_staff_profile unless @staff.staff_profile.present?
    @staff.build_alamat_tinggal unless @staff.alamat_tinggal.present?
  end

  def update
    @staff = Staff.find(params[:id])
    respond_to do |format|
      if @staff.update!(staff_params)
        if @staff.is_active?
          format.html{redirect_to staff_path(@staff), notice: "Data Berhasil Diperbarui"}
        elsif @staff.is_not_active?
          format.html{redirect_to edit_staff_path(@staff), notice: "Data Calon Mahasiswa Berhasil Diperbarui"}
        else
          format.html{redirect_to staff_path(@staff), notice: "Data mahasiswa Berhasil Diperbarui"}
        end
      else
        format.html{redirect_to edit_staff_path(@staff), notice: "Data Gagal Diperbarui"}
      end
    end
  end

  def show
    if current_user.is_staff?
      @staff = Staff.find(params[:id])
    else
      @staff = Staff.find(params[:id])
    end
  end

  def destroy
    @staff = Staff.find(params[:id])
    if @staff.destroy
      redirect_to staffs_path, notice: "Data berhasil dihapus"
    end
  end

  def staff_params
    params.require(:staff).permit(
      :major_id,
      :staff_photo_file,
      :religion_id,
      staff_profile_attributes:[
       :id,
       :nama,
       :gelar_depan_nama,
       :gelar_belakang_nama,
       :nomor_induk_pegawai,
       :nomor_induk_dosen_negara,
       :nomor_induk_kependudukan,
       :jenis_kelamin,
       :tempat_lahir,
       :tanggal_lahir,
       :lecture_type,
       :status,
       :status_perkawinan,
       :handphone,
       :kewarganegaraan,
       :_destroy
     ],
     alamat_tinggal_attributes:[
       :id,
       :street,
       :rt,
       :rw,
       :kelurahan,
       :kecamatan,
       :city,
       :province,
       :post_code,
       :_destroy
     ]
    )
  end
end
