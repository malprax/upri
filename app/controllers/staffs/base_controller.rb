class Staffs::BaseController < ApplicationController
  before_action :authenticate_user!
  before_action :checking_staff!
  #
  private
  def checking_staff!
    redirect_to send("#{user_base_url.pluralize}_dashboards_path"), flash: { error: 'Anda tidak memiliki akses ke halaman ini' } if !current_user.is_staff?
  end
end
