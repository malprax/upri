class AdminsController < ApplicationController
  before_action :authenticate_user!
  def new
    #code
  end

  def create
    @admin.skip_check_type = true
    @admin.is_active = true
    unless @admin.save
      render :new
    end
  end

  def edit
    #code
  end

  def update
    unless @admin.update_attributes(update_params)
      render :edit
    end
  end

  private
  def update_params
      params.require(:admin).permit(:email)
  end
end
