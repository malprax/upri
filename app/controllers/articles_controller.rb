class ArticlesController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :authenticate_user_active, except:[:show]
  def new
    @journal = Journal.find(params[:journal_id])
    @article = @journal.articles.build
  end

  def create
    respond_to do |format|
      @journal = Journal.find(params[:journal_id])
      if @article = @journal.articles.create(article_params)
        format.html{redirect_to journal_path(@article), notice: "Data berhasil dibuat"}
      else
        flash[:alert] = "Data gagal disimpan"
        render :new
      end
    end
  end

  def edit
    @journal = Journal.find(params[:journal_id])
    @article = @journal.articles.find(params[:id])
  end

  def update
    respond_to do |format|
      @journal = Journal.find(params[:Journal_id])
      @article = @journal.articles.find(params[:id])
      if @article.update_attributes(article_params)
        format.html{redirect_to journal_path(@journal), notice: "Data berhasil dibuat"}
      else
        flash[:alert] = "Data gagal disimpan"
        render :new
      end
    end
  end

  def show
    # code
      @journal = Journal.find(params[:Journal_id])
      @article = @journal.articles.find(params[:id])
      respond_to do |format|
        format.html
        format.js
      end
  end

  def destroy
    # code
    @journal = Journal.find(params[:Journal_id])
    @article = @journal.articles.find(params[:id])
    @article.destroy
    redirect_to folder_path(@journal)
  end

  private
  def set_document
    @journal = Journal.find(params[:Journal_id])
    @article = @journal.articles.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:author, :journal_id, :title, :article_file)
  end
end
