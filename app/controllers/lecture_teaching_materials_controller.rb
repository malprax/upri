class LectureTeachingMaterialsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_material, only:[:edit, :update, :destroy]
  def new
    @material = @lecture.lecture_teaching_materials.build
  end

  def create
    @material = @lecture.lecture_teaching_materials.build(material_params)
    if @material.save
       redirect_to lecture_path(@lecture, type:"BAHAN AJAR"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @material.update(material_params)
      redirect_to lecture_path(@lecture, type:"BAHAN AJAR"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @material.destroy
    redirect_to lecture_path(@lecture, type:"BAHAN AJAR"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_material
    @material = @lecture.lecture_teaching_materials.find(params[:id])
  end

  def material_params
    params.require(:lecture_teaching_material).permit(
      :lecture_id,
      :subject,
      :major,
      :kind,
      :semester_status,
      :academic_year
    )
  end
end
