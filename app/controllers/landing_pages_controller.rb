class LandingPagesController < ApplicationController
  layout 'landing_pages'
  def index
    @academic_year = AcademicYear.aktif
  end
end
