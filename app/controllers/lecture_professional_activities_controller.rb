class LectureProfessionalActivitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_professional, only:[:edit, :update, :destroy]
  def new
    @professional = @lecture.lecture_professional_activities.build
  end

  def create
    @professional = @lecture.lecture_professional_activities.build(professional_params)
    if @professional.save
       redirect_to lecture_path(@lecture, type:"PENGABDIAN MASYARAKAT"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @professional.update(professional_params)
      redirect_to lecture_path(@lecture, type:"PENGABDIAN MASYARAKAT"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @professional.destroy
    redirect_to lecture_path(@lecture, type:"PENGABDIAN MASYARAKAT"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_professional
    @professional = @lecture.lecture_professional_activities.find(params[:id])
  end

  def professional_params
    params.require(:lecture_professional_activity).permit(
      :lecture_id,
      :year,
      :activity,
      :place
    )
  end
end
