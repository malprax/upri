class StudentsController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :authenticate_user!
  # before_action :authenticate_admin, :authenticate_staff, only:[:destroy]
  # before_action :check_is_active, only:[:index, :show]

  def delete_file
    @attachment = ActiveStorage::Attachment.find(params[:id])
    @attachment.purge
    redirect_back(fallback_location: request.referer)
  end

  def print_blanko
    # :kind, :academic_year_id, :in_number, :in_word, :student_id, :bank_id
    @bank = Bank.where(name: "BRI", account_number: "034301001373303").first
    @academic_year = AcademicYear.find_by(is_active: true)
    @student = Student.find(params[:id])
    @payment = @student.payments.where(kind:'registrasi').first
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Blanko Pembayaran",
        template: "students/print_blanko.html.erb",
        layout: 'pdf.html',
        orientation: 'Landscape',
        margin:  {top:10,left: 15},
        zoom: 0.78125,
        locals: {:payment => @payment},
        page_size: 'A4'
      end
    end
  end

  def active_one
    @student = Student.find(params[:id])
    if @student.is_active == false
      @student.update(is_active: true)
      StudentMailer.activation(@student).deliver_now
      redirect_to students_path, notice: "Berhasil diaktifkan"
    elsif @student.is_active == true
      @student.update(is_active: false)
      redirect_to students_path, notice: "Berhasil digagalkan"
    elsif @student.is_active == nil
      @student.update(is_active: true)
      StudentMailer.activation(@student).deliver_now
      redirect_to students_path, notice: "Berhasil diaktifkan"
    end
  end

  def active_all
    if params[:commit] == "Semua Aktif"
      @students = Student.where(id: params[:student_id]).update_all(is_active: true)
      redirect_to students_path, notice: "Berhasil Aktif"
    elsif params[:commit] == "Semua Tidak Aktif"
      @students = Student.where(id: params[:student_id]).update_all(is_active: false)
      redirect_to students_path, notice: "Berhasil Tidak Aktif"
    end
  end

  def index
    # @students = StudentProfile.order(sort_column + " " + sort_direction)
    @students = Student.begining.includes(:major, :student_profile, :parent, :school, :subjects, :subject_plans, :payments).order(sort_column + " " + sort_direction).page(params[:page]).per(20)
    @students = @students.search(params[:search]) if params[:search].present?
    respond_to do |format|
      format.html
      format.js
    end
  end


  def sort_column
    params[:sort] || 'created_at'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def edit_formulir
    @student = Student.find(params[:id])
  end

  def edit
    if current_user.type == "Student"
      @student = current_user
    else
      @student = Student.find(params[:id])
    end

    @student.build_student_profile unless @student.student_profile.present?
    @student.build_alamat_tinggal unless @student.alamat_tinggal.present?
    @student.build_school unless @student.school.present?
    @student.build_parent unless @student.parent.present?
    @payment = @student.payments.where(kind:"registrasi").first if @student.payments.where(kind:"registrasi").present?
  end

  def update
    @student = Student.find(params[:id])
    respond_to do |format|
      if @student.update(student_params)
        if @student.is_active?
          format.html{redirect_to student_path(@student), notice: "Data Berhasil Diperbarui"}
        elsif @student.is_not_active?
            @student.create_payment
          format.html{redirect_to edit_student_path(@student), notice: "Data Calon Mahasiswa Berhasil Diperbarui"}
        else
          format.html{redirect_to students_path, notice: "Data mahasiswa Berhasil Diperbarui"}
        end
      else
        format.html{redirect_to edit_student_path(@student), notice: "Data Gagal Diperbarui"}
      end
    end
  end

  def show
    if current_user.is_student?
      @student = Student.find(params[:id])
    else
      @student = Student.find(params[:id])
    end
  end

  def destroy
    @student = Student.find(params[:id])
    if @student.destroy
      redirect_to students_path, notice: "Data berhasil dihapus"
    end
  end

  def make
    @student = Student.find(params[:id])
    respond_to do |format|
      @student.make!
      format.html{redirect_to daftar_calon_mahasiswa_path, notice: "Berhasil Jadi Mahasiswa"}
    end
  end

  def studyplans
    @student = current_user
    @subjects = Subject.all
    # @subject_ids = params[:subject_ids].require('subject_ids')
    @subject_ids = Subject.where(params[:subject_id])
    @students = Student.all
    @subject_ids.each do |subject_id|
      @study_plan = StudyPlan.new(study_plans_params)
      @study_plan.subject_id = subject_id
      if @study_plan.save
        flash[:success] = "Rencana studi berhasil dibuat"
      else
        @study_plan.errors
        return
      end
    end
    redirect_to root_path
  end
  # def addworkers_tm
  #   @trainings = Training.all
  #   @training = params[:training_id]
  #   @tm = Trainingsmembership.new(tm_params)
  #   if @tm.save
  #     flash[:success] = "Mitarbeiter eingefügt"
  #     redirect_to training_path
  #   else
  #     render 'add_worker_tm'
  #   end
  # end

  def study_plans_params
    params.require(:study_plan).permit(:student_id, :academic_year_id, subject_id:[])
  end

  def student_params
    params.require(:student).permit(
      :major_id,
      :student_photo_file,
      :kartu_keluarga_file,
      :ijazah_file,
      :kartu_tanda_penduduk_file,
      :gender,
      :birth_date,
      :birth_place,
      :referral,
      student_profile_attributes: [
       :id,
       :nama,
       :nomor_induk_mahasiswa,
       :jenis_kelamin,
       :tempat_lahir,
       :tanggal_lahir,
       :handphone,
       :agama,
       :kewarganegaraan,
       :major_id,
       :status,
       :keterangan_tinggal,
       :is_default,
       :_destroy
      ],
      alamat_tinggal_attributes:[
        :id,
        :street,
        :rt,
        :rw,
        :kelurahan,
        :kecamatan,
        :city,
        :province,
        :post_code,
        :_destroy
      ],

      parent_attributes:[
        :id,
        :nama_ayah,
        :pekerjaan_ayah,
        :parents_guardians_handphone,
        :nama_ibu,
        :pekerjaan_ibu,
        :nama_wali,
        :pekerjaan_wali,
        :_destroy
      ],
      school_attributes:[
        :id,
        :nama,
        :jurusan,
        :_destroy
      ],
      subject_ids:[]
    )
  end

end
