class ItemAccreditationsController < ApplicationController
  before_action :set_item_accreditation, only: [:show, :edit, :update, :destroy]

  # GET /item_accreditations
  # GET /item_accreditations.json
  def index
    @item_accreditations = ItemAccreditation.all
  end

  # GET /item_accreditations/1
  # GET /item_accreditations/1.json
  def show
  end

  # GET /item_accreditations/new
  def new
    @item_accreditation = ItemAccreditation.new
  end

  # GET /item_accreditations/1/edit
  def edit
  end

  # POST /item_accreditations
  # POST /item_accreditations.json
  def create
    @item_accreditation = ItemAccreditation.new(item_accreditation_params)

    respond_to do |format|
      if @item_accreditation.save
        format.html { redirect_to @item_accreditation, notice: 'Item accreditation was successfully created.' }
        format.json { render :show, status: :created, location: @item_accreditation }
      else
        format.html { render :new }
        format.json { render json: @item_accreditation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /item_accreditations/1
  # PATCH/PUT /item_accreditations/1.json
  def update
    respond_to do |format|
      if @item_accreditation.update(item_accreditation_params)
        format.html { redirect_to @item_accreditation, notice: 'Item accreditation was successfully updated.' }
        format.json { render :show, status: :ok, location: @item_accreditation }
      else
        format.html { render :edit }
        format.json { render json: @item_accreditation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /item_accreditations/1
  # DELETE /item_accreditations/1.json
  def destroy
    @item_accreditation.destroy
    respond_to do |format|
      format.html { redirect_to item_accreditations_url, notice: 'Item accreditation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item_accreditation
      @item_accreditation = ItemAccreditation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_accreditation_params
      params.require(:item_accreditation).permit(:item, :academic_year_id)
    end
end
