class LectureResearchesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_research, only:[:edit, :update, :destroy]
  def new
    @research = @lecture.lecture_researches.build
  end

  def create
    @research = @lecture.lecture_researches.build(research_params)
    if @research.save
       redirect_to lecture_path(@lecture, type:"PENELITIAN"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @research.update(research_params)
      redirect_to lecture_path(@lecture, type:"PENELITIAN"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @research.destroy
    redirect_to lecture_path(@lecture, type:"PENELITIAN"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_research
    @research = @lecture.lecture_researches.find(params[:id])
  end

  def research_params
    params.require(:lecture_research).permit(
      :lecture_id,
      :year,
      :research,
      :role,
      :source_funds
    )
  end
end
