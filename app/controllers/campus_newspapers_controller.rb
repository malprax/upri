class CampusNewspapersController < ApplicationController
  before_action :authenticate_user!, except: [:kabar_teknik]
  before_action :authenticate_user_active
  before_action :set_campus_newspaper, only: [:show, :edit, :update, :destroy]

  def kabar_teknik
    @news = CampusNewspaper.where(terbit: true).order("created_at desc")
    if params[:judul_kategori]
      @campus_newspapers = CampusNewspaper.where(terbit: true).order("created_at desc")
      @campus_newspapers.judul_kategori(params[:judul_kategori])
    else
      @campus_newspapers = CampusNewspaper.where(terbit: true).order("created_at desc")
    end
    if params[:kategori]
      @campus_newspapers = CampusNewspaper.where(kategori: params[:kategori], terbit: true).order("created_at desc")
    elsif params[:judul_kategori]
      @campus_newspapers = CampusNewspaper.where(terbit: true).order("created_at desc")
      @campus_newspaper.judul_kategori(params[:judul_kategori])
    else
      @campus_newspapers = CampusNewspaper.where(terbit: true).order("created_at desc")
    end
    @list_by_dates = CampusNewspaper.where(terbit:true).group_by{|kabar| kabar.created_at}
    @list_by_categories = CampusNewspaper.where(terbit:true).group_by{|kabar| kabar.kategori}
  end

  def index
    @campus_newspapers = CampusNewspaper.order("created_at desc")
  end

  def new
    @campus_newspaper = CampusNewspaper.new
  end

  def create
    @campus_newspaper = CampusNewspaper.new(campus_newspaper_params)
    if @campus_newspaper.save
      respond_to do |format|
        format.html{redirect_to campus_newspapers_path, notice: "Kabar kampus berhasil dibuat"}
      end
    else
      flash[:error]="Kabar kampus gagal dibuat"
      render :new
    end
  end

  def edit
  end

  def update
    @campus_newspaper = CampusNewspaper.find(params[:id])
    if @campus_newspaper.update_attributes(campus_newspaper_params)
      respond_to do |format|
        format.html{redirect_to campus_newspapers_path, notice: "Kabar kampus berhasil diperbarui"}
      end
    else
      flash[:error]="Kabar kampus gagal diperbarui"
      render :edit
    end
  end

  def show

  end

  def destroy
    @campus_newspaper.delete
    redirect_to campus_newspapers_path
  end

  private
  def set_campus_newspaper
    @campus_newspaper = CampusNewspaper.find(params[:id])
  end
  def campus_newspaper_params
    params.require(:campus_newspaper).permit(:judul, :isi, :kategori, :user_id, :terbit, :posisi, :attachment, uploads:[])
  end
end
