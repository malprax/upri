class CandidateStudentsController < ApplicationController
  # before_action :authenticate_user!, except: [:new, :create]
  before_action :set_candidate_student, only: [:edit, :update, :show, :destroy]
  def index
    @candidate_students = CandidateStudent.order(:created_at)
  end

  def new
    @candidate_student = CandidateStudent.new
  end

  def create
    @candidate_student = CandidateStudent.new(candidate_params)
    respond_to do |format|
      if verify_recaptcha(model: @candidate_student) && @candidate_student.save
        logger.debug "tersaveki ----------------------------------------"
        format.html{redirect_to root_path, notice: "Data berhasil disimpan"}
        format.json { render :index, status: :created, location: @candidate_student }
      else
        flash[:alert] = "Data gagal disimpan"
        logger.debug "Errorki ----------------------------------------"
        format.html{render :new, notice: "Data berhasil disimpan"}
        format.json { render json: @candidate_student.errors, status: :unprocessabel_entity}
      end
    end
  end

  private
  def set_candidate_student
    @candidate_student = CandidateStudent.find(params[:id])
  end

  def candidate_params
    params.require(:candidate_student).permit(
      :registration_number,
      :class_plan,
      :major_1,
      :major_2,
      :name,
      :birth_date,
      :birth_place,
      :gender,
      :phone_number,
      :street_address,
      :hamlet,
      :neighbourhood,
      :urban_village,
      :sub_district,
      :district,
      :province,
      :postal_code,
      :school,
      :school_address,
      :school_plan,
      :nisn,
      :sertificate_number,
      :sertificate_date,
      :national_exam_score,
      :father_name,
      :father_job,
      :father_salary,
      :father_education,
      :mother_name,
      :mother_job,
      :mother_salary,
      :mother_education,
      :know_from,
      :academic_year_id,
      :referral,
      :email,
      :approve
    )
  end
end
