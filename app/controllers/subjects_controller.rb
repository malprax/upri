class SubjectsController < ApplicationController
  before_action :authenticate_user!
  # before_action :check_staff_is_active, except: [:index]
  # before_action :authenticate_admin, only: [:destroy]
  before_action :set_subject, only: [:edit, :update, :destroy]
  # has_scope :semester_search
  # has_scope :word_search
  # has_scope :khusus_search
  helper_method :sort_column, :sort_direction

  def index
    @subject = Subject.new
    @subjects = Subject.includes(:subjectplans_subjects, :subject_plans).order(:semester).order(:matakuliah).order(:kredit).order("kategori desc")
    if current_user.is_student?
    @student = current_user
    elsif current_user.is_staff?
    @subjects = Subject.order(sort_column + " " + sort_direction)
    elsif current_user.is_lecture?
    @subjects = Subject.order(sort_column + " " + sort_direction)
    end
    @subjects = @subjects.khusus(params[:khusus]) if params[:khusus].present?
    @subjects = @subjects.word(params[:word]) if params[:word].present?
    @subjects = @subjects.semester(params[:semester]) if params[:semester].present?
    @subjects = @subjects.major(params[:major]) if params[:major].present?
    @subjects = @subjects.tahun_berlaku(params[:tahun_berlaku]) if params[:tahun_berlaku].present?
    @subjects = @subjects.kategori_matakuliah(params[:kategori_matakuliah]) if params[:kategori_matakuliah].present?
    respond_to do |format|
      format.html
      format.js
    end
  end

  def sort_column
    params[:sort] || 'semester'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def new
    @subject = Subject.new
  end

  def create
    @subject = Subject.new(subject_params)
    respond_to do |format|
      if @subject.save
        format.html{redirect_to subjects_path, notice: "Data Berhasil Dibuat" }
        format.json { render json: @subject }
      else
        format.html{redirect_to subjects_path, notice: "Data Gagal Dibuat" }
      end
    end

  end

  def edit
    @majors = Major.where(faculty_id: 3)
  end

  def update
    respond_to do |format|
      if @subject.update(subject_params)
        format.html{redirect_to subjects_path, notice: "Data Berhasil Dibuat" }
        format.json { render json: @subject }
      else
        format.html{redirect_to subjects_path, notice: "Data Gagal Dibuat" }
      end
    end
  end

  def destroy
    respond_to do |format|
      @subject.delete
      format.html {redirect_to subjects_path, notice: "Data Berhasil Dihapus"}
      format.json { head :no_content }
    end

  end

  private
  def set_subject
    @subject = Subject.find(params[:id])
  end

  def subject_params
    params.require(:subject).permit(
      :matakuliah,
      :kode,
      :kredit,
      :semester,
      :kategori,
      :faculty,
      :major,
      :tahun_berlaku,
      :khusus,
      tags:[]
    )
  end
end
