class LectureTeachingHistoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_teaching, only:[:edit, :update, :destroy]
  def new
    @teaching = @lecture.lecture_teaching_histories.build
  end

  def create
    @teaching = @lecture.lecture_teaching_histories.build(teaching_params)
    if @teaching.save
       redirect_to lecture_path(@lecture, type:"MENGAJAR"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @teaching.update(teaching_params)
      redirect_to lecture_path(@lecture, type:"MENGAJAR"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @teaching.destroy
    redirect_to lecture_path(@lecture, type:"MENGAJAR"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_teaching
    @teaching = @lecture.lecture_teaching_histories.find(params[:id])
  end

  def teaching_params
    params.require(:lecture_teaching_history).permit(
      :lecture_id,
      :subject,
      :major,
      :institute,
      :semester_status,
      :academic_year
    )
  end
end
