class LectureEducationHistoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lecture
  before_action :set_education, only:[:edit, :update, :destroy]
  def new
    @education = @lecture.lecture_education_histories.build
  end

  def create
    @education = @lecture.lecture_education_histories.build(education_params)
    if @education.save
       redirect_to lecture_path(@lecture, type:"RIWAYAT PENDIDIKAN"), notice: "Data berhasil disimpan"
    else
      flash[:alert] = "Data gagal disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    if @education.update(education_params)
      redirect_to lecture_path(@lecture, type:"RIWAYAT PENDIDIKAN"), notice: "Data berhasil diperbarui"
    else
      flash[:alert] = "Data gagal disimpan"
      render :edit
    end
  end

  def destroy
    @education.destroy
    redirect_to lecture_path(@lecture, type:"RIWAYAT PENDIDIKAN"), notice: "Data berhasil dihapus"
  end

  private
  def set_lecture
    @lecture = Lecture.find(params[:lecture_id])
  end

  def set_education
    @education = @lecture.lecture_education_histories.find(params[:id])
  end

  def education_params
    params.require(:lecture_education_history).permit(
      :lecture_id,
      :level,
      :major,
      :institute,
      :graduation_year
    )
  end
end
