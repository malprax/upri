class AcademicYearsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin
  before_action :set_academic_year, only:[:edit, :update]

  def index
    @academic_years = AcademicYear.sortir_dari_tahun_terakhir_dan_aktifnya
    @academic_year = AcademicYear.new
    # @academic_year_before = AcademicYear.find_by(is_active: true)
  end

  def new
    @academic_year = AcademicYear.new
    # @academic_year_before = AcademicYear.where(is_active: true).first
  end

  def create
    @academic_year = AcademicYear.new(academic_year_params)
    @academic_years = AcademicYear
    @academic_years.update_all(is_active: false)
    if @academic_year.save
      flash[:notice] = "Data berhasil disimpan"
      redirect_to academic_years_path
    else
      flash[:notice] = "Data tidak berhasil disimpan"
      render :new
    end
  end

  def edit
  end

  def update
    @academic_years = AcademicYear
    @academic_years.update_all(is_active: false)
    @academic_year.update(academic_year_params)
    if @academic_year.save
      flash[:notice] = "Data berhasil disimpan"
      redirect_to academic_years_path
    else
      flash[:alert] = "Data tidak dapat disimpan"
      render :edit
    end
  end


  private

  def set_academic_year
    @academic_year = AcademicYear.find(params[:id])
  end

  def academic_year_params
    params.require(:academic_year).permit(:name, :is_active, :semester, :start_at, :end_at)
  end
end
