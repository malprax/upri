class SubjectPlansController < ApplicationController
  before_action :authenticate_user!
  # before_action :authenticate_staff, except: [:index]
  before_action :authenticate_student, only: [:print]
  before_action :set_subject_plan, only: [:show, :edit, :update, :destroy, :approve_one, :print]
  before_action :set_semester_active, only: [:index, :show, :new, :edit, :destroy, :approve_one, :print, :print_sp]
  before_action :check_if_approved?, only:[:edit, :destroy]

  def check_if_approved?
    @subject_plan = SubjectPlan.find(params[:id])
    if current_user.is_student?
      redirect_to subject_plans_path, alert: "KRS sudah tidak bisa lagi di edit karena sudah diperiksa bagian akademik" if @subject_plan.is_approved?
    end
  end

  def authenticate_student
    if current_user.is_student?
      @subject_plan = SubjectPlan.find(params[:id])
      redirect_to token_session_path(subject_plan_id: @subject_plan.id), notice: "Masukkan Kode Token Terlebih Dahulu" if current_authenticate_student.nil?
    end
  end

  def authenticate_staff
    if current_user.is_staff?
      redirect_to token_session_path, notice: "Masukkan Kode Token Terlebih Dahulu" if current_authenticate_staff.nil?
    end
  end

  def index
    @subject_plan_semester_pendek = SubjectPlan.where(student_id: current_user.id, academic_year_id: current_academic_year.id).first
    if current_user.is_student?
      @subject_plans = SubjectPlan.includes(:academic_year, student:[:student_profile, major: :faculty]).where(student_id: current_user.id).order("academic_years.name desc").order("academic_years.semester desc").page(params[:page]).per(10)
    elsif current_user.is_staff?
      @academic_year_active = AcademicYear.where(is_active: true).first
      @academic_years = AcademicYear.order("created_at desc")
      @subject_plans = SubjectPlan.includes(:academic_year, student:[:student_profile, major: :faculty]).order("academic_years.name desc").order("academic_years.semester desc").page(params[:page]).per(10)
    elsif current_user.is_lecture?
      @academic_year_active = AcademicYear.where(is_active: true).first
      @academic_years = AcademicYear.order("created_at desc")
      @subject_plans = SubjectPlan.includes(:academic_year, student:[:student_profile, major: :faculty]).order("academic_years.name desc").order("academic_years.semester desc").page(params[:page]).per(10)
    end
    @subject_plans = @subject_plans.search(params[:search]) if params[:search].present?
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new

    if current_user.is_student?
      if current_user.student_profile.angkatan.to_i >= 2018
        # if @academic_year_active.semester == 'ganjil'
        #   @subjects = Subject.where("'pertambangan' = ANY (tags)", semester:["1","3","5","7"]).where.not(tahun_berlaku: "2014").order(:semester).order(:matakuliah).order(:kategori)
        # elsif @academic_year_active.semester == 'genap'
        #   @subjects = Subject.where("'pertambangan' = ANY (tags)", semester:["2","4","6","8"]).where.not(tahun_berlaku: "2014").order(:semester).order(:matakuliah).order(:kategori)
        # elsif @academic_year_active.semester == 'pendek'
        #   @subjects = Subject.where("'pertambangan' = ANY (tags)").where.not(tahun_berlaku: "2014").order(:semester).order(:matakuliah).order(:kategori)
        # end
        @subjects = Subject.where("'pertambangan' = ANY (tags)").where.not(tahun_berlaku: "2014").order(:semester).order(:matakuliah).order(:kategori)

      else
        if current_user.student_profile.konsentrasi == "tambang umum"
          # if @academic_year_active.semester == 'ganjil'
          #   @subjects = Subject.where("'pertambangan' = ANY (tags)", semester:["1","3","5","7"]).where.not(tahun_berlaku: "2018", khusus:"tambang eksplorasi").order(:semester).order(:matakuliah).order(:kategori)
          # elsif @academic_year_active.semester == 'genap'
          #   @subjects = Subject.where("'pertambangan' = ANY (tags)", semester:["2","4","6","8"]).where.not(tahun_berlaku: "2018", khusus:"tambang eksplorasi").order(:semester).order(:matakuliah).order(:kategori)
          # elsif @academic_year_active.semester == 'pendek'
          #   @subjects = Subject.where("'pertambangan' = ANY (tags)").where.not(tahun_berlaku: "2018", khusus:"tambang eksplorasi").order(:semester).order(:matakuliah).order(:kategori)
          # end
          @subjects = Subject.where("'pertambangan' = ANY (tags)").where.not(tahun_berlaku: "2018", khusus:"tambang eksplorasi").order(:semester).order(:matakuliah).order(:kategori)
        else
          # if @academic_year_active.semester == 'ganjil'
          #   @subjects = Subject.where("'pertambangan' = ANY (tags)", semester:["1","3","5","7"]).where.not(tahun_berlaku: "2018", khusus:"tambang umum").order(:semester).order(:matakuliah).order(:kategori)
          # elsif @academic_year_active.semester == 'genap'
          #   @subjects = Subject.where("'pertambangan' = ANY (tags)", semester:["2","4","6","8"]).where.not(tahun_berlaku: "2018", khusus:"tambang umum").order(:semester).order(:matakuliah).order(:kategori)
          # elsif @academic_year_active.semester == 'pendek'
          #   @subjects = Subject.where("'pertambangan' = ANY (tags)").where.not(tahun_berlaku: "2018", khusus:"tambang umum").order(:semester).order(:matakuliah).order(:kategori)
          # end
          @subjects = Subject.where("'pertambangan' = ANY (tags)").where.not(tahun_berlaku: "2018", khusus:"tambang umum").order(:semester).order(:matakuliah).order(:kategori)
        end
      end
    end
    @subject_plan = SubjectPlan.new
  end

  def create
    @subject_plan = SubjectPlan.new(subject_plans_params)
    if @subject_plan.save
      redirect_to @subject_plan, notice: 'KRS berhasil dibuat.'
    else
      flash[:danger] = 'KRS gagal disimpan'
      render :new
    end
  end

  def edit
    if current_user.is_student?
      @student = current_user.id
      @subjects = Subject.where("'#{SubjectPlan.tag_params_major(current_user.major.name)}' = ANY (tags)").order(:semester).includes(:subject_plans).order(:matakuliah).order(:kategori)
      if current_user.student_profile.angkatan.to_i >= 2018
        @subjects = @subjects.where.not(tahun_berlaku: "2014")
      else
        if current_user.student_profile.konsentrasi == "tambang umum"
          @subjects = @subjects.where.not(tahun_berlaku: "2018", khusus:"tambang eksplorasi")
        else
          @subjects = @subjects.where.not(tahun_berlaku: "2018", khusus:"tambang umum")
        end
      end
    elsif current_user.is_staff?
      @student = @subject_plan.student
      @subjects = Subject.where("'#{SubjectPlan.tag_params_major(@student.major.name)}' = ANY (tags)").order(:semester).order(:matakuliah).order(:kategori)
      if @student.student_profile.angkatan.to_i >= 2018
        @subjects = @subjects.where.not(tahun_berlaku: "2014")
      else
        if @student.student_profile.konsentrasi == "tambang umum"
          @subjects = @subjects.where.not(tahun_berlaku: "2018", khusus:"tambang eksplorasi")
        else
          @subjects = @subjects.where.not(tahun_berlaku: "2018", khusus:"tambang umum")
        end
      end
    end
  end

  def update
    respond_to do |format|
      if @subject_plan.update!(subject_plans_params)
        format.html {redirect_to subject_plan_path(@subject_plan), notice: "KRS berhasil diupdate"}
      else
        flash[:danger] = 'KRS gagal disimpan'
        format.html {render :edit}
      end
    end
  end

  def show
    @academic_year = AcademicYear.where(is_active: true).first
    @subject_plan = SubjectPlan.find(params[:id])
    respond_to do |format|
      format.html
    end
  end

  def print
    respond_to do |format|
      @subject_plan = SubjectPlan.find(params[:id])
      @subjects = @subject_plan.subjects.order(:semester).order(:matakuliah)
      if @subject_plan.save
        @subject_plan.update(is_printed: true)
        format.html
        format.pdf do
          render pdf: "KRS #{@subject_plan.student.student_profile.nama} #{@subject_plan.student.student_profile.nomor_induk_mahasiswa}",
          template: "subject_plans/print_pdf.html.erb",
          layout: 'pdf.html',
          locals: {:subjects => @subjects},
          page_size: 'A4'
        end
      end
    end
  end

  def print_sp
    respond_to do |format|
      @subject_plan = SubjectPlan.find(params[:id])
      @subjects = @subject_plan.subjects.order(:semester).order(:matakuliah)
      if @subject_plan.save
        @subject_plan.update(is_printed: true)
        format.html
        format.pdf do
          render pdf: "KRS #{@subject_plan.student.student_profile.nama} #{@subject_plan.student.student_profile.nomor_induk_mahasiswa}",
          template: "subject_plans/print_sp_pdf.html.erb",
          layout: 'pdf.html',
          locals: {:subjects => @subjects},
          page_size: 'A4'
        end
      end
    end
  end

  def destroy
    @subject_plan.destroy
    redirect_to subject_plans_path, notice: "Data KRS berhasil dihapus"
    respond_to do |format|
      format.html
    end
  end

  def approve_one
    @student = @subject_plan.student
    @student.regenerate_access_token
    if @subject_plan.is_approved == false
      @subject_plan.update(is_approved: true)
      SubjectPlanMailer.send_token_payment(@student).deliver_now
      redirect_to subject_plans_path, notice: "Data KRS diterima"

    elsif @subject_plan.is_approved == true
      @subject_plan.update(is_approved: false)
      redirect_to subject_plans_path, alert: "Data KRS ditolak"
    end

  end

  def set_approve
    if params[:commit] == "ACC"
      @subject_plans = SubjectPlan.where(id: params[:subject_plan_id]).update_all(is_approved: true)
      redirect_to subject_plans_path, notice: "Data KRS diterima"
    elsif params[:commit] == "Tidak ACC"
      @subject_plans = SubjectPlan.where(id: params[:subject_plan_id]).update_all(is_approved: false)
      redirect_to subject_plans_path, alert: "Data KRS ditolak"
    end
  end

  private
  def check_subject_plan_present
    if current_user.subject_plans.length > 0
      redirect_to root_url, notice: "Maaf, anda telah membuat KRS semester ini"
    end
  end

  def set_semester_active
    @academic_year_active = AcademicYear.where(is_active: true).first
  end

  def set_subject_plan
    @subject_plan = SubjectPlan.find(params[:id])
  end
  def student
    @student = current_user
  end
  def subject_plans_params
    #  academic_year_id            :integer
    #  student_id                  :bigint(8)
    #  is_approved                 :boolean          default(FALSE)
    #  is_passed                   :boolean          default(FALSE)
    #  is_printed                  :boolean          default(FALSE)
    #  stuctural_id                :integer
    #  last_grade_poin_average     :float
    #  created_at                  :datetime         not null
    #  updated_at                  :datetime         not null
    #  penasehat_akademik          :string
    #  nip_nidn_penasehat_akademik :string
    #  dekan                       :string
    #  nip_nidn_dekan              :string

    params.require(:subject_plan).permit(
      :academic_year_id,
      :student_id,
      :is_approved,
      :is_passed,
      :is_printed,
      :last_grade_poin_average,
      :penasehat_akademik,
      :nip_nidn_penasehat_akademik,
      :dekan,
      :nip_nidn_dekan,
      :subject_plan_file,
      subject_ids: [])
  end
end
