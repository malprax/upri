class Students::BaseController < ApplicationController
  before_action :authenticate_user!
  before_action :checking_student!
  before_action :checking_profile!
  private

  def checking_student!
    redirect_to send("#{user_base_url.pluralize}_dashboards_path"), flash: { error: 'Anda tidak memiliki akses ke halaman ini' } if !current_user.is_student?
  end

  def checking_profile!
    redirect_to edit_student_path(current_user), flash: { error: 'Mohon adik isi profil dan upload dokumennya terlebih dahulu' } unless current_user.is_active?
  end
end
