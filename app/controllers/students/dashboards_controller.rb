class Students::DashboardsController < Students::BaseController
  def index
    @campus_newspapers = CampusNewspaper.where(terbit: true).order("created_at desc" )
  end

end
