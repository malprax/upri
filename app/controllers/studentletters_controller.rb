class StudentlettersController < ApplicationController
  before_action :set_studentletter, only: [:show, :edit, :update, :destroy]

  # GET /studentletters
  # GET /studentletters.json
  def index_surat
    @studentletters = Studentletter.order(:created_at)
  end
  
  def index
    @studentletters = Studentletter.all
  end

  # GET /studentletters/1
  # GET /studentletters/1.json
  def show
  end

  # GET /studentletters/new
  def new
    @studentletter = Studentletter.new
  end

  # GET /studentletters/1/edit
  def edit
  end

  # POST /studentletters
  # POST /studentletters.json
  def create
    @studentletter = Studentletter.new(studentletter_params)

    respond_to do |format|
      if @studentletter.save
        format.html { redirect_to studentletters_path, notice: 'Studentletter was successfully created.' }
        format.json { render :show, status: :created, location: @studentletter }
      else
        format.html { render :new }
        format.json { render json: @studentletter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /studentletters/1
  # PATCH/PUT /studentletters/1.json
  def update
    respond_to do |format|
      if @studentletter.update(studentletter_params)
        format.html { redirect_to studentletters_path, notice: 'Studentletter was successfully updated.' }
        format.json { render :show, status: :ok, location: @studentletter }
      else
        format.html { render :edit }
        format.json { render json: @studentletter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /studentletters/1
  # DELETE /studentletters/1.json
  def destroy
    @studentletter.destroy
    respond_to do |format|
      format.html { redirect_to studentletters_url, notice: 'Studentletter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_studentletter
      @studentletter = Studentletter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def studentletter_params
      params.require(:studentletter).permit(
        :nama,
        :email,
        :nomor_induk_mahasiswa,
        :program_studi,
        :tempat_lahir,
        :tanggal_lahir,
        :alamat,
        :telepon,
        :handphone,
        :tanggal_yudisium,
        :judul_skripsi,
        :nama_orang_tua,
        :tempat_kerja,
        :pangkat_golongan,
        :kategori_nomor_induk_instansi,
        :nomor_induk_instansi,
        :telepon_orangtua,
        :jenis_Studentletter,
        :keterangan_pindah,
        :diketahui_kepala_tata_usaha,
        :tanggal_diketahui_kepala_tata_usaha,
        :kepala_tata_usaha,
        :nomor_Studentletter,
        :nomor_ijazah_fakultas,
        :nomor_ijazah_universitas,
        :kepada,
        :casu_quo_kepada,
        :jenis_Studentletter_lainnya,
        :foto_ijazah_data,
        :foto_bukti_pembayaran_semester_berjalan_data
      )
    end
end
