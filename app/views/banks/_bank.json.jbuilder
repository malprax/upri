json.extract! bank, :id, :name, :account_name, :account_number, :code, :created_at, :updated_at
json.url bank_url(bank, format: :json)
