json.extract! item_accreditation, :id, :item, :academic_year_id, :created_at, :updated_at
json.url item_accreditation_url(item_accreditation, format: :json)
