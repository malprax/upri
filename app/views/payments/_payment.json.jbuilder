json.extract! payment, :id, :kind, :academic_year_id, :in_number, :in_word, :student_id, :bank_id, :is_approve, :created_at, :updated_at
json.url payment_url(payment, format: :json)
