require "image_processing/mini_magick"
class PaymentFormUploader < Shrine
  ALLOWED_TYPES = %w[image/jpeg image/png image/jpg]
  include ImageProcessing::MiniMagick #for create thumb

  plugin :determine_mime_type #determine mime type
  plugin :remove_attachment #remove_attachment image
  plugin :cached_attachment_data #for cached_image_data
  plugin :store_dimensions #can edit dimension
  plugin :pretty_location
  plugin :processing #for create thumb
  plugin :versions #for create thumb
  plugin :activerecord
  plugin :validation_helpers #validation

  process(:store) do |io, context|
    original = io.download
    thumbnail = ImageProcessing::MiniMagick
      .source(original)
      .resize_to_limit!(600, nil)
    original.close!
    { original: io, thumbnail: thumbnail }
  end

  Attacher.validate do
    validate_max_size 300.kilobyte, message: "is too large (max is 500 KB)"
    validate_mime_type_inclusion(ALLOWED_TYPES)
  end
end
