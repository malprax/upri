module ApplicationHelper
  def check_semester
    if current_academic_year.semester == "Pendek"
      8
    else
      21
    end
  end
  def active_storage_file(file, default, size, dispotition)
    if file.attached?
      if file.representable?
        link_to image_tag(file.representation(resize: size)), rails_blob_path(file, disposition: dispotition)
      end
    else
      content_tag :img,'',src: default
    end
  end

  def active_storage_file_payment(file, user, payment, size, dispotition)
    if file.attached?
      if file.representable?
        link_to image_tag(file.representation(resize: size)), rails_blob_path(file, disposition: dispotition)
      end
    else
      link_to icon_upload(''), edit_student_payment_path(user, payment, type:"upload_#{payment.kind}")
    end
  end

  def active_storage_file_subject_plan(file, user, payment, size, dispotition)
    if file.attached?
      if file.representable?
        link_to image_tag(file.representation(resize: size)), rails_blob_path(file, disposition: dispotition)
      end
    else
      link_to icon_upload(''), edit_subject_plan_path(user, type:"upload_krs")
    end
  end

  def complete_profile(nama, jenis_kelamin, religion, handphone, kewarganegaraan, major, tempat_lahir, tanggal_lahir, nama_sekolah, jurusan_sekolah)
    if nama.present? && jenis_kelamin.present? && religion.present? && handphone.present? && kewarganegaraan.present? && major.present? && tempat_lahir.present? && tanggal_lahir.present? && nama_sekolah.present? && jurusan_sekolah.present?
      true
    else
      false
    end
  end

  def complete_document(student_photo_file, kartu_keluarga_file, kartu_tanda_penduduk_file, ijazah_file)
    if student_photo_file.present? && kartu_keluarga_file.present? && kartu_tanda_penduduk_file.present? && ijazah_file.present?
      true
    else
      false
    end
  end

  def sortable_ajax(column, title = nil)
    title ||= column.titleize
    direction = column == sort_column && sort_direction == "asc" ? 'desc' : 'asc'
    link = {:sort => column, :direction => direction}
    link_to "<i class='fas fa-sort'></i> #{title}".html_safe, link, id: 'text-link', remote: true
    # <i class="fas fa-sort-amount-up"></i>
    # <i class="fas fa-sort-amount-down"></i>
  end

  def sortable(column, title = nil)
    title ||= column.titleize
    direction = column == sort_column && sort_direction == "asc" ? 'desc' : 'asc'
    link = {:sort => column, :direction => direction}
    link_to "<i class='fas fa-sort'></i> #{title}".html_safe, link, id: 'text-link'
    # <i class="fas fa-sort-amount-up"></i>
    # <i class="fas fa-sort-amount-down"></i>
  end

  def status_active(status)
    if status == true
      "has-text-link"
    end
  end

  def checklist_present(table)
    if table.present?
      icon_check('')
    else
      icon_times('')
    end
  end
  def address_format(jalan, rt, rw, kelurahan, kecamatan, kota)
    if !rt.present?
      ("#{jalan}\n\rRW\r#{rw}\n\rKelurahan\r#{kelurahan}\n\rKecamatan\r#{kecamatan}\n\rKota\r#{kota}").gsub("\r","&nbsp;").gsub("\n",",").html_safe
    elsif !rw.present?
      ("#{jalan}\n\rRT\r#{rt}\n\rKelurahan\r#{kelurahan}\n\rKecamatan\r#{kecamatan}\n\rKota\r#{kota}").gsub("\r","&nbsp;").gsub("\n",",").html_safe
    elsif !kelurahan.present?
      ("#{jalan}\n\rRT\r#{rt}\n\rRW\r#{rw}\n\rKelurahan\r#{kelurahan}\n\rKecamatan\r#{kecamatan}\n\rKota\r#{kota}").gsub("\r","&nbsp;").gsub("\n",",").html_safe
    elsif !kecamatan.present?
      ("#{jalan}\n\rRT\r#{rt}\n\rRW\r#{rw}\n\rKelurahan\r#{kelurahan}\n\rKecamatan\r#{kecamatan}\n\rKota\r#{kota}").gsub("\r","&nbsp;").gsub("\n",",").html_safe
    elsif !kota.present?
      ("#{jalan}\n\rRT\r#{rt},\n\rRW\r#{rw}\n\rKelurahan\r#{kelurahan}\n\rKecamatan\r#{kecamatan}\n\rKota\r#{kota}").gsub("\r","&nbsp;").gsub("\n",",").html_safe
    elsif !rt.present? && !rw.present?
      ("#{jalan}\n\rKelurahan\r#{kelurahan}\n\rKecamatan\r#{kecamatan}\n\rKota\r#{kota}").gsub("\r","&nbsp;").gsub("\n",",").html_safe
    elsif !rt.present? && !rw.present? && !kelurahan.present?
      ("#{jalan}\n\rKecamatan\r#{kecamatan}\n\rKota\r#{kota}").gsub("\r","&nbsp;").gsub("\n",",").html_safe
    elsif !rt.present? && !rw.present? && !kelurahan.present? && !kecamatan.present?
      ("#{jalan}\n\rKota\r#{kota}").gsub("\r","&nbsp;").gsub("\n",",").html_safe
    elsif !rt.present? && !rw.present? && !kelurahan.present? && !kecamatan.present? && !kota.present?
      "#{jalan}"
    else
      ("#{jalan}\n\rRT\r#{rt}\n\rRW\r#{rw}\n\rKelurahan\r#{kelurahan}\n\rKecamatan\r#{kecamatan}\n\rKota\r#{kota}").gsub("\r", "&nbsp;").gsub("\n",",").html_safe
    end
  end
  def lecture_name(gelar_depan, nama, gelar_belakang)
    if gelar_depan.present?
      "#{gelar_depan}\n\r#{nama}\n\r#{gelar_belakang}"
    else
      "#{nama}\n\r#{gelar_belakang}"
    end
  end
  def info_profile
    if current_user.is_student? && current_user.student_profile.present?
      @info_profile = current_user.student_profile.nama
    elsif  current_user.is_staff? &&  current_user.staff_profile.present?
      @info_profile = current_user.staff_profile.nama
    elsif current_user.is_lecture? &&  current_user.lecture_profile.present?
      @info_profile = current_user.lecture_profile.nama
    else
      @info_profile = current_user.email
    end
  end

  def select_for_checking_the_address_notes(keterangan_tinggal)
    case keterangan_tinggal
    when "Kedua Orang Tua"
      "Kedua Orang Tua"
    when "Ayah"
      "Ayah"
    when "Ibu"
      "Ibu"
    when "Wali"
      "Wali"
    when "Sendiri"
      "Sendiri"
    end
  end

  def select_parents(keterangan_tinggal)
    case keterangan_tinggal
    when "Kedua Orang Tua"
      "Ayah"
    when "Ayah"
      "Ayah"
    when "Ibu"
      "Ibu"
    when "Wali"
      "Wali"
    end
  end

  def checking_parents_name(student, keterangan_tinggal)
    case keterangan_tinggal
    when "Kedua Orang Tua"
      student.parent.nama_ayah.titleize
    when "Ayah"
      student.parent.nama_ayah.titleize
    when "Ibu"
      student.parent.nama_ibu.titleize
    when "Wali"
      student.parent.nama_wali.titleize
    end
  end

  def checking_parents_job(student, keterangan_tinggal)
    case keterangan_tinggal
    when "Kedua Orang Tua"
      student.parent.pekerjaan_ayah
    when "Ayah"
      student.parent.pekerjaan_ayah
    when "Ibu"
      student.parent.pekerjaan_ibu
    when "Wali"
      student.parent.pekerjaan_wali
    end
  end

  def krs_status(object)
    if object.is_approved == true
      "SUDAH DIPERIKSA DAN DIBETULKAN OLEH BAGIAN AKADEMIK"
    elsif object.is_approved == false
      "BELUM DIPERIKSA OLEH BAGIAN AKADEMIK"
    end
  end

  def check_approve_with_link(object, link, method)
    if object.is_approved == true
      link_to link, class:'button is-small is-success',method: method, data:{confirm: "Yakin ubah approve?"} do
        icon_check('')
      end

    elsif object.is_approved == false
      link_to link, class:'button is-small is-danger',method: method, data:{confirm: "Yakin ubah approve?"} do
        icon_times('')
      end
    end
  end

  def check_approve(object)
    if object.is_approved == true
        icon_check('')
    elsif object.is_approved == false
        icon_times('')
    end
  end

  def check_approve_status_student(object)
    if object.is_approved == true
        icon_check('')
    elsif object.is_approved == false
        icon_times('')
    end
  end

  def check_active_with_link(object, link, method)
    if object.is_active == true
      link_to link, class:'button is-small is-success',method: method, data:{confirm: "Yakin ubah data?"} do
        icon_check('')
      end

    elsif object.is_active== false
      link_to link, class:'button is-small is-danger',method: method, data:{confirm: "Yakin ubah data?"} do
        icon_times('')
      end
    end
  end

  def check_active_status(object)
    if object.is_active == true
      icon_check('')
    elsif object.is_active == false
      icon_times('')
    end
  end

  # set resource for image
  def set_image_resource(resource, size = "113x151", text = "Captured Image")
    if resource.image_data?
      resource.image_url
    else
      "https://placehold.it/#{size}?text=#{}"
    end
  end

  def set_identity_resource(resource, size = "113x151", text = "Captured Image")
    if resource.identity.file_identity_data?
      resource.identity.file_identity_url
    else
     "https://placehold.it/#{size}?text=#{}"
    end
  end

  def set_blangko_resource(resource, size = "350x250", text = "Captured Image")
    if resource.blangko_data?
      resource.blangko_url
    else
     "https://placehold.it/#{size}?text=#{}"
    end
  end

  def set_thumb_resource(resource, size = "72x100", text = "Captured Image")
    if resource.image_data?
      resource.image_url
    else
     "https://placehold.it/#{size}?text=#{}"
    end
  end

  def set_book_resource(resource, size = "72x100", text = "Captured Image")
    if resource.cover_buku_data?
      resource.cover_buku_url.to_s
    else
     "https://placehold.it/#{size}?text=#{}"
    end
  end

  def set_skripsi_resource(resource, size = "72x100", text = "Captured Image")
    if resource.cover_skripsi_data?
      resource.cover_skripsi_url
    else
     "https://placehold.it/#{size}?text=#{}"
    end
  end

  def set_skripsi_show_resource(resource, size = "216x300", text = "Captured Image")
    if resource.cover_skripsi_data?
      resource.cover_skripsi_url
    else
     "https://placehold.it/#{size}?text=#{}"
    end
  end

  def set_book_show_resource(resource, size = "144x200", text = "Captured Image")
    if resource.cover_buku_data?
      resource.cover_buku_url.to_s
    else
     "https://placehold.it/#{size}?text=#{}"
    end
  end

  def icon_edit(value)
   content_tag(:i, value, :class => "fas fa-edit")
  end

  def icon_delete(value)
   content_tag(:i, value, :class => "fas fa-trash-alt")
  end

  def icon_print(value)
   content_tag(:i, value, :class => "fas fa-print")
  end

  def icon_print_button(value)
   content_tag(:i, value, :class => "fas fa-print")
  end

  def icon_check(value)
   content_tag(:i, value, :class => "fas fa-check is-success")
  end

  def icon_times(value)
   content_tag(:i, value, :class => "fas fa-times is-danger")
  end

  def icon_lock(value)
   content_tag(:i, value, :class => "fas fa-lock")
  end

  def icon_plane(value)
    content_tag(:i, value, :class => "fas fa-paper-plane-o")
  end

  def icon_out(value)
    content_tag(:i, value, :class => "fa fa-sign-out")
  end

  def icon_plus(value)
    content_tag(:i, value, :class => "fa fa-plus-square")
  end

  def icon_graduate(value)
    content_tag(:i, value, :class => "fa fa-graduation-cap")
  end

  def icon_save(value)
    content_tag(:i, value, :class => "far fa-save")
  end

  def icon_excel(value)
    content_tag(:i, value, :class => "fas fa-file-excel")
  end

  def icon_word(value)
    content_tag(:i, value, :class => "fas fa-file-word")
  end

  def icon_file(value)
    content_tag(:i, value, :class => "fas fa-file-alt")
  end
  def icon_download(value)
    content_tag(:i, value, :class => "fas fa-download")
  end

  def icon_upload(value)
    content_tag(:i, value, :class => "fas fa-upload")
  end

  def icon_pdf(value)
    content_tag(:i, value, :class => "fas fa-file-pdf")
  end

  def icon_kuliah(value)
    content_tag(:i, value, :class => "fas fa-book")
  end

  def icon_search(value)
    content_tag(:i, value, :class => "fas fa-search")
  end

  def options_for_faculties
    Faculty.all.map {|faculty| [faculty.name, faculty.id]}
  end

  def is_active_controller(controller_name, class_name = nil)
    # if params[:controller] == controller_name
    #   class_name == nil ? "is-active is-tab" : class_name
    # else
    #   nil
    # end
    params[:controller] == controller_name ? "is-active is-tab" : class_name
  end

  def is_active_action(action_name)
    params[:action] == action_name ? "is-active" : nil
  end

  def avatar_url(user)
    "https://www.gravatar.com/avatar/#{Digest::MD5::hexdigest(user.email).downcase}.jpg?id=identicon&s=50"
  end

  def upload_server
    if Rails.env.production?
      :s3
    else
      :app
    end
  end

end
