class PromoteJob < ApplicationJob
  include SuckerPunch::Job

  def perform(data)
    Shrine::Attacher.promote(data)
  end
end
