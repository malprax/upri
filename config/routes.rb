Rails.application.routes.draw do
  resources :journals do
    member  do
      delete :delete_journal_file_attachment
    end
    resources :articles
  end
  resources :banks
  resources :payments do
    collection do
      put :set_approve
    end
    member do
      get :approve
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :subject_plans do
    member do
      put :approve_one
      put :print
      get :print_sp
    end
    collection do
      put :set_approve
    end
  end
  get 'tahun_akademik', to: 'academics#tahun_akademik', as: :tahun_akademik
  resources :academics
  resources :candidate_students
  resources :item_accreditations
  resources :authenticate_phones, only: [:new, :create] do |p|
    collection do
      get 'verifikasi'
      post 'konfirmasi'
    end
  end

  get 'index_skripsi', to: 'essays#index_skripsi', as: :index_skripsi
  resources :essays

  get 'index_buku', to: 'book_collections#index_buku', as: :index_buku
  resources :book_collections

  get 'index_surat', to: 'studentletters#index_surat', as: :index_surat
  resources :studentletters
  resources :jenjang_didiks

  get 'kabar_teknik', to: 'campus_newspapers#kabar_teknik', as: :kabar_teknik
  resources :campus_newspapers
  resources :landing_pages, only: [:index]



  resources :phone_verifications, only: [:new, :create] do |p|
    collection do
      get 'verifikasi'
      post 'konfirmasi'
    end
  end

  # resources :mahasiswas do
  #   collection do
  #     put :
  #   end
  # end


  get 'daftar_calon_mahasiswa', to: 'dashboards#daftar_calon_mahasiswa', as: "daftar_calon_mahasiswa"

  get 'buat_semester_genap', to: 'academic_years#buat_semester_genap', as: "buat_semester_genap"
  get 'buat_semester_ganjil', to: 'academic_years#buat_semester_ganjil', as: "buat_semester_ganjil"

  get 'token_session', :to => "sessions#new"
  post 'token_session', :to => "sessions#create"
  get 'token_session_out', :to => "sessions#destroy"



  get 'token', to: 'graduates#token', as: "token"
  get 'wisudawan', to: 'graduates#wisudawan', as: "wisudawan"
  get 'semua', to: 'graduates#semua', as: "semua"
  get 'galeri', to: 'graduates#galeri', as: "galeri"
  get 'buku', to: 'graduates#buku', as: "buku"

  get 'index_dosen', to: 'dummy_lectures#index_dosen', as: "index_dosen"
  resources :dummy_lectures
  resources :academic_years

  get 'jadwal_kuliah', to: 'pages#jadwal_kuliah', as: "jadwal_kuliah"
  resources :schedule_subjects

  resources :subjects

  get 'info', to: 'pages#info', as: "info"

  get 'kalendar_akademik', to: 'pages#kalendar_akademik', as: "kalendar_akademik"
  get 'daftar', to: 'pages#daftar', as: "daftar"

  get 'akreditasi', to: 'item_accreditations#index', as: "akreditasi"

  concern :documentable do
    resources :documents, only: [:new, :create, :edit, :update, :destroy]
  end
  concern :dashboardtable do
    # authenticated :user do
    #   root to: 'dashboards#index'#, as: :authenticated_root
      resources :dashboards, only: [:index]
    # end
  end

  namespace :juniors do
    concerns :dashboardtable
  end

  namespace :staffs do
    concerns :dashboardtable
  end

  namespace :students do
    concerns :dashboardtable
  end

  namespace :lectures do
    concerns :dashboardtable
  end

  namespace :admins do
    concerns :dashboardtable
  end

  resources :lectures, only:[:index, :show, :edit, :update, :destroy] do
    member do
      get :profile
      get :educations
      get :training_professionals
      get :teachings
      get :materials
      get :researchs
      get :scientifics
      get :papers
      get :reviewers
      get :conferences
      get :professionals
      get :positions
      get :student_organizations
      get :scientific_organizations
      get :appreciations
      put :print
    end
      resources :lecture_education_histories, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :professional_trainings, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_teaching_histories, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_teaching_materials, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_researches, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_scientific_works, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_papers, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_reviewers, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_conferences, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_position_histories, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_professional_activities, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_student_organization_histories, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_appreciations, only:[:index, :new, :create, :edit, :update, :destroy]
      resources :lecture_scientist_organizations, only:[:index, :new, :create, :edit, :update, :destroy]
  end

  resources :students, only:[:index, :show, :edit, :update, :destroy], concerns: :documentable do
    member do
      get :edit_formulir
      patch :make
      get :delete_file
      # p :print
      get :print_blanko
      get :print_formulir
      put :active_one
    end
    collection do
      get :studyplans
      put :active_all
    end

    resources :payments, only:[:index, :new, :create, :show, :edit, :update, :destroy] do
      member do
        get :print
        get :print_registrasi
        get :letter
      end
    end
  end

  resources :staffs , only:[:index, :show, :new, :create,  :edit, :update, :destroy ] do
    member do
      get :profile
    end
  end


  resources :lecture_lists
  resources :graduates do
    collection  do
      put :update_all
      post :import
    end
    member do
      get :download
      get :send_token_email
      get :formulir_wisuda
      get :edit_formulir
      patch :update_access_token, to:'graduates#update_access_token'
    end
  end

  resources :mahasiswas do
    member do
      get 'biodata', to: 'mahasiswas#biodata', as: :biodata
      get 'edit_biodata', to: 'mahasiswas#edit_biodata', as: :edit_biodata
    end
    collection do
      put :activate
    end
    resources :documents

  end

  get 'index_mahasiswa', to: 'mahasiswa_profiles#index_mahasiswa', as: :index_mahasiswa
  resources :mahasiswa_profiles

  root to: 'pages#index'

  devise_for :users, controllers: {
        passwords: 'users/passwords',
        sessions: 'users/sessions',
        registrations: 'users/registrations',
        confirmations: 'users/confirmations'}

  if Rails.env.production?
    mount Shrine.presign_endpoint(:cache) => "/presign"
  else
    # In development and test environment we're using filesystem storage
    # for speed, so on the client side we'll upload files to our app.
    mount Shrine.upload_endpoint(:cache) => "/upload"
  end
end
