SimpleForm.setup do |config|
  #config.generate_additional_classes_for = ['field']''
  config.button_class = 'button is-primary'

  config.wrappers :default, tag: 'div', class: '', error_class: 'has-error' do |b|
    b.use         :placeholder
    b.optional    :maxlength
    b.optional    :pattern
    b.optional    :min_max
    b.optional    :readonly

    b.wrapper tag: 'div', class: 'field' do |ba|
      ba.use :input, class: 'input'
      ba.optional :hint, wrap_with: { tag: 'p', class: 'help' }
      ba.optional :label, class: 'label', wrap_with: { tag: 'label', class: 'label' }
    end

    #
    #   %label.checkbox
    #     %input{:type => "checkbox"}/
    #     I agree to the
    #     %a{:href => "#"} terms and conditions
    #
    #

  end

  config.wrappers :boolean, tag: 'div', class: 'control', error_class: 'has-error' do |b|
    b.wrapper :label, tag: "label", class: "checkbox" do |ba|
      ba.use :input, class: 'checkbox'
      ba.use :label_text
      ba.use :hint, wrap_with: {tag: 'p', class: 'help'}
    end
  end

  config.wrappers :inline_input, tag: "div", class: "field is-horizontal" do |b|
    b.wrapper :label, tag: "div", class: "field-label is-normal" do |bl|
      bl.optional :label, class: "label"
    end
    b.wrapper :input, tag: "div", class: "field-body" do |bi|
      bi.wrapper :input, tag: "div", class: "field" do |bf|
        bf.use :input, class: "input"
        bf.optional :hint, wrap_with: {tag: "p", class: "help"}
      end
    end
  end

  config.wrappers :email_bulma, tag: "div", class:"field", error_class: 'has-error' do |b|
    b.use :label, tag: 'label', class: 'label'
    b.wrapper :input, tag: 'div', class: 'control has-icons-right' do |bi|
      bi.use :input, class:"input is-danger", type: 'email', placeholder: 'Email input'
      bi.optional :span, wrap_with: {tag: 'span', class: 'icon is-small is-left'}
      bi.optional :hint, wrap_with: {tag: 'p', class: 'help is-danger'}
    end

  end

  config.wrappers :text_bulma, tag: "div", class:"field", error_class: 'has-error' do |b|
    b.use :label, tag: 'label', class: 'label'
    b.wrapper tag: 'div', class: 'control has-icons-right' do |bi|
      bi.use :placeholder
      bi.use :input, class:"input is-primary", type: 'text'
      bi.optional :icon_left, wrap_with: {tag: 'span', class: 'icon is-small is-left'}
      bi.optional :icon_right, wrap_with: {tag: 'span', class: 'icon is-small is-right'}
    end
  end

  config.wrappers :text_bulma_small, tag: "div", class:"field", error_class: 'has-error' do |b|
    b.use :label, tag: 'label', class: 'label has-text-weight-light'
    b.wrapper tag: 'div', class: 'control has-icons-right' do |bi|
      bi.use :placeholder
      bi.use :input, class:"input is-primary is-small", type: 'text'
      bi.optional :icon_left, wrap_with: {tag: 'span', class: 'icon is-small is-left'}
      bi.optional :icon_right, wrap_with: {tag: 'span', class: 'icon is-small is-right'}
    end
  end

  config.wrappers :text_hidden_bulma_small, tag: "div", class:"field is-hidden", error_class: 'has-error' do |b|
    b.use :label, tag: 'label', class: 'label has-text-weight-light'
    b.wrapper tag: 'div', class: 'control has-icons-right' do |bi|
      bi.use :placeholder
      bi.use :input, class:"input is-primary is-small", type: 'text'
      bi.optional :icon_left, wrap_with: {tag: 'span', class: 'icon is-small is-left'}
      bi.optional :icon_right, wrap_with: {tag: 'span', class: 'icon is-small is-right'}
    end
  end

  config.wrappers :password_bulma, tag: "div", class:"field", error_class: 'has-error' do |b|
    b.use :label, tag: 'label', class: 'label'
    b.wrapper :input, tag: 'div', class: 'control has-icons-right' do |bi|
      bi.use :input, class:"input is-success", type: 'password', placeholder: '--Password--'
      bi.optional :span, wrap_with: {tag: 'span', class: 'icon is-small is-left'}
      bi.optional :hint, wrap_with: {tag: 'p', class: 'help is-danger'}
    end

  end

  config.wrappers :password_bulma_small, tag: "div", class:"field", error_class: 'has-error' do |b|
    b.use :label, tag: 'label', class: 'label'
    b.wrapper :input, tag: 'div', class: 'control has-icons-right' do |bi|
      bi.use :input, class:"input is-success is-small", type: 'password', placeholder: '--Password--'
      bi.optional :span, wrap_with: {tag: 'span', class: 'icon is-small is-left'}
      bi.optional :hint, wrap_with: {tag: 'p', class: 'help is-danger'}
    end

  end

  config.wrappers :password_confirmation_bulma, tag: "div", class:"field", error_class: 'has-error' do |b|
    b.use :label, tag: 'label', class: 'label'
    b.wrapper :input, tag: 'div', class: 'control has-icons-right' do |bi|
      bi.use :input, class:"input is-success is-small", type: 'password', placeholder: '--Ulangi Password--'
      bi.optional :span, wrap_with: {tag: 'span', class: 'icon is-small is-left'}
      bi.optional :hint, wrap_with: {tag: 'p', class: 'help is-danger'}
    end

  end

  config.wrappers :password_confirmation_bulma_small, tag: "div", class:"field", error_class: 'has-error' do |b|
    b.use :label, tag: 'label', class: 'label'
    b.wrapper :input, tag: 'div', class: 'control has-icons-right' do |bi|
      bi.use :input, class:"input is-success is-small", type: 'password', placeholder: '--Ulangi Password--'
      bi.optional :span, wrap_with: {tag: 'span', class: 'icon is-small is-left'}
      bi.optional :hint, wrap_with: {tag: 'p', class: 'help is-danger'}
    end

  end

  config.wrappers :select_bulma, tag: 'div', class: 'field is-fullwidth', error_class: 'has-error' do |b|
    b.wrapper :input, tag: 'div', class:'control' do |bi|
      bi.use :placeholder
      bi.use :input, tag: 'div', class:"select is-primary"
    end
  end

  config.wrappers :date_bulma, tag: 'div', class: 'field date is-fullwidth', error_class: 'has-error' do |b|
    b.wrapper :input, tag: 'div', class:'control' do |bi|
      bi.use :placeholder
      bi.use :input, tag: 'input ', class:"input date optional is-small is-primary", type: "date"
    end
  end

  config.wrappers :select_bulma_small, tag: 'div', class: 'field is-fullwidth select is-small is-primary', error_class: 'has-error' do |b|
    b.wrapper :input, tag: 'div', class:'control' do |bi|
      bi.use :placeholder
      bi.use :input, tag: 'div', class:"is-primary is-small"
    end
  end

  config.wrappers :select_hidden_bulma_small, tag: 'div', class: 'field is-hidden is-fullwidth is-small is-primary ' do |b|
    b.wrapper :input, tag: 'div', class:'control ' do |bi|
      bi.use :placeholder
      bi.use :input, tag: 'div', class:"select optional is-primary is-small"
    end
  end

  config.wrappers :group_select_bulma_small, tag: 'div', class: 'field is-fullwidth ', error_class: 'has-error' do |b|
    b.wrapper :input, tag: 'div', class:'control select is-fullwidth is-small is-primary' do |bi|
      bi.use :placeholder
      bi.use :input, tag: 'div', class:"is-small select"
    end
  end

  config.wrappers :file_bulma, tag: 'div', class: 'field', error_class: 'has-error' do |b|
    b.wrapper :input, tag: 'div', class:'file is-boxed' do |bi|
      bi.use :placeholder
      bi.use :input, tag: 'div', class:""
    end
  end

  config.wrappers :file_bulma_small, tag: 'div', class: 'field', error_class: 'has-error' do |b|
    b.wrapper :input, tag: 'div', class:'file is-small is-boxed' do |bi|
      bi.use :placeholder
      bi.use :input, tag: 'div', class:"is-small"
    end
  end

  1.upto(12) do |col|
    config.wrappers "select_plan#{col}", tag: "div", class: "field" do |b|
      b.optional :label, class: "label is-small "
      b.wrapper :input, tag: "div", class: "control is-#{col} is-expanded is-small" do |bi|
        b.use :input, tag: "div", class: "select is-small is-fullwidth"
      end
    end
    config.wrappers "input_plan#{col}", tag: "div", class: "field is-#{col}" do |b|
      b.use :placeholder
      b.wrapper :input, tag: "div", class: "control is-expanded is-fullwidth" do |bi|
        # bi.use :input, class: "input", type: "text", class: "is-small"
      end
    end
  end

  # <div class="field">
  #   <div class="file is-small is-boxed">
  #     <label class="file-label">
  #       <input class="file-input" type="file" name="resume">
  #       <span class="file-cta">
  #         <span class="file-icon">
  #           <i class="fas fa-upload"></i>
  #         </span>
  #         <span class="file-label">
  #           Small file…
  #         </span>
  #       </span>
  #     </label>
  #   </div>
  # </div>

  class StringInput < SimpleForm::Inputs::StringInput
    def input(wrapper_options)
      template.content_tag(:div, super, class: 'control')
    end
  end

  class TextInput < SimpleForm::Inputs::TextInput
    def input_html_classes
      super.push('textarea')
    end
  end

  class CurrencyInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    currency = options.delete(:currency) || default_currency
    merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)

    content_tag(:div, input_group(currency, merged_input_options), class: "input-group")
  end

  private

  def input_group(currency, merged_input_options)
    "#{currency_addon(currency)} #{@builder.text_field(attribute_name, merged_input_options)}".html_safe
  end

  def currency_addon(currency)
    content_tag(:span, currency, class: "input-group-addon")
  end

  def default_currency
    "Rp"
  end
end


end
