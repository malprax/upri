Recaptcha.configure do |config|
  if Rails.env.production?
    config.site_key  = ENV["GOOGLE_RECAPTCHA_SITE_KEY"]
    config.secret_key = ENV["GOOGLE_RECAPTCHA_SECRET_KEY"]
  else
    config.site_key  = Rails.application.credentials.google_recaptcha[:site_key]
     config.secret_key = Rails.application.credentials.google_recaptcha[:secret_key]
  end


end
