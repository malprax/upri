if Rails.env.production?
  Authy.api_key = ENV["AUTHY_API_KEY"]
else
  Authy.api_key = Rails.application.credentials.authy[:api_key]
end
Authy.api_uri = 'https://api.authy.com/'
