require "shrine"
# if Rails.env.production?
  require "shrine/storage/s3"

  if Rails.env.production?
    s3_options = {
      access_key_id:     ENV["AWS_ACCESS_KEY_ID"],
      secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"],
      region:            ENV["AWS_REGION"],
      bucket:            ENV["AWS_BUCKET"]
    }
    Shrine.storages = {
      cache: Shrine::Storage::S3.new(prefix: "cache", **s3_options),
      store: Shrine::Storage::S3.new(**s3_options),
    }
  else
    s3_options = {
      access_key_id:     Rails.application.credentials.aws_production[:s3_access_key_id],
      secret_access_key: Rails.application.credentials.aws_production[:s3_secret_access_key],
      region:            Rails.application.credentials.aws_production[:s3_region],
      bucket:            Rails.application.credentials.aws_production[:s3_bucket]
    }

    Shrine.storages = {
      cache: Shrine::Storage::S3.new(prefix: "cache", **s3_options),
      store: Shrine::Storage::S3.new(**s3_options),
    }
  end
# else
#   require "shrine/storage/file_system"
#
#   Shrine.storages = {
#     cache: Shrine::Storage::FileSystem.new("public", prefix: "assets/images/cache"),
#     store: Shrine::Storage::FileSystem.new("public", prefix: "assets/images"),
#   }
# end

#Shrine.plugin :sequel # load integration for the Sequel ORM
Shrine.plugin :backgrounding
# Shrine.plugin :determine_mime_type
# Shrine.plugin :cached_attachment_data #for forms
# Shrine.plugin :restore_cached_data # refresh metadata when attaching the cached file
Shrine.plugin :activerecord
Shrine.plugin :default_url_options, cache: s3_options, store: s3_options
if Rails.env.production?
  Shrine.plugin :presign_endpoint, presign_options: { method: :put }
else
  Shrine.plugin :upload_endpoint
end

Shrine::Attacher.promote { |data| PromoteJob.perform_async(data) }
Shrine::Attacher.delete { |data| DeleteJob.perform_async(data) }
