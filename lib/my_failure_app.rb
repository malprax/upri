class MyFailureApp < Devise::FailureApp
  def route(scope)
    :new_user_session_url
  end

  def respond
      if http_auth?
        http_auth
      else
        redirect
      end
    end
end
