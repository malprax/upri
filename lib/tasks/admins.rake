require 'io/console'
namespace :admins do
  task :account_setup => :environment do
    STDOUT.puts "Enter admin email:"
    email = STDIN.gets.strip
    STDOUT.puts "Is this super admin?: (y/n)"

    if email.present?
      STDOUT.puts "Enter admin password:"
      password = STDIN.noecho(&:gets).chomp
      if password.present?
        STDOUT.puts "Enter admin password again:"
        confirmation_password = STDIN.noecho(&:gets).chomp
        if confirmation_password.present?
          if confirmation_password == password
            user = Admin.new
            user.email = email
            # user.first_name = "Admin"
            user.password = password
            
            User.skip_callback(:create, :before, :check_type)
            if user.save
              STDOUT.puts "Success."
            else
              STDOUT.puts "Error. #{user.errors.inspect}"
            end
          else
            STDOUT.puts "Password not match"
          end
        end
      end
    else
      STDOUT.puts "Some error happened."
    end
  end
end
