namespace :save_data_dikti do
  desc "Jenjang Pendidikan"
  task jenjang_didik: :environment do |t|
    jenjang_didik_list_hash = JSON.parse(File.read('json/jenjang_pendidikan.json'))

    jenjang_didik_list_hash.each do |j|
      JenjangDidik.where(id: j['id_jenjang_didik'].to_i, nama:j['nama_jenjang_didik']).first_or_create
    end

  end

  desc "Agama"
  task agama: :environment do |t|
    agama_list_hash = JSON.parse(File.read('json/agama.json'))

    agama_list_hash.each do |j|
      Religion.where(id: j['id_agama'].to_i, nama:j['nama_agama']).first_or_create
    end

  end

  desc "Negara"
  task negara: :environment do |t|
    negara_list_hash = JSON.parse(File.read('json/negara.json'))

    negara_list_hash.each do |j|
      State.where(kode: j['id_negara'], nama:j['nama_negara']).first_or_create
    end
  end

  desc "Wilayah"
  task wilayah: :environment do |t|
    wilayah_list_hash = JSON.parse(File.read('json/wilayah.json'))

    wilayah_list_hash.each do |j|
      District.where(id_wilayah: j['id_wilayah'], nama:j['nama_wilayah'], id_negara:j['id_negara']).first_or_create
    end

  end

  desc "save state id"
  task save_state_id: :environment do |t|
    State.all do |negara|
      District.where(id_negara: negara.kode) do |wilayah|
        wilayah.update(state_id: negara.id)
      end
    end
  end

  desc "Tinggal bersama"
  task tinggal_bersama: :environment do |t|
   tinggal_bersama_list_hash = JSON.parse(File.read('json/jenis_tinggal.json'))

   tinggal_bersama_list_hash.each do |j|
      Live.where(id: j['id_jenis_tinggal'].to_i, nama:j['nama_jenis_tinggal']).first_or_create
    end

  end

  desc "Alat Transportasi"
  task alat_transportasi: :environment do |t|
    alat_transportasi_list_hash = JSON.parse(File.read('json/alat_transportasi.json'))

    alat_transportasi_list_hash.each do |j|
      Transportation.where(id: j['id_alat_transportasi'].to_i, nama:j['nama_alat_transportasi']).first_or_create
    end

  end

  desc "Pendidikan"
  task pendidikan: :environment do |t|
    pendidikan_list_hash = JSON.parse(File.read('json/jenjang_pendidikan.json'))

    pendidikan_list_hash.each do |j|
      Education.where(id: j['id_jenjang_didik'].to_i, nama:j['nama_jenjang_didik']).first_or_create
    end

  end

  desc "Pekerjaan"
  task pekerjaan: :environment do |t|
    pekerjaan_list_hash = JSON.parse(File.read('json/pekerjaan.json'))

    pekerjaan_list_hash.each do |j|
      Job.where(id: j['id_pekerjaan'].to_i, nama:j['nama_pekerjaan']).first_or_create
    end

  end

  desc "Penghasilan"
  task penghasilan: :environment do |t|
    penghasilan_list_hash = JSON.parse(File.read('json/penghasilan.json'))

    penghasilan_list_hash.each do |j|
      Income.where(id: j['id_penghasilan'].to_i, nama:j['nama_penghasilan']).first_or_create
    end

  end

  desc "Kebutuhan Khusus"
  task kebutuhan_khusus: :environment do |t|
    kebutuhan_khusus_list_hash = JSON.parse(File.read('json/kebutuhan_khusus.json'))

    kebutuhan_khusus_list_hash.each do |j|
      SpecialNeed.where(id: j['id_kebutuhan_khusus'].to_i, nama:j['nama_kebutuhan_khusus']).first_or_create
    end

  end


  desc "Perguruan Tinggi"
  task perguruan_tinggi: :environment do |t|
    perguruan_tinggi_list_hash = JSON.parse(File.read('json/perguruan_tinggi.json'))

    perguruan_tinggi_list_hash.each do |j|
      # Agama.where(id: j['id_jenjang_didik'].to_i, nama:j['nama_jenjang_didik']).first_or_create
    end

  end

  desc "Prodi"
  task prodi: :environment do |t|
    prodi_list_hash = JSON.parse(File.read('json/prodi.json'))

    prodi_list_hash.each do |j|
      # Agama.where(id: j['id_jenjang_didik'].to_i, nama:j['nama_jenjang_didik']).first_or_create
    end

  end

  desc "Mahasiswa"
  task mahasiswa: :environment do |t|
    prodi_list_hash = JSON.parse(File.read('json/mahasiswa.json'))

    prodi_list_hash.each do |j|
      # Agama.where(id: j['id_jenjang_didik'].to_i, nama:j['nama_jenjang_didik']).first_or_create
    end

  end

  desc "Dosen"
  task dosen: :environment do |t|
    prodi_list_hash = JSON.parse(File.read('json/dosen.json'))

    prodi_list_hash.each do |j|
      # Agama.where(id: j['id_jenjang_didik'].to_i, nama:j['nama_jenjang_didik']).first_or_create
    end

  end
end
