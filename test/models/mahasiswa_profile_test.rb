# == Schema Information
#
# Table name: mahasiswa_profiles
#
#  id                            :bigint(8)        not null, primary key
#  mahasiswa_id                  :integer
#  email                         :string
#  nama                          :string
#  nim                           :string
#  jenis_kelamin                 :string
#  tempat_lahir                  :string
#  tanggal_lahir                 :date
#  kategori                      :string
#  religion_id                   :bigint(8)
#  major_id                      :bigint(8)
#  faculty_id                    :bigint(8)
#  nik                           :string
#  nisn                          :string
#  npwp                          :string
#  state_id                      :bigint(8)
#  jalan                         :string
#  dusun                         :string
#  rt                            :string
#  rw                            :string
#  kelurahan                     :string
#  kode_pos                      :string
#  district_id                   :bigint(8)
#  live_id                       :bigint(8)
#  transportation_id             :bigint(8)
#  telepon                       :string
#  handphone                     :string
#  penerima_kps                  :string
#  nomor_kps                     :string
#  nik_ayah                      :string
#  nama_ayah                     :string
#  tanggal_lahir_ayah            :string
#  pendidikan_ayah_id            :bigint(8)
#  pekerjaan_ayah_id             :bigint(8)
#  penghasilan_ayah_id           :bigint(8)
#  nik_ibu                       :string
#  nama_ibu                      :string
#  tanggal_lahir_ibu             :string
#  pendidikan_ibu_id             :bigint(8)
#  pekerjaan_ibu_id              :bigint(8)
#  penghasilan_ibu_id            :bigint(8)
#  nama_wali                     :string
#  tanggal_lahir_wali            :string
#  pendidikan_wali_id            :bigint(8)
#  pekerjaan_wali_id             :bigint(8)
#  penghasilan_wali_id           :bigint(8)
#  kebutuhan_khusus_ayah_id      :bigint(8)
#  kebutuhan_khusus_ibu_id       :bigint(8)
#  kebutuhan_khusus_mahasiswa_id :bigint(8)
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#
# Indexes
#
#  index_mahasiswa_profiles_on_district_id                    (district_id)
#  index_mahasiswa_profiles_on_faculty_id                     (faculty_id)
#  index_mahasiswa_profiles_on_kebutuhan_khusus_ayah_id       (kebutuhan_khusus_ayah_id)
#  index_mahasiswa_profiles_on_kebutuhan_khusus_ibu_id        (kebutuhan_khusus_ibu_id)
#  index_mahasiswa_profiles_on_kebutuhan_khusus_mahasiswa_id  (kebutuhan_khusus_mahasiswa_id)
#  index_mahasiswa_profiles_on_live_id                        (live_id)
#  index_mahasiswa_profiles_on_mahasiswa_id                   (mahasiswa_id)
#  index_mahasiswa_profiles_on_major_id                       (major_id)
#  index_mahasiswa_profiles_on_pekerjaan_ayah_id              (pekerjaan_ayah_id)
#  index_mahasiswa_profiles_on_pekerjaan_ibu_id               (pekerjaan_ibu_id)
#  index_mahasiswa_profiles_on_pekerjaan_wali_id              (pekerjaan_wali_id)
#  index_mahasiswa_profiles_on_pendidikan_ayah_id             (pendidikan_ayah_id)
#  index_mahasiswa_profiles_on_pendidikan_ibu_id              (pendidikan_ibu_id)
#  index_mahasiswa_profiles_on_pendidikan_wali_id             (pendidikan_wali_id)
#  index_mahasiswa_profiles_on_penghasilan_ayah_id            (penghasilan_ayah_id)
#  index_mahasiswa_profiles_on_penghasilan_ibu_id             (penghasilan_ibu_id)
#  index_mahasiswa_profiles_on_penghasilan_wali_id            (penghasilan_wali_id)
#  index_mahasiswa_profiles_on_religion_id                    (religion_id)
#  index_mahasiswa_profiles_on_state_id                       (state_id)
#  index_mahasiswa_profiles_on_transportation_id              (transportation_id)
#

require 'test_helper'

class MahasiswaProfileTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
