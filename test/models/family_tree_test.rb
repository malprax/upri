# == Schema Information
#
# Table name: family_trees
#
#  id                         :bigint(8)        not null, primary key
#  family_name                :string
#  family_status              :string
#  gender                     :string
#  birth_date                 :string
#  birth_place                :string
#  job                        :string
#  info                       :string
#  address                    :string
#  phone                      :string
#  office_name                :string
#  office_registration_number :string
#  user_id                    :bigint(8)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#
# Indexes
#
#  index_family_trees_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

require 'test_helper'

class FamilyTreeTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
