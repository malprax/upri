# == Schema Information
#
# Table name: payments
#
#  id               :bigint(8)        not null, primary key
#  kind             :string
#  academic_year_id :bigint(8)        not null
#  in_number        :float
#  in_word          :string
#  student_id       :integer          not null
#  bank_id          :bigint(8)        not null
#  approval_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  paid             :datetime
#  type_student     :string
#  is_approved      :boolean          default(FALSE)
#
# Indexes
#
#  index_payments_on_academic_year_id  (academic_year_id)
#  index_payments_on_bank_id           (bank_id)
#
# Foreign Keys
#
#  fk_rails_...  (academic_year_id => academic_years.id)
#  fk_rails_...  (bank_id => banks.id)
#
require 'test_helper'

class PaymentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
