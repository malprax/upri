# == Schema Information
#
# Table name: users
#
#  id                         :bigint(8)        not null, primary key
#  email                      :string           default(""), not null
#  encrypted_password         :string           default(""), not null
#  reset_password_token       :string
#  reset_password_sent_at     :datetime
#  remember_created_at        :datetime
#  sign_in_count              :integer          default(0), not null
#  current_sign_in_at         :datetime
#  last_sign_in_at            :datetime
#  current_sign_in_ip         :inet
#  last_sign_in_ip            :inet
#  confirmation_token         :string
#  confirmed_at               :datetime
#  confirmation_sent_at       :datetime
#  unconfirmed_email          :string
#  failed_attempts            :integer          default(0), not null
#  unlock_token               :string
#  locked_at                  :datetime
#  image_data                 :text
#  type                       :string
#  provider                   :string
#  uid                        :string
#  name                       :string
#  birth_date                 :date
#  birth_place                :string
#  gender                     :string
#  religion                   :string
#  marital_status             :string
#  hobby                      :string
#  stambuk                    :string
#  is_student                 :boolean          default(FALSE)
#  is_graduate                :boolean
#  is_lecture                 :boolean
#  is_staff                   :boolean
#  access_token               :string
#  nidn                       :string
#  nip                        :string
#  dummy_lecture_1_id         :bigint(8)
#  dummy_lecture_2_id         :bigint(8)
#  major_id                   :bigint(8)
#  faculty_id                 :bigint(8)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  blangko_data               :text
#  academic_year_id           :integer
#  sortlist                   :integer
#  is_active                  :boolean          default(TRUE)
#  phone_number               :string
#  authy_id                   :string
#  last_sign_in_with_authy    :datetime
#  authy_enabled              :boolean          default(FALSE)
#  is_phone_verified          :boolean          default(FALSE)
#  is_alumni                  :boolean          default(FALSE)
#  religion_id                :bigint(8)
#  lecture_functional_type_id :bigint(8)
#  referral                   :string
#
# Indexes
#
#  index_users_on_access_token                (access_token) UNIQUE
#  index_users_on_authy_id                    (authy_id)
#  index_users_on_confirmation_token          (confirmation_token) UNIQUE
#  index_users_on_dummy_lecture_1_id          (dummy_lecture_1_id)
#  index_users_on_dummy_lecture_2_id          (dummy_lecture_2_id)
#  index_users_on_email                       (email) UNIQUE
#  index_users_on_faculty_id                  (faculty_id)
#  index_users_on_lecture_functional_type_id  (lecture_functional_type_id)
#  index_users_on_major_id                    (major_id)
#  index_users_on_nidn                        (nidn) UNIQUE
#  index_users_on_religion_id                 (religion_id)
#  index_users_on_reset_password_token        (reset_password_token) UNIQUE
#  index_users_on_stambuk                     (stambuk) UNIQUE
#  index_users_on_unlock_token                (unlock_token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (lecture_functional_type_id => lecture_functional_types.id)
#  fk_rails_...  (religion_id => religions.id)
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
