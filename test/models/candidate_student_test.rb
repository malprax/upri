# == Schema Information
#
# Table name: candidate_students
#
#  id                  :bigint(8)        not null, primary key
#  registration_number :string
#  class_plan          :string
#  major_1             :string
#  major_2             :string
#  name                :string
#  birth_date          :date
#  birth_place         :string
#  gender              :string
#  phone_number        :string
#  street_address      :string
#  hamlet              :string
#  neighbourhood       :string
#  urban_village       :string
#  sub_district        :string
#  district            :string
#  province            :string
#  postal_code         :integer
#  school              :string
#  school_address      :string
#  school_plan         :string
#  nisn                :string
#  sertificate_number  :string
#  sertificate_date    :string
#  national_exam_score :string
#  father_name         :string
#  father_job          :string
#  father_salary       :string
#  father_education    :string
#  mother_name         :string
#  mother_job          :string
#  mother_salary       :string
#  mother_education    :string
#  know_from           :string
#  academic_year_id    :integer
#  referral            :string
#  email               :string
#  approve             :boolean          default(FALSE)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'test_helper'

class CandidateStudentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
