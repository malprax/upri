# == Schema Information
#
# Table name: student_profiles
#
#  id                       :bigint(8)        not null, primary key
#  student_id               :integer
#  nama                     :string
#  nomor_induk_mahasiswa    :string
#  jenis_kelamin            :string
#  tempat_lahir             :string
#  tanggal_lahir            :datetime
#  handphone                :string
#  agama                    :string
#  kewarganegaraan          :string
#  major_id                 :bigint(8)
#  status                   :string
#  keterangan_tinggal       :string
#  is_active                :boolean          default(FALSE)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  angkatan                 :string
#  konsentrasi              :string
#  tahun_berlaku_matakuliah :string
#
# Indexes
#
#  index_student_profiles_on_major_id  (major_id)
#

require 'test_helper'

class StudentProfileTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
