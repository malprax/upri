require 'test_helper'

class SubjectPlansControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get subject_plans_new_url
    assert_response :success
  end

  test "should get create" do
    get subject_plans_create_url
    assert_response :success
  end

  test "should get edit" do
    get subject_plans_edit_url
    assert_response :success
  end

  test "should get update" do
    get subject_plans_update_url
    assert_response :success
  end

  test "should get destroy" do
    get subject_plans_destroy_url
    assert_response :success
  end

end
