require 'test_helper'

class CampusNewspapersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get campus_newspapers_index_url
    assert_response :success
  end

  test "should get new" do
    get campus_newspapers_new_url
    assert_response :success
  end

  test "should get edit" do
    get campus_newspapers_edit_url
    assert_response :success
  end

end
