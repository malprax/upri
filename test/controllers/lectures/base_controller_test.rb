require 'test_helper'

class Lectures::BaseControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get lectures_base_index_url
    assert_response :success
  end

end
