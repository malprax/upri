require 'test_helper'

class Lectures::DashboardsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get lectures_dashboards_index_url
    assert_response :success
  end

end
