require 'test_helper'

class BookCollectionsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get book_collections_index_url
    assert_response :success
  end

  test "should get new" do
    get book_collections_new_url
    assert_response :success
  end

  test "should get edit" do
    get book_collections_edit_url
    assert_response :success
  end

end
