require 'test_helper'

class AcademicYearsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get academic_years_new_url
    assert_response :success
  end

  test "should get edit" do
    get academic_years_edit_url
    assert_response :success
  end

end
