require 'test_helper'

class ItemAccreditationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @item_accreditation = item_accreditations(:one)
  end

  test "should get index" do
    get item_accreditations_url
    assert_response :success
  end

  test "should get new" do
    get new_item_accreditation_url
    assert_response :success
  end

  test "should create item_accreditation" do
    assert_difference('ItemAccreditation.count') do
      post item_accreditations_url, params: { item_accreditation: { academic_year_id: @item_accreditation.academic_year_id, item: @item_accreditation.item } }
    end

    assert_redirected_to item_accreditation_url(ItemAccreditation.last)
  end

  test "should show item_accreditation" do
    get item_accreditation_url(@item_accreditation)
    assert_response :success
  end

  test "should get edit" do
    get edit_item_accreditation_url(@item_accreditation)
    assert_response :success
  end

  test "should update item_accreditation" do
    patch item_accreditation_url(@item_accreditation), params: { item_accreditation: { academic_year_id: @item_accreditation.academic_year_id, item: @item_accreditation.item } }
    assert_redirected_to item_accreditation_url(@item_accreditation)
  end

  test "should destroy item_accreditation" do
    assert_difference('ItemAccreditation.count', -1) do
      delete item_accreditation_url(@item_accreditation)
    end

    assert_redirected_to item_accreditations_url
  end
end
