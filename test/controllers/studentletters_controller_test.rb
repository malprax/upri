require 'test_helper'

class StudentlettersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @studentletter = studentletters(:one)
  end

  test "should get index" do
    get studentletters_url
    assert_response :success
  end

  test "should get new" do
    get new_studentletter_url
    assert_response :success
  end

  test "should create studentletter" do
    assert_difference('Studentletter.count') do
      post studentletters_url, params: { studentletter: { alamat: @studentletter.alamat, diketahui_kepala_tata_usaha: @studentletter.diketahui_kepala_tata_usaha, faculty_id: @studentletter.faculty_id, jenis_surat: @studentletter.jenis_surat, judul_skripsi: @studentletter.judul_skripsi, kategori_nomor_induk_instansi: @studentletter.kategori_nomor_induk_instansi, kepala_tata_usaha: @studentletter.kepala_tata_usaha, keterangan_pindah: @studentletter.keterangan_pindah, major_id: @studentletter.major_id, nama: @studentletter.nama, nama_orang_tua: @studentletter.nama_orang_tua, nomor_induk_instansi: @studentletter.nomor_induk_instansi, nomor_surat: @studentletter.nomor_surat, nomor_telepon: @studentletter.nomor_telepon, nomor_telepon_orang_tua: @studentletter.nomor_telepon_orang_tua, pangkat_golongan: @studentletter.pangkat_golongan, stambuk: @studentletter.stambuk, tanggal_diketahui: @studentletter.tanggal_diketahui, tanggal_lahir: @studentletter.tanggal_lahir, tanggal_yudisium: @studentletter.tanggal_yudisium, tempat_kerja: @studentletter.tempat_kerja, tempat_lahir: @studentletter.tempat_lahir } }
    end

    assert_redirected_to studentletter_url(Studentletter.last)
  end

  test "should show studentletter" do
    get studentletter_url(@studentletter)
    assert_response :success
  end

  test "should get edit" do
    get edit_studentletter_url(@studentletter)
    assert_response :success
  end

  test "should update studentletter" do
    patch studentletter_url(@studentletter), params: { studentletter: { alamat: @studentletter.alamat, diketahui_kepala_tata_usaha: @studentletter.diketahui_kepala_tata_usaha, faculty_id: @studentletter.faculty_id, jenis_surat: @studentletter.jenis_surat, judul_skripsi: @studentletter.judul_skripsi, kategori_nomor_induk_instansi: @studentletter.kategori_nomor_induk_instansi, kepala_tata_usaha: @studentletter.kepala_tata_usaha, keterangan_pindah: @studentletter.keterangan_pindah, major_id: @studentletter.major_id, nama: @studentletter.nama, nama_orang_tua: @studentletter.nama_orang_tua, nomor_induk_instansi: @studentletter.nomor_induk_instansi, nomor_surat: @studentletter.nomor_surat, nomor_telepon: @studentletter.nomor_telepon, nomor_telepon_orang_tua: @studentletter.nomor_telepon_orang_tua, pangkat_golongan: @studentletter.pangkat_golongan, stambuk: @studentletter.stambuk, tanggal_diketahui: @studentletter.tanggal_diketahui, tanggal_lahir: @studentletter.tanggal_lahir, tanggal_yudisium: @studentletter.tanggal_yudisium, tempat_kerja: @studentletter.tempat_kerja, tempat_lahir: @studentletter.tempat_lahir } }
    assert_redirected_to studentletter_url(@studentletter)
  end

  test "should destroy studentletter" do
    assert_difference('Studentletter.count', -1) do
      delete studentletter_url(@studentletter)
    end

    assert_redirected_to studentletters_url
  end
end
