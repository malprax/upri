# Preview all emails at http://localhost:3000/rails/mailers/subject_plan_mailer
class SubjectPlanMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/subject_plan_mailer/send_token_payment
  def send_token_payment
    SubjectPlanMailer.send_token_payment
  end

end
