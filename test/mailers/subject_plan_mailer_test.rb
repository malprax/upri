require 'test_helper'

class SubjectPlanMailerTest < ActionMailer::TestCase
  test "send_token_payment" do
    mail = SubjectPlanMailer.send_token_payment
    assert_equal "Send token payment", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
